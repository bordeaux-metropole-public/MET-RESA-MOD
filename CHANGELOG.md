# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [8.x-3.19] - 2022-04-13

### Added

- Possibilité de désactiver les cautions par chèque.
- Intégration de la gestion des cautions dans le handler V2.

## [8.x-3.18] - 2022-01-10

### Fixed

- Gestion du cache du block Reservation pour corriger un bug d'affichage
  probablement lié à la migration vers Webform 6.x.

## [8.x-3.17] - 2022-01-10

### Added

- Suppression automatique de l'entité `ReservationDemande` associée à une
  soumission `webform` lorsque cette dernière est supprimée.

## [8.x-3.16] - 2022-01-04

### Added

- Mettre en place un accès sécurisé aux entités `ReservationDemande` en fonction
  des ressources affectées à l'utilisateur (#72).

## [8.x-2.0.4] - 2019-09-24

### Fixed

- \#58 : Correction "Administrive Notes"
- \#65 : Route de retour vide suite à la modification d'une demande

## [8.x-2.0.3] - 2019-09-24

### Fixed

- \#58 : Correction "Administrive Notes"

## [8.x-2.0.2] - 2019-08-12

### Fixed

- \#62 : Correction Cancelled
- champs par défaut Commun

## [8.x-2.0.1] - 2019-07-26

### Fixed

- Modification des demandes non publiées
- plantage sqlTables

## [8.x-2.0.0] - 2019-07-26

### Fixed

- \#47 : Erreur Décompte

### Added

- ajout export CSV Webform
- ajout de la personnalisation des demandes
- ajout changement statut CAUTION

### Changed

- mise en français des mois
- ajout ancrage block
- ajout block ancrage
- Demande - ajout compteur en fonction des statuts
- mécanisme Caution
- modification statut
- modification disponibilités
- modification cheque
- ajout date craneau + saisie manuel

## [8.x-1.0.0] - 2018-09-11

### Added

- Initialisation du projet

# Templates:

## [x.x.x] - YYYY-MM-DD

### Added

### Changed

### Removed

### Deprecated

### Fixed

### Security
