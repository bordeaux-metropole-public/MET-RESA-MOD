(function ($, Drupal, drupalSettings) {

  var fetchTimer = null;

  Drupal.behaviors.reservation_datepicker_element = {

    attach: function (context) {

      var calendarInputId = '#reservation-calendar';
      once('reservation-calendar', calendarInputId, context).forEach(function () { // to avoid double binding

        if (fetchTimer) {
          clearTimeout(fetchTimer);
          fetchTimer = null;
        }

        var statutDates = {},
          rdidDates = {},
          horaireDates = {},
          placeDates = {},
          placeNoWaitDates = {},
          jaugeDates = {},
          currentDate = new Date(),
          currentYear = currentDate.getFullYear(),
          currentMonth = currentDate.getMonth(),
          formId = $('#webform-reservation-ressource-id').val(),
          demandeId = $('#webform-reservation-demande-id').val(),
          CalendarDatepickerId = '#reservation-calendar-datepicker',
          horaireWrapperId = '#reservation-horaire-wrapper',
          horaireTitleId = '#span-reservation-horaire-title',
          horaireSelectId = '#reservation-horaire-select',
          nbAccompagnantsSelector = '#reservation-nb-accompagnants-wrapper',
          nbAccompagnantsSelectId = '#reservation-accompagnateur-select',
          moisEntier = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
          moisCourt = ["Di", "Lu", "Ma", "Me", "Je", "Ve", "Sa"];

        hideHoraire();
        hideNbAccompagnants();
        disableNextButton();

        showButtons();
        if ($(calendarInputId).hasClass('showElements')) {
          showElements();
          showHoraire();
          showNbAccompagnants();
        }

        var autoRefresh = $(calendarInputId).hasClass('autorefresh');

        $(CalendarDatepickerId).datepicker({
          dateFormat: "yy-mm-dd",
          firstDay: 1,
          minDate: new Date(currentYear, currentMonth, '01'),
          maxDate: new Date(currentYear + 5, currentMonth, ''),
          monthNames: moisEntier,
          dayNamesMin: moisCourt,
          onSelect: function (date) {
            dateSelection(date);
          },
          beforeShowDay: function (date) {
            if (statutDates[date] === 'unavailableDates') {
              return [false, 'unavailable', ''];
            }
            else if (statutDates[date] === 'waitDates') {
              if (isOriginalChosenDate(getDateFormat(date))) {
                return [true, 'waiting', 'waitDate'];
              }
              else {
                return [false, 'waiting', 'waitDate'];
              }
            }
            else if (statutDates[date] === 'reserveDates') {
              return [false, 'reserved', 'reserveDate'];
            }
            else if (statutDates[date] === 'openDates') {
              return [true, 'opened', 'openDate'];
            }
            else {
              return [false, 'closed', ''];
            }
          },
        }).find('.ui-datepicker-current-day')
          .removeClass('ui-datepicker-current-day');

        // Affichage de la légende du calendrier
        once('calendar-legend-display', CalendarDatepickerId).forEach(function (element) {
          $(element).append(
            '<div id="legendBloc">' +
            '<div class="legendDiv"><svg width="30" height="30"><rect width="30" height="30" style="fill:#a4c414;stroke-width:0;" /></svg><p>Disponible</p></div>' +
            '<div class="legendDiv"><svg width="30" height="30"><rect width="30" height="30" style="fill:#c60b0b;stroke-width:0;" /></svg><p>Réservé</p></div>' +
            '<div class="legendDiv"><svg width="30" height="30"><rect width="30" height="30" style="fill:#da9800;stroke-width:0;" /></svg><p>En attente</p></div>' +
            '<div class="legendDiv"><svg width="30" height="30"><rect width="30" height="30" style="fill:#cacaca;stroke-width:0;" /></svg><p>Fermé</p></div>' +
            '</div>'
          )
        });

        if ($(horaireSelectId) && $(nbAccompagnantsSelectId)) {
          $(horaireSelectId).on('change', function () {
            var placesDispo = refreshNbAccompagnantsByHoraire(this.value);
            toggleNextButton(placesDispo > 0);
            setDisponibilitesCookie(placesDispo);
          });
        }

        var sameJSON = false;
        var previousExpiresTimestamp = '';

        if (formId) {
          previousExpiresTimestamp = getJSONExpiresCookie();
          let reservationCalendarURL = '/reservation/calendar/' + formId;
          fetchJSONData(reservationCalendarURL);
        }

        function fetchJSONData(urlReservationCalendar) {
          if (document.getElementById(CalendarDatepickerId.slice(1))) { // Only if calendar present on page
            if (!fetchTimer) { // Don't fetch while a timer is on
              $.ajax({
                url: urlReservationCalendar,
                dataType: "json",
                async: false,
                success: function (data, status, request) {
                  var now = new Date();
                  var expiresDate = new Date(request.getResponseHeader('Expires'));
                  var newExpiresTimestamp = '' + expiresDate.getTime();
                  sameJSON = expiresDate > now && previousExpiresTimestamp === newExpiresTimestamp;
                  setJSONExpiresCookie(newExpiresTimestamp);
                  previousExpiresTimestamp = newExpiresTimestamp;
                  if (!sameJSON) {
                    deleteCookie('dispos-' + formId);
                  }
                  if (autoRefresh) {
                    var expiresIn = expiresDate.getTime() - now.getTime() + 5000; // 5s delay for clock desync
                    expiresIn = Math.max(10000, expiresIn); // 10s minimum delay
                    fetchTimer = setTimeout(function () {
                        fetchTimer = null;
                        fetchJSONData(urlReservationCalendar);
                      }, expiresIn
                    );
                  }
                  processJSONData(data);
                  selectCurrentCalendarValue();
                }
              });
            }
          }
        }

        function processJSONData(data) {
          if (data && data.length > 0) {
            $.each(data, function (key, val) {
              var dateData = new Date(val.date);
              var formattedDate = getDateFormat(dateData);
              statutDates[dateData] = val.statut;
              rdidDates[formattedDate] = val.rdid;
              jaugeDates[formattedDate] = val.jauge;
              placeDates[formattedDate] = val.place;
              placeNoWaitDates[formattedDate] = val['place-nowait'];
              if (val.horaire) {
                horaireDates[formattedDate] = val.horaire;
              }
            });
            var datePicker = $(CalendarDatepickerId);
            var firstDate = new Date(data[0].date);
            firstDate.setDate(1); // on se positionne sur le premier jour du
                                  // mois
            var currentMinDate = datePicker.datepicker('option', 'minDate');
            if (currentMinDate !== firstDate) {
              datePicker.datepicker('option', 'minDate', firstDate);
              datePicker.datepicker('refresh').find('.ui-datepicker-current-day')
                .removeClass('ui-datepicker-current-day');
            }
          }
        }

        function selectCurrentCalendarValue() {
          var currentRdid = $(calendarInputId).val();
          if (currentRdid) {
            for (var dateKey in rdidDates) {
              if (rdidDates.hasOwnProperty(dateKey)) {
                if (rdidDates[dateKey] === currentRdid) {
                  $(CalendarDatepickerId).datepicker('setDate', new Date(dateKey));
                  dateSelection(dateKey);
                  break;
                }
              }
            }
          }
        }

        function dateSelection(date) {
          $("#nombreplacedate").remove();
          $(calendarInputId).val(rdidDates[date]);
          showElements();
          var currentHoraireId = $(horaireSelectId).val();
          $(horaireSelectId).find('option').remove().end();
          var placesDispo;
          if (horaireDates[date]) {
            showHoraire();
            $(horaireTitleId).html('<i style="color:orange" >' + getDateDMY(date) + '</i>');
            var horaires = horaireDates[date];
            if (horaires.length > 0) {
              if (!currentHoraireId) {
                currentHoraireId = $(horaireSelectId).attr('data-original');
              }
              var hasSelection = false;
              $.each(horaires, function (key, val) {
                placesDispo = getRealPlacesDisposByDateAndHoraire(
                  $(calendarInputId).val(), val.rhid, val.place, val['place-nowait']);
                if (placesDispo > 0 || val.jauge === "0") { // places dispos ou pas de jauge
                  var label = val.heure;
                  if ($(calendarInputId).hasClass('showplacehoraire') && val.jauge === "1") { // jauge active
                    label += ' : ' + placesDispo + ' place(s) restante(s)';
                  }
                  var selected = val.rhid === currentHoraireId;
                  hasSelection = hasSelection || selected;
                  $(horaireSelectId).append(
                    $('<option>', {
                      value: val.rhid,
                      text: label,
                      selected: selected
                    })
                  );
                }
              });
              // Si aucun élement n'est sélectionné, on sélectionne le premier
              // pour lequel il reste des places
              if (!hasSelection) {
                $.each(horaires, function (key, val) {
                  if (val.place > 0) {
                    $(horaireSelectId).val(val.rhid);
                    return false;
                  }
                });
              }
              placesDispo = refreshNbAccompagnantsByHoraire($(horaireSelectId).val());
              toggleNextButton(placesDispo > 0);
              setDisponibilitesCookie(placesDispo);
            }
            else {
              refreshNbAccompagnants(0);
              toggleNextButton(false);
            }
          }
          else {
            hideHoraire();
            // BUG : code below required because otherwise webform keeps in
            // form_state previous selected horaire value
            $(horaireSelectId).append(
              $('<option>', {
                value: '',
                text: '',
                selected: true
              })
            );
            placesDispo = getRealPlacesDispos(placeDates[date], placeNoWaitDates[date]);
            if ($(calendarInputId).hasClass('showplacedate') && jaugeDates[date] === "1") { // Affichage si jauge active
              $(CalendarDatepickerId).append(
                '<div id="nombreplacedate"><p><b>Nombre de place(s) restante(s) : </b>' + placesDispo + '</p></div>'
              );
            }
            refreshNbAccompagnants(placesDispo);
            toggleNextButton(placesDispo > 0);
            setDisponibilitesCookie(placesDispo);
          }
        }

        function refreshNbAccompagnantsByHoraire(rhid) {
          var placesDispo = 0;
          var horaires = horaireDates[$(CalendarDatepickerId).val()];
          var refreshed = false;
          if (horaires) {
            for (var i = 0; i < horaires.length; i += 1) {
              var rh = horaires[i];
              if (rhid === rh.rhid) {
                if (rh.jauge === "1") {
                  placesDispo = getRealPlacesDispos(rh.place, rh['place-nowait']);
                }
                else {
                  placesDispo = 999; // TODO ?
                }
                refreshNbAccompagnants(placesDispo);
                refreshed = true;
                break;
              }
            }
          }
          if (!refreshed) {
            refreshNbAccompagnants(0);
          }
          return placesDispo;
        }

        function getPlacesByHoraire(rhid) {
          var horaires = horaireDates[$(CalendarDatepickerId).val()];
          if (horaires) {
            for (var i = 0; i < horaires.length; i += 1) {
              var rh = horaires[i];
              if (rhid === rh.rhid) {
                return rh.place;
              }
            }
          }
          return 0;
        }

        function refreshNbAccompagnants(placesDispo) {
          if ($(nbAccompagnantsSelectId)) {
            var originalValue;
            if (isOriginalChoice()) {
              originalValue = $(nbAccompagnantsSelectId).attr('data-original');
              var selectedValue = $(nbAccompagnantsSelectId).val();
              if (selectedValue && selectedValue !== originalValue) {
                originalValue = selectedValue;
              }
            }
            var nbAccompMax = parseInt($(nbAccompagnantsSelectId).attr('data-maximum'));
            var placesPossibles = Math.min(placesDispo - 1, nbAccompMax);
            // -1 pour le demandeur ^^.
            if (placesPossibles < 0) {
              placesPossibles = 0;
            }
            var nbAccompSelSize = $(nbAccompagnantsSelectId + ' option').length;
            var j;
            for (j = nbAccompSelSize; j <= placesPossibles; j += 1) {
              $(nbAccompagnantsSelectId).append('<option value="' + j + '">' + j + '</option>');
            }
            for (j = placesPossibles + 1; j < nbAccompSelSize; j += 1) {
              $(nbAccompagnantsSelectId + " option[value='" + j + "']").remove();
            }
            if (placesDispo <= 1) {
              hideNbAccompagnants();
            }
            else {
              if (isOriginalChoice()) { // try to reselect original option (TODO?)
                $(nbAccompagnantsSelectId + " option:eq('" + originalValue + "')").prop('selected', true);
              }
              showNbAccompagnants();
            }
          }
        }

        function getRealPlacesDispos(placesDispo, placesNoWait) {
          return getRealPlacesDisposByDateAndHoraire(
            $(calendarInputId).val(), $(horaireSelectId).val(), placesDispo, placesNoWait);
        }

        function getRealPlacesDisposByDateAndHoraire(calendarId, horaireId, placesDispo, placesNoWait) {
          if (sameJSON) {
            var placesDispoFromCookie = getDisponibilitesFromCookie(calendarId, horaireId);
            if (placesDispoFromCookie) {
              placesDispo = placesDispoFromCookie;
            }
          }
          else {
            if (isOriginalChosenDateAndHoraireIds(calendarId, horaireId)) {
              placesDispo += getOriginalPlacesReservees();
            }
          }
          if (placesDispo > placesNoWait) { // Palliatif pour l'affichage
            placesDispo = placesNoWait;
          }
          return placesDispo;
        }

        function isOriginalChosenDate(date) {
          return $(calendarInputId).attr('data-original') === rdidDates[date];
        }

        function isOriginalChosenDateAndHoraireIds(dateId, horaireId) {
          var calendarOriginalId = $(calendarInputId).attr('data-original');
          var horaireOriginalAttr = $(horaireSelectId).attr('data-original');
          var horaireOriginalId = horaireOriginalAttr ? horaireOriginalAttr : '';
          return calendarOriginalId === dateId && horaireOriginalId === (horaireId ? horaireId : '');
        }

        function isOriginalChoice() {
          return isOriginalChosenDateAndHoraireIds($(calendarInputId).val(), $(horaireSelectId).val());
        }

        function getOriginalPlacesReservees() {
          return getPlacesReserveesFromAccompagnantsString($(nbAccompagnantsSelectId).attr('data-original'));
        }

        function getPlacesReserveesFromAccompagnantsString(strValue) {
          var placesReservees = 0;
          if (demandeId > '') { // Demande en cours
            placesReservees += 1; // pour le demandeur
            var nbAccompagnants = parseInt(strValue);
            if (!isNaN(nbAccompagnants)) {
              placesReservees += nbAccompagnants;
            }
          }
          return placesReservees;
        }

        function getDateFormat(date) {

          let day = date.getDate();
          let month = date.getMonth();
          let year = date.getFullYear();
          month = month + 1;

          if (month < 10) {
            month = '0' + month;
          }

          if (day < 10) {
            day = '0' + day;
          }

          let dateFormat = year + '-' + month + '-' + day;

          return dateFormat;
        }

        function getDateDMY(date) {
          let d = new Date(date);

          let day = d.getDate();
          let month = d.getMonth();
          let year = d.getFullYear();

          let dateFormat = day + ' ' + moisEntier[month] + ' ' + year;

          return dateFormat;
        }


        function toggleNextButton(state) {
          if (state) {
            enableNextButton();
          }
          else {
            disableNextButton();
          }
        }

        function enableNextButton() {
          var form = $(calendarInputId).closest("form");
          $(form).find('button.form-submit').prop('disabled', false);
        }

        function disableNextButton() {
          var form = $(calendarInputId).closest("form");
          $(form).find('button.form-submit').prop('disabled', true);
        }

        function hideHoraire() {
          if (!$(calendarInputId).hasClass('showElements')) {
            $(horaireWrapperId).hide();
          }
        }

        function showHoraire() {
          $(horaireWrapperId).show();
        }

        function hideNbAccompagnants() {
          if (!$(calendarInputId).hasClass('showElements')) {
            $(nbAccompagnantsSelector).hide();
          }
        }

        function showNbAccompagnants() {
          $(nbAccompagnantsSelector).show();
        }

        function showElements() {
          $('.region-sidebar-right .field--type-text-with-summary').css('display', 'block');
        }

        function showButtons() {
          $('.block-reservation .form-submit').css('display', 'block');
        }

        // Required for IE compatibility instead of array.find
        function arrayFind(arr, callback) {
          for (var i = 0; i < arr.length; i = i + 1) {
            var match = callback(arr[i]);
            if (match) {
              return arr[i];
            }
          }
        }

        function getCookie(name) {
          let cookieArray = document.cookie.split('; ');
          let cookieStr = arrayFind(cookieArray, function (row) {
            return row.indexOf(name) === 0;
          });
          if (cookieStr) {
            return cookieStr.split('=')[1];
          }
          else {
            return null;
          }
        }

        function deleteCookie(name) {
          document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        }

        function getJSONExpiresCookie() {
          return getCookie('json-expires-' + formId);
        }

        function setJSONExpiresCookie(newExpiresTimestamp) {
          document.cookie = 'json-expires-' + formId + '=' + newExpiresTimestamp;
        }

        function getDisponibilitesFromCookie(calendarId, horaireId) {
          var cookieArray = getDisponibilitesCookieArray();
          return cookieArray[calendarId + '-' + horaireId];
        }

        function getDisponibilitesCookieArray() {
          var cookieArray = [];
          var cookieStr = getCookie('dispos-' + formId);
          if (cookieStr) {
            var cookieSplit = cookieStr.split('|');
            for (var i = 0; i < cookieSplit.length; i += 1) {
              var disposArray = cookieSplit[i].split('-');
              cookieArray[disposArray[0] + '-' + disposArray[1]] = parseInt(disposArray[2]);
            }
          }
          return cookieArray;
        }

        function setDisponibilitesCookie(placesDispo) {
          var calendarOriginalId = $(calendarInputId).val();
          if (calendarOriginalId) {
            var horaireOriginalId = $(horaireSelectId).val();
            var cookieArray = getDisponibilitesCookieArray();
            cookieArray[calendarOriginalId + '-' + horaireOriginalId] = placesDispo;
            var cookieArrayStr = '';
            for (var key in cookieArray) {
              if (cookieArray.hasOwnProperty(key)) {
                if (cookieArrayStr > '') {
                  cookieArrayStr += '|';
                }
                cookieArrayStr += key + '-' + cookieArray[key];
              }
            }
            var cookieStr = 'dispos-' + formId + '=' + cookieArrayStr;
            document.cookie = cookieStr;
          }
        }
      });
    }
  };
})(jQuery, Drupal, drupalSettings);
