<?php

namespace Drupal\reservation\Controller;

use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\reservation\Entity\ReservationHoraire;
use Drupal\reservation\Entity\ReservationRessourceNode;
use Drupal\reservation\ReservationConstants;
use Drupal\reservation\Service\ReservationCalendarServices;
use Drupal\reservation\Service\ReservationDateServices;
use Drupal\reservation\Service\ReservationDemandeServices;
use Drupal\reservation\Service\ReservationDemandeTokenServices;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Ce controller permet la mise a disposition des dates et places pour le
 * datepicker dans le webform.
 *
 * Class CalendarSelectionController.
 *
 * @package Drupal\reservation\Controller
 */
class CalendarSelectionController extends ControllerBase implements ContainerInjectionInterface {

  /**
   *
   */
  const STATUT_RESERVE = 'reserveDates';

  /**
   *
   */
  const STATUT_WAIT = 'waitDates';

  /**
   *
   */
  const STATUT_OPEN = 'openDates';

  /**
   *
   */
  const JSON_CALENDAR_SETTINGS = 'json_calendar';

  /**
   * Default calendar JSON cache maximum age in seconds.
   */
  const CALENDAR_JSON_DEFAULT_CACHE_MAXAGE = 10;

  /**
   * Default lock time in seconds.
   */
  const DEFAULT_LOCK_TIME = 60;

  /**
   * @var \Drupal\reservation\Service\ReservationDateServices
   */
  private ReservationDateServices $dateServices;

  /**
   * @var \Drupal\reservation\Service\ReservationDemandeTokenServices
   */
  private ReservationDemandeTokenServices $tokenServices;

  /**
   * @var \Drupal\reservation\Service\ReservationDemandeServices
   */
  private ReservationDemandeServices $demandeServices;

  /**
   * @var \Drupal\reservation\Service\ReservationCalendarServices
   */
  protected ReservationCalendarServices $calendarServices;

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * @var int
   */
  protected $cacheMaxAge;

  /**
   * @var bool
   */
  protected $debugEnabled;

  /**
   * CalendarSelectionController constructor.
   *
   * @param \Drupal\reservation\Service\ReservationDateServices $dateServices
   * @param \Drupal\reservation\Service\ReservationDemandeTokenServices $tokenServices
   * @param \Drupal\reservation\Service\ReservationDemandeServices $demandeServices
   * @param \Drupal\reservation\Service\ReservationCalendarServices $calendarServices
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   */
  public function __construct(ReservationDateServices $dateServices,
                              ReservationDemandeTokenServices $tokenServices,
                              ReservationDemandeServices $demandeServices,
                              ReservationCalendarServices $calendarServices,
                              CacheBackendInterface $cache) {
    $this->dateServices = $dateServices;
    $this->tokenServices = $tokenServices;
    $this->demandeServices = $demandeServices;
    $this->calendarServices = $calendarServices;
    $this->cache = $cache;

    $reservationSettings = \Drupal::config(ReservationConstants::MODULE_SETTINGS);
    $jsonCalendarSettings = $reservationSettings->get(self::JSON_CALENDAR_SETTINGS);
    $maxAgeSettings = $jsonCalendarSettings['max-age'];
    if (is_numeric($maxAgeSettings)) {
      $this->cacheMaxAge = intval($maxAgeSettings);
    }
    else {
      $this->cacheMaxAge = self::CALENDAR_JSON_DEFAULT_CACHE_MAXAGE;
    }
    $this->debugEnabled = ($jsonCalendarSettings['debug-enabled'] == 1);
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *
   * @return CalendarSelectionController
   */
  public static function create(ContainerInterface $container) {

    return new static(
      $container->get('reservation.date'),
      $container->get('reservation.demande.token'),
      $container->get('reservation.demande'),
      $container->get('reservation.calendar'),
      $container->get('cache.default')
    );
  }

  /**
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function getNombrePlacesDisponiblesByHoraire($rhid = NULL) {
    $nbPlacesDisponibles = 0;

    if ($rhid == NULL || $rhid == -1) {
      $token = $this->tokenServices->getCookie();
      $demande = $this->tokenServices->loadDemandeByToken($token);
      if ($demande == NULL) {
        $nbPlacesDisponibles = 0;
      }
      else {
        if ($demande->getHoraire()) {
          $date = $this->calendarServices->getDemandeHoraireDetail($demande->gethoraire());
        }
        else {
          $date = $this->calendarServices->getDemandeDate($demande->getDate());
        }
        $nbPlacesDisponibles = $date["place"] + 0;
      }
    }
    else {
      $reservationHoraire = ReservationHoraire::load($rhid);
      if ($reservationHoraire != NULL) {
        $count = $this->calendarServices->getDemandeHoraireDetail($reservationHoraire);

        $nbPlacesDisponibles = $count['horaire']['place'];
      }
    }

    return new JsonResponse($nbPlacesDisponibles);
  }

  /**
   * Tableau de données regroupant les dates et les horaires disponibles avec
   * le statut exemple :
   * [
   *       {   // date sans horaire
   *           "rdid":"10",
   *           "date":"06\/28\/2018",
   *           "statut":"waitDates",
   *           "horaire":false
   *       },
   *       {   // date avec horaire
   *           "rdid":"44",
   *           "date":"08\/01\/2018",
   *           "statut":"openDates",
   *           "horaire":
   *               [
   *                   {
   *                       "rhid":"458",
   *                       "heure":"10:10",
   *                       "place":10
   *                   },
   *                   {
   *                       "rhid":"488",
   *                       "heure":"12:20",
   *                       "place":1
   *                   }
   *              ]
   *      },
   *  ]
   *
   * @param mixed $nid
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function getDate($nid) {

    $jsonResponse = NULL;

    if (\Drupal::currentUser()->hasPermission('creation manuelle demande')) {

      $dates = $this->extractDatesFromDatesPubliees($nid, FALSE);

      $jsonResponse = new JsonResponse($dates);
    }
    else {
      if ($this->cacheMaxAge > 0) {

        $cacheAndLockIdentifier = __METHOD__ . '_' . $nid;

        $valueCache = $this->cache->get($cacheAndLockIdentifier);
        if ($valueCache) {
          $jsonResponse = new JsonResponse($valueCache->data);
          if ($this->debugEnabled) {
            \Drupal::logger(ReservationConstants::LOG_CHANNEL)
              ->debug(t("Cache toujours valide"));
          }
        }
        else {
          $lock = \Drupal::lock();

          if (!$lock->acquire($cacheAndLockIdentifier, self::DEFAULT_LOCK_TIME)) {
            $valueCache = $this->cache->get($cacheAndLockIdentifier, TRUE);

            if ($valueCache) {
              $jsonResponse = new JsonResponse($valueCache->data);
              if ($this->debugEnabled) {
                \Drupal::logger(ReservationConstants::LOG_CHANNEL)
                  ->debug(t("Lock en cours, cache possiblement expiré"));
              }
            }
            else {
              $jsonResponse = new JsonResponse();
              if ($this->debugEnabled) {
                \Drupal::logger(ReservationConstants::LOG_CHANNEL)
                  ->debug(t("Lock en cours, pas de cache, retour vide"));
              }
            }
          }
          else {
            $valueCache = $this->cache->get($cacheAndLockIdentifier);

            if ($valueCache) {
              $jsonResponse = new JsonResponse($valueCache->data);
              if ($this->debugEnabled) {
                \Drupal::logger(ReservationConstants::LOG_CHANNEL)
                  ->debug(t("Lock acquis, cache encore valide"));
              }
            }
            else {
              if ($this->debugEnabled) {
                \Drupal::logger(ReservationConstants::LOG_CHANNEL)
                  ->debug(t("Lock acquis, pas de cache"));
              }
              // Purge des demandes obsolètes.
              $this->tokenServices->destroyTokenObsolete();

              // Extraction des demandes publiées et leurs statuts.
              $dates = $this->extractDatesFromDatesPubliees($nid);

              $this->cache->set($cacheAndLockIdentifier, $dates, time() + ($this->cacheMaxAge / 2));

              $jsonResponseCacheMetaData['#cache'] = [
                'max-age' => $this->cacheMaxAge,
                'contexts' => [
                  'url',
                ],
              ];

              $jsonResponse = new CacheableJsonResponse($dates);
              $jsonResponse->addCacheableDependency(CacheableMetadata::createFromRenderArray($jsonResponseCacheMetaData));

            }

            $lock->release($cacheAndLockIdentifier);
          }
        }

        $jsonResponse->setPublic();
        $jsonResponse->setMaxAge($this->cacheMaxAge);
        $jsonResponse->setExpires(new \DateTime('@' . (\Drupal::time()
          ->getRequestTime() + $this->cacheMaxAge)));

      }
      else {
        // Extraction des demandes publiées et leurs statuts.
        $dates = $this->extractDatesFromDatesPubliees($nid);

        $jsonResponse = new JsonResponse($dates);

      }
    }

    return $jsonResponse;
  }

  /**
   * Extraction des demandes publiées et leurs statuts.
   *
   * @param mixed $nid
   *
   * @return array
   */
  public function extractDatesFromDatesPubliees($nid, $published = TRUE) {
    $dates = [];
    /** @var \Drupal\reservation\Entity\ReservationRessourceNode $ressourceNode */
    $ressourceNode = ReservationRessourceNode::load($nid);
    $datesPubliees = $this->dateServices->getPublishedDates($nid, FALSE, $published);
    foreach ($datesPubliees as $datePubliee) {
      if ($datePubliee->getHoraire()) {
        // Récupération des dates ayant un horaire.
        $date = $this->calendarServices->getDemandeHoraire($datePubliee, $ressourceNode);
      }
      else {
        // Récupération des dates seules.
        if ($ressourceNode->isOffLimit($datePubliee->getDateTime())) {
          $date = $this->calendarServices->getOffLimitDate($datePubliee);
        }
        else {
          $date = $this->calendarServices->getDemandeDate($datePubliee);
        }
      }
      if (!empty($date)) {
        $dates[] = $date;
      }
    }
    return $dates;
  }

}
