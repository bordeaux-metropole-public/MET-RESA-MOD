<?php

namespace Drupal\reservation\Element;

use Drupal\webform\Element\WebformCompositeBase;

/**
 * Element Webform Custom permettant d'afficher un champ select, contenant une
 * jauge dynamique de place.
 *
 * @FormElement("webform_reservation_ancrage")
 */
class WebformReservationAncrage extends WebformCompositeBase {

  /**
   *
   */
  public static function initializeCompositeElements(array &$element) {
    $composite_elements = static::getCompositeElements($element);

    $element['#attached']['library'][] = 'reservation/reservation.ancrage';
    static::initializeCompositeElementsRecursive($element, $composite_elements);
    return $composite_elements;
  }

  /**
   *
   * @param array $element
   *
   * @return string
   */
  public static function getCompositeElements(array $element) {
    $elements = [];

    return $elements;
  }

  /**
   *
   * @return string
   */
  public function getInfo() {
    $info = parent::getInfo();
    $info['#id'] = 'reservation-ancrage';
    $info['#prefix'] = '<div id="webform-reservation-ancrage">';
    $info['#suffix'] = '</div>';

    return $info;
  }

}
