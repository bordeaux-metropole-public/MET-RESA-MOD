<?php

namespace Drupal\reservation\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\FormElement;
use Drupal\Core\Url;
use Drupal\reservation\Entity\ReservationDemande;

/**
 * Provides a 'webform_reservation_caution' form element.
 *
 * Il permet d'afficher le ou les boutons de choix du type de caution pour la
 * réservation.
 *
 * @FormElement("webform_reservation_caution")
 *
 * @see \Drupal\Core\Render\Element\FormElement
 * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Render%21Element%21FormElement.php/class/FormElement
 * @see \Drupal\Core\Render\Element\RenderElement
 * @see https://api.drupal.org/api/drupal/namespace/Drupal%21Core%21Render%21Element
 */
class WebformReservationCaution extends FormElement {

  /**
   * @param $element
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param $complete_form
   *
   * @return mixed
   */
  public static function processCaution(&$element, FormStateInterface $form_state, &$complete_form) {

    $name = $element['#name'];

    // Here you can add and manipulate your element's properties and callbacks.
    $demandeId = $form_state->getValue('webform_reservation_config')['webform_reservation_demande_id'];

    if ($demandeId) {

      $element_caution = ['#tree' => TRUE];

      /** @var \Drupal\reservation\Service\ReservationDemandeServices $demandeServices */
      $demandeServices = \Drupal::service('reservation.demande');
      /** @var \Drupal\reservation\Entity\ReservationDemande $demande */
      $demande = $demandeServices->load($demandeId);

      if ($demande) {

        /** @var \Drupal\reservation\Entity\ReservationRessourceNode $resourceNode */
        $resourceNode = $demande->getDate()->getReservationRessourceNode();

        if ($resourceNode->getCautionStatut()) {

          $id = $element['#id'];

          $montantCaution = $resourceNode->getCautionMontant();

          $element_caution['message']['#type'] = 'item';
          $msg_caution_message = t('Pour valider votre réservation, il vous faut déposer une caution');
          $element_caution['message']['#markup'] = '<p>' . $msg_caution_message . '.</p>';

          $element_caution['montant']['#type'] = 'item';
          $msg_caution_montant = t('Montant de&nbsp;: %montant €', ['%montant' => $montantCaution]);
          $element_caution['montant']['#markup'] = '<p>' . $msg_caution_montant . '</p>';

          $element_caution['carte_bancaire_ok']['#type'] = 'item';
          $element_caution['carte_bancaire_ok']['#id'] = 'edit-' . $id . '-carte-bancaire-ok';
          $msg_caution_cb_ok = t('Vous avez validé votre caution par carte bancaire.');
          $element_caution['carte_bancaire_ok']['#markup'] = '<p><strong>' . $msg_caution_cb_ok . '</strong></p>';

          $default_value = $element['#default_value'] ?? NULL;

          // Si caution par CB pas déjà effectuée.
          if ($default_value != ReservationDemande::CAUTION_CARTE) {

            if (isset($element['#cheque_accepte']) && $element['#cheque_accepte'] === TRUE) {

              $element_caution['form']['#type'] = 'fieldset';
              $caution_form = &$element_caution['form'];
              $caution_form['#id'] = 'edit-' . $id . '-form';

              $caution_form['choix']['#type'] = 'item';
              $msg_caution_choix = t('Sélectionnez le mode de caution&nbsp;:');
              $caution_form['choix']['#markup'] = '<p>' . $msg_caution_choix . '</p>';

              $caution_form['cheque']['#type'] = 'radio';
              $caution_form['cheque']['#id'] = 'edit-' . $id . '-cheque';
              $caution_form['cheque']['#name'] = $name . '[form][caution_type]';
              $caution_form['cheque']['#title'] = t('Chèque');
              $caution_form['cheque']['#return_value'] = ReservationDemande::CAUTION_CHEQUE;
              // Required for checked.
              $caution_form['cheque']['#value'] = ReservationDemande::CAUTION_CHEQUE;

              $caution_form['carte_bancaire']['#type'] = 'radio';
              $caution_form['carte_bancaire']['#id'] = 'edit-' . $id . '-carte-bancaire';
              $caution_form['carte_bancaire']['#name'] = $name . '[form][caution_type]';
              $caution_form['carte_bancaire']['#title'] = t('Carte bancaire');
              $caution_form['carte_bancaire']['#return_value'] = ReservationDemande::CAUTION_CARTE;
              // Required for checked.
              $caution_form['carte_bancaire']['#value'] = ReservationDemande::CAUTION_CARTE;
            }
            else {
              $caution_form = &$element_caution;
            }

            $caution_form['carte_bancaire_link'] = [
              '#type' => 'link',
              '#id' => 'edit-' . $id . '-carte-bancaire-link',
              '#title' => t('Enregistrer votre caution par carte bancaire'),
              '#url' => Url::fromRoute('reservation.demande.caution.modal_v2', ['demandeid' => $demandeId]),
              '#attributes' => [
                'class' => [
                  'use-ajax',
                  'button',
                  'button-action',
                  'button--primary',
                ],
              ],
            ];
          }

          $element_caution['caution']['#type'] = 'hidden';
          $element_caution['caution']['#id'] = 'edit-' . $id;
          $element_caution['caution']['#default_value'] = $default_value;

          $element_caution['token']['#type'] = 'hidden';
          $element_caution['token']['#id'] = 'edit-' . $id . '-token';
          $element_caution['token']['#default_value'] = $demande->getWebformToken();

        }
        else {

          $element_caution['message']['#type'] = 'item';
          $msg_caution_message = t("Cette ressource n'exige pas de caution.");
          $element_caution['message']['#markup'] = '<p>' . $msg_caution_message . '</p>';

        }

      }
      else {

        $element_caution['message']['#type'] = 'item';
        $msg_caution_message = t('Un numéro de demande de réservation est exigé pour le dépôt de la caution.');
        $element_caution['message']['#markup'] = '<p>' . $msg_caution_message . '</p>';

      }

      $element[$name] = $element_caution;
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    if ($input === FALSE) {
      return NULL;
    }
    else {
      return isset($input) ? $input['caution'] : NULL;
    }
  }

  /**
   * Webform element validation handler for #type 'webform_example_element'.
   */
  public static function validateCaution(&$element, FormStateInterface $form_state, &$complete_form) {
    $name = $element['#name'];
    $caution_values = $form_state->getValue($name);
    $value = $caution_values['caution'] ?? NULL;
    $element['#value'] = $value;
    $form_state->setValueForElement($element, $value);
  }

  /**
   * Prepares a #type 'caution' render element for theme_element().
   *
   * @param array $element
   *   An associative array containing the properties of the element.
   *   Properties used: #title, #value, #description, #size, #maxlength,
   *   #placeholder, #required, #attributes.
   *
   * @return array
   *   The $element with prepared variables ready for theme_element().
   */
  public static function preRenderCaution(array $element) {

    $name = $element['#name'];

    if (isset($element[$name]['caution'])) {
      Element::setAttributes($element[$name]['caution'], ['id']);
    }

    if (isset($element[$name]['form']['carte_bancaire'])) {
      Element::setAttributes($element[$name]['form']['carte_bancaire'], ['value']);
    }

    if (isset($element[$name]['form']['cheque'])) {
      Element::setAttributes($element[$name]['form']['cheque'], ['value']);
    }

    if (isset($element[$name]['token'])) {
      Element::setAttributes($element[$name]['token'], ['id']);
    }

    $element['#attached']['library'][] = 'reservation/reservation.caution';

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#input' => TRUE,
      '#id' => 'reservation-caution',
      '#process' => [
        [$class, 'processCaution'],
        [$class, 'processAjaxForm'],
      ],
      '#element_validate' => [
        [$class, 'validateCaution'],
      ],
      '#pre_render' => [
        [$class, 'preRenderCaution'],
      ],
      '#theme_wrappers' => ['form_element'],
    ];
  }

}
