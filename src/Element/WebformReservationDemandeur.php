<?php

namespace Drupal\reservation\Element;

/**
 * Provides a 'webform_reservation_demandeur'.
 *
 * @FormElement("webform_reservation_demandeur")
 */
class WebformReservationDemandeur extends WebformReservationPersonne {

}
