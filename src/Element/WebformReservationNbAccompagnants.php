<?php

namespace Drupal\reservation\Element;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Element\WebformCompositeBase;
use Drupal\webform\Utility\WebformElementHelper;

/**
 * Element Webform Custom permettant d'afficher un champ select, contenant une
 * jauge dynamique de place.
 *
 * @FormElement("webform_reservation_nb_accompagnants")
 */
class WebformReservationNbAccompagnants extends WebformCompositeBase {

  /**
   *
   * @param array $element
   *
   * @return array
   */
  public static function getCompositeElements(array $element) {
    $elements = [];

    if (isset($element["#jauge"])) {
      $options = array_combine(range(1, $element["#jauge"]), range(1, $element["#jauge"]));
    }
    else {
      $options = [];
    }

    $default_value = isset($element['#default_value']) ? intval($element['#default_value']) : 0;

    $elements['reservation-accompagnateur-select'] = [
      '#id' => 'reservation-accompagnateur-select',
      '#type' => 'select',
      '#title' => "Nombre d'accompagnants",
      '#options' => $options,
      "#default_value" => $default_value,
      '#empty_value' => 0,
      '#validated' => TRUE,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function preRenderWebformCompositeFormElement($element) {
    $element = parent::preRenderCompositeFormElement($element);

    // @todo Faut-il soustraire 1 pour le demandeur ?
    $jauge = $element['#jauge'];
    $element['reservation-accompagnateur-select']['#attributes']['data-maximum'] = $jauge;

    $value = $element['#value']['reservation-accompagnateur-select'] ?? NULL;
    $element['reservation-accompagnateur-select']['#attributes']['data-original'] = $value;

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function validateWebformComposite(&$element, FormStateInterface $form_state, &$complete_form) {
    // This method is overriden because parent method removes zero values and we
    // need to keep them here.
    $value = NestedArray::getValue($form_state->getValues(), $element['#parents']);
    if (!is_numeric($value['reservation-accompagnateur-select'])) {
      WebformElementHelper::setRequiredError($element['reservation-accompagnateur-select'], $form_state);
    }
  }

  /**
   *
   * @return array
   */
  public function getInfo() {
    return [
      '#default_value' => 0,
      '#prefix' => '<div id="reservation-nb-accompagnants-wrapper">',
      '#suffix' => '</div>',
    ] + parent::getInfo();
  }

}
