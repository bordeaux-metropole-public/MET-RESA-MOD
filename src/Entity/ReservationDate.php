<?php

namespace Drupal\reservation\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\reservation\ReservationDateInterface;
use Drupal\user\UserInterface;

/**
 * Entité liée à ReservationRessourceNode qui permet d'allouer des dates
 * réservables
 *
 * @ingroup reservation
 *
 * @ContentEntityType(
 *   id = "reservation_date",
 *   label = @Translation("Reservation Date entity"),
 *   handlers = {
 *     "views_data" =
 *   "Drupal\reservation\Entity\ViewsData\ReservationDemandeDateData",
 *     "route_provider" = {
 *       "html" = "Drupal\reservation\ReservationDateHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "reservation_date",
 *   admin_permission = "administer Reservation Date entities",
 *   entity_keys = {
 *     "id" = "rdid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/reservation_date/{reservation_date}",
 *   },
 *   field_ui_base_route = "reservation_date.settings"
 * )
 */
class ReservationDate extends ContentEntityBase implements ReservationDateInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   *
   * When a new entity instance is added, set the user_id entity reference to
   * the current user as the creator of the instance.
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   *
   * Define the field properties here.
   *
   * Field name, type and size determine the table structure.
   *
   * In addition, we can define how the field and its content can be manipulated
   * in the GUI. The behaviour of the widgets used can be determined here.
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields['rdid'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the reservationdate entity.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the reservationdate entity.'))
      ->setReadOnly(TRUE);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('User Name'))
      ->setDescription(t('The Name of the associated user.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 1,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['nid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Node ID'))
      ->setDescription(t('Node lié à la réservation.'))
      ->setSetting('target_type', 'reservation_ressource_node')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'title',
        'weight' => 2,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['date'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Day'))
      ->setDescription(t('Day to reservation'))
      ->setSettings([
        'max_length' => 2,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 10,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['statut'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('statut'))
      ->setDescription(t('The statut of the reservationressource entity.'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['publie'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publié'))
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 30,
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 30,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['horaire'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Horaire'))
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 30,
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 30,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['rappel'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Rappel'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 40,
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 40,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['rappel_jour'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Rappel à J-'))
      ->setDefaultValue(3)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 41,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 41,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['enquete'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Enquete'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 50,
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 50,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['enquete_jour'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Enquête de satisfaction à J+'))
      ->setDefaultValue(3)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 51,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 51,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['jauge_statut'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Statut Jauge'))
      ->setDefaultValue(1)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 60,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 60,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['jauge_nombre'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Jauge'))
      ->setDefaultValue(1)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 60,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 60,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getChangedTime() {
    return $this->get('changed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * @return \Drupal\reservation\ReservationRessourceNodeInterface
   */
  public function getReservationRessourceNode() {
    return $this->get('nid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getDateFormat($format) {
    $date = $this->getDateTime();
    return $date->format($format);
  }

  /**
   * {@inheritdoc}
   */
  public function getDateTime() {
    return new \DateTime($this->getDate());
  }

  /**
   * {@inheritdoc}
   */
  public function getDate() {
    return $this->get('date')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatut() {
    return $this->get('statut')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatut($statut) {
    $this->set('statut', $statut ? '1' : '0');
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getHoraire() {
    return $this->get('horaire')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getJaugeStatut() {
    return $this->get('jauge_statut')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getJaugeNombre() {
    return $this->get('jauge_nombre')->value;
  }

  /**
   * Test si la demande est accessible pour la notification d'un rappel.
   *
   * {@inheritdoc}
   */
  public function availableRappel() {
    if ($this->getRappel()) {
      $date = $this->getDateTime();
      $dateRappel = new \DateTime();
      $dateRappel->add(new \DateInterval('P' . $this->getRappelJour() . 'D'));
      if ($date->format('Y-m-d') == $dateRappel->format('Y-m-d')) {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
    else {
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getRappel() {
    return $this->get('rappel')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getRappelJour() {
    return $this->get('rappel_jour')->value;
  }

  /**
   * Test si la demande est accessible pour la notification d'une enquête.
   *
   * {@inheritdoc}
   */
  public function availableEnquete() {
    if ($this->getEnquete()) {
      $date = $this->getDateTime();
      $dateEnquete = new \DateTime();
      $dateEnquete->sub(new \DateInterval('P' . $this->getEnqueteJour() . 'D'));
      if ($date->format('Y-m-d') == $dateEnquete->format('Y-m-d')) {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
    else {
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getEnquete() {
    return $this->get('enquete')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getEnqueteJour() {
    return $this->get('enquete_jour')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getPublie() {
    return $this->get('publie')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPublie($publie) {
    $this->set('publie', $publie ? '1' : '0');
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setHoraire($horaire) {
    $this->set('horaire', $horaire ? '1' : '0');
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDay() {
    return $this->get('day')->value;
  }

  /**
   * @return bool
   */
  public function isToday() {
    $date = new \DateTime();
    $match_date = $this->getDateTime();
    $interval = $date->diff($match_date);
    return ($interval->days == 0);
  }

}
