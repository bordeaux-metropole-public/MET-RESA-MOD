<?php

namespace Drupal\reservation\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\reservation\ReservationDemandeInterface;
use Drupal\reservation\ReservationUtils;

/**
 * Defines the ContentEntityExample entity.
 *
 * Entité permettant à un utilisateur de réserver un créneau, en fonction de
 * ReservationDate et/ou ReservationHoraire.
 *
 * @ingroup reservation
 *
 * @ContentEntityType(
 *   id = "reservation_demande",
 *   label = @Translation("Réservation Demande entity"),
 *   handlers = {
 *     "views_data" =
 *   "Drupal\reservation\Entity\ViewsData\ReservationDemandeData",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\reservation\ReservationDemandeListBuilder",
 *
 *     "form" = {
 *       "default" = "Drupal\reservation\Form\ReservationDemandeForm",
 *       "add" = "Drupal\reservation\Form\ReservationDemandeForm",
 *       "edit" = "Drupal\reservation\Form\ReservationDemandeForm",
 *       "delete" = "Drupal\reservation\Form\ReservationDemandeDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\reservation\ReservationDemandeHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\reservation\ReservationDemandeAccessControlHandler",
 *   },
 *   base_table = "reservation_demande",
 *   admin_permission = "administer ReservationDemande entities",
 *   entity_keys = {
 *     "id" = "rdmid"
 *   },
 *   links = {
 *     "canonical" =
 *   "/admin/structure/reservation_demande/{reservation_demande}",
 *     "add-form" = "/admin/structure/reservation_demande/add",
 *     "edit-form" =
 *   "/admin/structure/reservation_demande/{reservation_demande}/edit",
 *     "delete-form" =
 *   "/admin/structure/reservation_demande/{reservation_demande}/delete",
 *     "collection" = "/admin/structure/reservation_demande",
 *   },
 *   field_ui_base_route = "reservation_demande.settings"
 * )
 */
class ReservationDemande extends ContentEntityBase implements ReservationDemandeInterface {

  const CAUTION_NON_APPLICABLE = 'na';

  const CAUTION_NON_RENSEIGNEE = 'nr';

  const CAUTION_CARTE = 'carte';

  const CAUTION_CHEQUE = 'cheque';

  const CAUTION_ACCEPT = 'accept';

  const CAUTION_DECLINE = 'decline';

  const CAUTION_EXCEPT = 'except';

  const CAUTION_CANCEL = 'cancel';

  const STATUT_CAUTION = 'caution';

  const STATUT_FORMULAIRE = 'formulaire';

  const STATUT_NO_SHOW = 'noshow';

  const STATUT_SHOW = 'show';

  const STATUT_CONFIRME = 'confirme';

  const STATUT_ANNULE = 'annule';

  const STATUT_ATTENTE = 'attente';

  const STATUT_REFUSE = 'refuse';

  const STATUT_ARCHIVE = 'archive';

  const STATUT_RAPPEL = 'rappel';

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['rdmid'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the reservationdemande entity.'))
      ->setReadOnly(TRUE);

    $fields['rdid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Réservation Date'))
      ->setSetting('target_type', 'reservation_date')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'title',
        'weight' => -3,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['rhid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Réservation Horaire'))
      ->setSetting('target_type', 'reservation_horaire')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'title',
        'weight' => -3,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['sid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Webform Submission'))
      ->setSetting('target_type', 'webform_submission')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'title',
        'weight' => -3,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['statut'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('statut'))
      ->setSettings([
        'allowed_values' => [
          'confirme' => 'Confirmée',
          'annule' => 'Annulée',
          'attente' => 'En attente',
          'caution' => 'En attente Caution',
          'refuse' => 'Refusée',
          'archive' => 'Archivée',
          'noshow' => 'NoShow',
          'show' => t('Resa Show'),
        ],
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 20,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['jauge'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Nombre total de personnes'))
      ->setDefaultValue(1)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 60,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 60,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['demandeur'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Demandeur'))
      ->setRequired(TRUE)
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 1,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['email'] = BaseFieldDefinition::create('email')
      ->setLabel(t('Email'))
      ->setRequired(FALSE)
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 1,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['telephone'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Téléphone'))
      ->setRequired(FALSE)
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 1,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['postcode'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Code postal'))
      ->setRequired(FALSE)
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 1,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['caution'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('caution'))
      ->setRequired(TRUE)
      ->setDefaultValue(ReservationDemande::CAUTION_NON_APPLICABLE)
      ->setSettings([
        'allowed_values' => [
          ReservationDemande::CAUTION_NON_APPLICABLE => t('N/A'),
          ReservationDemande::CAUTION_NON_RENSEIGNEE => t('Non Renseigné'),
          ReservationDemande::CAUTION_CHEQUE => t('Chèque'),
          ReservationDemande::CAUTION_CARTE => t('Carte Bancaire'),
        ],
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 20,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['webform_token'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Webform token'))
      ->setRequired(FALSE)
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 1,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['webform_token_expiration'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Webform token expiration time'))
      ->setRequired(FALSE)
      ->setSettings([
        'datetime_type' => 'datetime',
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'datetime_default',
        'settings' => [
          'format_type' => 'medium',
        ],
        'weight' => 1,
      ])
      ->setDisplayOptions('form', [
        'type' => 'datetime_default',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    // $this->set('name', $name);
    return $this;
  }

  /**
   *
   * @return mixed
   */
  public function getDateCreneau() {
    $ressourceDate = $this->rdid->entity;
    $ressourceHoraire = $this->rhid->entity;

    if ($ressourceHoraire) {
      $date_reservation = $ressourceDate->getDateFormat('d/m/Y') . ' ' . $ressourceHoraire->getHeureDebutFormat('(H:i') . '-' . $ressourceHoraire->getHeureFinFormat('H:i') . ')';
    }
    elseif ($ressourceDate) {
      $date_reservation = $ressourceDate->getDateFormat('d/m/Y');
    }
    else {
      return NULL;
    }

    return $date_reservation;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedFormat() {
    $date = new \DateTime();
    $date->setTimestamp($this->getCreatedTime());
    return $date->format('d/m/Y H:i');
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getChangedTime() {
    return $this->get('changed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getDemandeur() {
    return $this->get('demandeur')->value ? $this->get('demandeur')->value : '';
  }

  /**
   *
   */
  public function getCautionLabel() {
    $values_list = $this->getFieldDefinition('caution')
      ->getSetting('allowed_values');
    return $values_list[$this->getCaution()];
  }

  /**
   * {@inheritdoc}
   */
  public function getCaution() {
    return $this->get('caution')->value;
  }

  /**
   *
   */
  public function getCautionDelay() {
    $dateCreated = new \DateTime();
    $dateCreated->setTimestamp($this->getCreatedTime());
    $dateNow = new \DateTime();
    return $dateCreated->diff($dateNow)->format(" (%a J)");
  }

  /**
   *
   * @param mixed $caution
   *
   * @return $this
   */
  public function setCaution($caution) {
    $this->set('caution', $caution);
    return $this;
  }

  /**
   * @return array
   */
  public function getWebformSubmissionData() {
    if ($this->getWebformSubmission()) {
      return $this->getWebformSubmission()->getData();
    }
    else {
      return [];
    }
  }

  /**
   * @return mixed
   */
  public function getWebformSubmission() {
    return $this->get('sid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getHoraire() {
    return $this->get('rhid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getEmail() {
    return $this->get('email')->value ? $this->get('email')->value : '';
  }

  /**
   * {@inheritdoc}
   */
  public function getTitleNode() {
    if ($this->getDate()) {
      if ($this->getDate()->getReservationRessourceNode()) {
        if ($this->getDate()->getReservationRessourceNode()->getNode()) {
          return $this->getDate()
            ->getReservationRessourceNode()
            ->getNode()
            ->getTitle();
        }
        else {
          return "Erreur - Pas de node lié";
        }
      }
      else {
        return "Erreur - Pas de node lié";
      }
    }
    else {
      return "Erreur - Pas de node lié";
    }
  }

  /**
   * @return \Drupal\reservation\Entity\ReservationDate
   */
  public function getDate() {
    return $this->get('rdid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getTelephone() {
    return $this->get('telephone')->value ? $this->get('telephone')->value : '';
  }

  /**
   * Returns the postcode.
   */
  public function getPostcode() {
    return $this->get('postcode')->value ? $this->get('postcode')->value : '';
  }

  /**
   *
   * @return mixed
   */
  public function getStatut() {
    return $this->get('statut')->value;
  }

  /**
   *
   */
  public function getRdid() {
    return $this->get('rdid')->value;
  }

  /**
   *
   * @param mixed $rdid
   *
   * @return $this
   */
  public function setRdid($rdid) {
    $this->set('rdid', $rdid);
    return $this;
  }

  /**
   *
   */
  public function getRhid() {
    return $this->get('rhid')->value;
  }

  /**
   *
   * @param mixed $rhid
   *
   * @return $this
   */
  public function setRhid($rhid) {
    $this->set('rhid', $rhid);
    return $this;
  }

  /**
   *
   *
   * @return mixed
   */
  public function getSid() {
    return $this->get('sid')->target_id;
  }

  /**
   *
   * @param mixed $sid
   *
   * @return $this
   */
  public function setSid($sid) {
    $this->set('sid', $sid);
    return $this;
  }

  /**
   *
   * @param mixed $statut
   *
   * @return $this
   */
  public function setStatut($statut) {
    $this->set('statut', $statut);
    return $this;
  }

  /**
   *
   * @return mixed
   */
  public function getJauge() {
    return $this->get('jauge')->value;
  }

  /**
   *
   * @param mixed $jauge
   *
   * @return $this
   */
  public function setJauge($jauge) {
    $this->set('jauge', $jauge);
    return $this;
  }

  /**
   *
   * @param mixed $email
   *
   * @return $this
   */
  public function setEmail($email) {
    $this->set('email', $email);
    return $this;
  }

  /**
   *
   * @param mixed $telephone
   *
   * @return $this
   */
  public function setTelephone($telephone) {
    $this->set('telephone', $telephone);
    return $this;
  }

  /**
   *
   * @param mixed $postcode
   *
   * @return $this
   */
  public function setPostcode($postcode) {
    $this->set('postcode', $postcode);
    return $this;
  }

  /**
   * @return mixed
   */
  public function getWebformToken() {
    return $this->get('webform_token')->value;
  }

  /**
   *
   * @param mixed $token
   *
   * @return $this
   */
  public function setWebformToken($token) {
    $this->set('webform_token', $token);
    return $this;
  }

  /**
   * @return \DateTime
   */
  public function getWebformTokenExpiration() {
    return ReservationUtils::getDateEntityFieldValue($this, 'webform_token_expiration');
  }

  /**
   *
   * @param \DateTime $expiration
   *
   * @return $this
   */
  public function setWebformTokenExpiration(\DateTime $expiration = NULL) {
    return ReservationUtils::setDateEntityFieldValue($this, 'webform_token_expiration', $expiration);
  }

  /**
   *
   * @param mixed $nom
   * @param mixed $prenom
   *
   * @return $this
   */
  public function setDemandeur($nom = NULL, $prenom = NULL) {
    if ($prenom) {
      $demandeur = $nom . ' ' . $prenom;
      $this->set('demandeur', $demandeur);
    }
    else {
      $this->set('demandeur', $nom);
    }
    return $this;
  }

}
