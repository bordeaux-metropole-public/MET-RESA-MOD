<?php

namespace Drupal\reservation\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\reservation\ReservationDemandeInterface;

/**
 * Defines the ContentEntityExample entity.
 *
 * Entité permettant de gèrer le temps de réservation pour un utilisateur.
 *
 * @ingroup reservation
 *
 * @ContentEntityType(
 *   id = "reservation_demande_token",
 *   label = @Translation("ressource demande token entity"),
 *   base_table = "reservation_demande_token",
 *   entity_keys = {
 *     "id" = "token",
 *   }
 * )
 */
class ReservationDemandeToken extends ContentEntityBase implements ReservationDemandeInterface {

  const TOKEN_TIME_LIMIT = 'time_limit';

  const TOKEN_TIME_CAUTION = 'time_caution';

  const TOKEN_TIME_MANUELLE = 'time_manuelle';


  use EntityChangedTrait;

  /**
   *
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['token'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Token'))
      ->setDescription(t('Token reservation.'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 50,
        'text_processing' => 0,
      ]);

    $fields['rdmid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Réservation Demande ID'))
      ->setSetting('target_type', 'reservation_demande')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'title',
        'weight' => -3,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['timeout'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Time Out'))
      ->setRequired(TRUE)
      ->setDefaultValue('time_limit')
      ->setSettings([
        'allowed_values' => [
          'time_limit' => 'Temps Formulaire',
          'time_caution' => 'Temps Caution',
          'time_manuelle' => 'Temps Demande Manuelle',
        ],
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 20,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    // $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedFormat() {
    $date = new \DateTime();
    $date->setTimestamp($this->getCreatedTime());
    return $date;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getChangedTime() {
    return $this->get('changed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getDemande() {
    return $this->get('rdmid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getTimeout() {
    return $this->get('timeout')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getToken() {
    return $this->get('token')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setToken($timeout) {
    $this->set('token', $token);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setTimeout($timeout) {
    $this->set('timeout', $timeout);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setDemande($rdmid) {
    $this->set('rdmid', $rdmid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($created) {
    $this->set('created', $created);
    return $this;
  }

}
