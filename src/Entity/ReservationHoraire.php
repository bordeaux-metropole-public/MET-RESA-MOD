<?php

namespace Drupal\reservation\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\reservation\ReservationHoraireInterface;
use Drupal\reservation\ReservationUtils;

/**
 * Defines the ContentEntityExample entity.
 *
 * Entité liée à ReservationDate qui permet d'allouer des horaires pour des
 * dates réservables.
 *
 * @ingroup reservation
 *
 * @ContentEntityType(
 *   id = "reservation_horaire",
 *   label = @Translation("Reservation Horaire entity"),
 *   handlers = {
 *     "views_data" =
 *   "Drupal\reservation\Entity\ViewsData\ReservationDemandeHoraireData",
 *     "route_provider" = {
 *       "html" = "Drupal\reservation\ReservationHoraireHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "reservation_horaire",
 *   admin_permission = "administer Reservation Horaire entities",
 *   entity_keys = {
 *     "id" = "rhid"
 *   },
 *   links = {
 *     "canonical" =
 *   "/admin/structure/reservation_horaire/{reservation_horaire}",
 *   },
 *   field_ui_base_route = "reservation_horaire.settings"
 * )
 */
class ReservationHoraire extends ContentEntityBase implements ReservationHoraireInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   *
   * When a new entity instance is added, set the user_id entity reference to
   * the current user as the creator of the instance.
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   *
   * Define the field properties here.
   *
   * Field name, type and size determine the table structure.
   *
   * In addition, we can define how the field and its content can be manipulated
   * in the GUI. The behaviour of the widgets used can be determined here.
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields['rhid'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the reservationhoraire entity.'))
      ->setReadOnly(TRUE);

    $fields['rdid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Node ID'))
      ->setDescription(t("Lien vers l'entité reservation_date."))
      ->setSetting('target_type', 'reservation_date')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'title',
        'weight' => -3,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['statut'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('statut'))
      ->setDescription(t('The statut of the reservationressource entity.'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['heure_debut'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Heure de début'))
      ->setDescription(t("Heure de début d'une réservation"))
      ->setSettings([
        'max_length' => 2,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 10,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['heure_fin'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Heure de fin'))
      ->setDescription(t("Heure de fin d'une réreservation"))
      ->setSettings([
        'max_length' => 2,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 10,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['jauge_statut'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Statut Jauge'))
      ->setDefaultValue(1)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 60,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 60,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['jauge_nombre'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Jauge'))
      ->setDefaultValue(1)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 60,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 60,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getRdid() {
    return $this->get('rdid')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getChangedTime() {
    return $this->get('changed')->value;
  }

  /**
   * @param \DateTime $heureDebut
   *
   * @return \Drupal\Core\Entity\EntityInterface
   */
  public function setHeureDebut(\DateTime $heureDebut) {
    return $this->setHeure('heure_debut', $heureDebut);
  }

  /**
   * @param $fieldName
   * @param \DateTime $heure
   *
   * @return \Drupal\Core\Entity\EntityInterface
   */
  public function setHeure($fieldName, \DateTime $heure) {
    return ReservationUtils::setDateEntityFieldValue($this, $fieldName, $heure);
  }

  /**
   * @param \DateTime $heureFin
   *
   * @return \Drupal\Core\Entity\EntityInterface
   */
  public function setHeureFin(\DateTime $heureFin) {
    return $this->setHeure('heure_fin', $heureFin);
  }

  /**
   * @return mixed
   */
  public function getJaugeStatut() {
    return $this->get('jauge_statut')->value;
  }

  /**
   * @return mixed
   */
  public function getJaugeNombre() {
    return $this->get('jauge_nombre')->value;
  }

  /**
   * @return mixed
   */
  public function getDate() {
    return $this->get('rdid')->entity;
  }

  /**
   * @param $format
   *
   * @return mixed
   */
  public function getHeureDebutFormat($format) {
    return $this->getHeureDebut()->format($format);
  }

  /**
   * @return mixed
   */
  public function getHeureDebut() {
    return $this->getHeure('heure_debut');
  }

  /**
   * @param $fieldName
   *
   * @return mixed
   */
  public function getHeure($fieldName) {
    return ReservationUtils::getDateEntityFieldValue($this, $fieldName);
  }

  /**
   * @param $format
   *
   * @return mixed
   */
  public function getHeureFinFormat($format) {
    return $this->getHeureFin()->format($format);
  }

  /**
   * @return mixed
   */
  public function getHeureFin() {
    return $this->getHeure('heure_fin');
  }

  /**
   * {@inheritdoc}
   */
  public function setJaugeStatut($jauge_statut) {
    $this->set('jauge_statut', $jauge_statut ? '1' : '0');
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatut() {
    return $this->get('statut')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatut($statut) {
    $this->set('statut', $statut ? '1' : '0');
    return $this;
  }

}
