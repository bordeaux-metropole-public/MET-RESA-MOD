<?php

namespace Drupal\reservation\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\reservation\ReservationMailInterface;

/**
 * Defines the ContentEntityExample entity.
 *
 * Element gérant l'historisation d'envoi des notifications.
 *
 * @ingroup reservation
 *
 * @ContentEntityType(
 *   id = "reservation_mail",
 *   label = @Translation("ressource mail entity"),
 *   base_table = "reservation_mail",
 *   entity_keys = {
 *     "id" = "rmid",
 *   }
 * )
 */
class ReservationMail extends ContentEntityBase implements ReservationMailInterface {

  use EntityChangedTrait;

  /**
   *
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields['rmid'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the reservation mail entity.'))
      ->setReadOnly(TRUE);

    $fields['rdmid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Notification ID'))
      ->setSetting('target_type', 'reservation_demande')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'title',
        'weight' => 2,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 2,
      ])
      ->setReadOnly(FALSE);

    $fields['rnid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Notification ID'))
      ->setSetting('target_type', 'reservation_notification')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'title',
        'weight' => 2,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 2,
      ])
      ->setReadOnly(FALSE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getChangedTime() {
    return $this->get('changed')->value;
  }

}
