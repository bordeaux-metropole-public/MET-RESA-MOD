<?php

namespace Drupal\reservation\Entity\ViewsData;

use Drupal\views\EntityViewsData;

/**
 * Provides the views data for the entity.
 */
abstract class EntityViewsDataBase extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $this->attachDateTimeViewsData($data);
    return $data;
  }

  /**
   * Fix views data integration for the datetime fields.
   */
  protected function attachDateTimeViewsData(&$data) {
    // Automatic integration blocked behind https://www.drupal.org/node/2489476
    $datetime_columns = $this->getDatetimeColumnsAsStringArray();

    foreach ($data as $table_name => $table_data) {
      foreach ($datetime_columns as $datetime_column_name) {
        $data[$table_name][$datetime_column_name]['filter']['id'] = 'datetime';
        $data[$table_name][$datetime_column_name]['filter']['field_name'] = $datetime_column_name;
        $data[$table_name][$datetime_column_name]['argument']['id'] = 'datetime';
        $data[$table_name][$datetime_column_name]['argument']['field_name'] = $datetime_column_name;
        $data[$table_name][$datetime_column_name]['sort']['id'] = 'datetime';
        $data[$table_name][$datetime_column_name]['sort']['field_name'] = $datetime_column_name;
      }
    }
  }

  /**
   *
   */
  abstract protected function getDatetimeColumnsAsStringArray();

}
