<?php

namespace Drupal\reservation\Entity\ViewsData;

/**
 * Provides the views data for the entity ReservationDemandeDate.
 */
class ReservationDemandeDateData extends EntityViewsDataBase {

  /**
   *
   */
  protected function getDatetimeColumnsAsStringArray() {
    $datetime_columns = [
      'date',
    ];

    return $datetime_columns;
  }

}
