<?php

namespace Drupal\reservation\Event;

use Drupal\Core\Entity\EntityInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Wraps a node insertion demo event for event listeners.
 */
class EntityEvent extends Event {

  /**
   *
   */
  const NODE_SAVE = 'reservation.node.save';

  /**
   *
   */
  const NODE_DELETE = 'reservation.node.delete';

  /**
   *
   */
  const RESERVATION_RESSOURCE_SAVE = 'reservation.ressource.save';

  /**
   *
   */
  const WEBFORM_SUBMISSION = 'reservation.webform.submission';

  /**
   *
   */
  const RESERVATION_DEMANDE_UPDATE = 'reservation.demande.update';

  /**
   *
   */
  const RESERVATION_DEMANDE_DELETE = 'reservation.demande.delete';

  /**
   *
   */
  const RESSOURCE_NODE_DELETE = 'reservation.ressource.node.delete';

  /**
   *
   */
  const WEBFORM_SUBMISSION_DELETE = 'webform.submission.delete';

  /**
   * Node entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * Constructs a node insertion event object.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity object related to the event.
   */
  public function __construct(EntityInterface $entity) {
    $this->entity = $entity;
  }

  /**
   * Get the inserted entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The related entity object.
   */
  public function getEntity() {
    return $this->entity;
  }

}
