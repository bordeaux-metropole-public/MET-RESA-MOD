<?php

namespace Drupal\reservation\EventSubscriber;

use Drupal\reservation\Entity\ReservationNotification;
use Drupal\reservation\Entity\ReservationRessourceNode;
use Drupal\reservation\Event\EntityEvent;
use Drupal\reservation\Service\ReservationRessourceNodeServices;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Logs the creation of a new node.
 */
class ReservationEntitySubscriber implements EventSubscriberInterface {

  /**
   * Permet la création d'une entité RessourceNotification .
   *
   * @param mixed $nid
   * @param mixed $type_email
   *
   * @return \Drupal\Core\Entity\EntityBase|\Drupal\Core\Entity\EntityInterface
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function createRessourceNotificationByTypeEmail($nid, $type_email) {
    /** @var \Drupal\reservation\Service\ReservationNotificationServices $reservationNotification */
    $reservationNotification = \Drupal::service('reservation.notification');
    $exist_entity = $reservationNotification->getNotificationByNidType($nid, $type_email);
    if (!$exist_entity) {
      $reservationSettings = \Drupal::config('reservation.settings');
      $rows = $reservationSettings->get('notification');
      $row = $rows[$type_email];
      return self::createReservationNotificationEntity($nid, $type_email, $row);
    }
    else {
      return $exist_entity;
    }
  }

  /**
   * @param $nid
   * @param $type_email
   * @param $config
   *
   * @return \Drupal\Core\Entity\EntityBase|\Drupal\Core\Entity\EntityInterface
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private static function createReservationNotificationEntity($nid, $type_email, $config) {
    $entite = ReservationNotification::create([
      'nid' => $nid,
      'type_email' => $type_email,
      'statut' => $config['field']['statut'],
      'email_from' => $config['field']['from'],
      'email_to' => $config['field']['to'] ?? '',
      'email_cc' => $config['field']['cc'],
      'email_objet' => $config['field']['objet'],
      'email_corps' => $config['field']['corps'],
    ]);
    $entite->save();
    return $entite;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[EntityEvent::NODE_SAVE][] = ['onNodeInsert', 2];
    $events[EntityEvent::NODE_DELETE][] = ['onNodeDelete', 2];
    $events[EntityEvent::RESSOURCE_NODE_DELETE][] = [
      'onRessourceNodeDelete',
      2,
    ];
    $events[EntityEvent::RESERVATION_RESSOURCE_SAVE][] = [
      'onReservationRessourceSave',
      2,
    ];
    $events[EntityEvent::RESERVATION_DEMANDE_DELETE][] = ['onDemandeDelete', 2];
    $events[EntityEvent::WEBFORM_SUBMISSION_DELETE][] = [
      'onWebformSubmissionDelete',
      2,
    ];
    return $events;
  }

  /**
   * Permet la création d'une entité RessourceNode .
   *
   * @param \Drupal\reservation\Event\EntityEvent $event
   */
  private static function createRessourceNode($nid, $type) {
    /** @var \Drupal\reservation\Service\ReservationRessourceServices $reservationRessource */
    $reservationRessource = \Drupal::service('reservation.ressource');

    $ressourceNode = ReservationRessourceNode::load($nid);
    if (!$ressourceNode) {
      $ressource = $reservationRessource->getRessourceByType($type);
      $entite = ReservationRessourceNode::create([
        'nid' => $nid,
        'rrid' => $ressource->id(),
        'automatique' => FALSE,
        'statut' => TRUE,
        'caution_statut' => $ressource->getCautionStatut(),
        'caution_montant' => $ressource->getCautionMontant(),
      ]);
      $entite->save();
    }
  }

  /**
   * Permet la création d'une entité RessourceNotification .
   *
   * @param mixed $nid
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private static function createRessourceNotification($nid) {
    /** @var \Drupal\reservation\Service\ReservationNotificationServices $reservationNotification */
    $reservationNotification = \Drupal::service('reservation.notification');
    $reservationSettings = \Drupal::config('reservation.settings');
    $notifSettings = $reservationSettings->get('notification');
    foreach ($notifSettings as $typeNotif => $notifConfig) {
      $notification = $reservationNotification->getNotificationByNidType($nid, $typeNotif);
      if (!$notification) {
        self::createReservationNotificationEntity($nid, $typeNotif, $notifConfig);
      }
    }
  }

  /**
   * Permet la création des entités ReservationRessourceNode et
   * ReservationRessourceNotification, lors de la création d'un node.
   *
   * @param \Drupal\reservation\Event\EntityEvent $event
   */
  public function onNodeInsert(EntityEvent $event) {
    /** @var \Drupal\reservation\Service\ReservationRessourceServices $reservationRessource */
    $reservationRessource = \Drupal::service('reservation.ressource');

    $entity = $event->getEntity();
    $nid = $entity->id();
    $type = $entity->getType();

    $isReservableType = $reservationRessource->getTypeExist($type, '1');
    if ($isReservableType) {
      // $this->createRessourceNode($nid, $type);
      // $this->createRessourceNotification($nid);
    }
  }

  /**
   * Permet la suppression des entités ReservationRessourceNode lors de la
   * suppression d'un node.
   *
   * @param \Drupal\reservation\Event\EntityEvent $event
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function onNodeDelete(EntityEvent $event) {
    $entity = $event->getEntity();
    if (!empty($entity)) {
      $nid = $entity->id();
      self::deleteResourceNode($nid);
    }
  }

  /**
   * @param $nid
   *
   * @return bool
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private static function deleteResourceNode($nid) {
    $resourceNode = ReservationRessourceNodeServices::getNodeById($nid);
    if (!empty($resourceNode)) {
      $resourceNode->delete();
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Permet la suppression des users associés, notifications
   * ainsi que dates et horaires lors de la suppression d'un resoource_node.
   *
   * @param \Drupal\reservation\Event\EntityEvent $event
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function onRessourceNodeDelete(EntityEvent $event) {
    $entity = $event->getEntity();
    if (!empty($entity)) {
      $nid = $entity->id();
      self::deleteResourceUsers($nid);
      self::deleteResourceNotifications($nid);
      self::deleteResourceDatesHoraires($nid);
      // @todo suppression des demandes ?
    }
  }

  /**
   * @param $nid
   */
  private static function deleteResourceUsers($nid) {
    /** @var \Drupal\reservation\Service\ReservationRessourceUserServices $reservationRessourceUser */
    $reservationRessourceUser = \Drupal::service('reservation.ressource.user');
    $userResources = $reservationRessourceUser->getRessourceUserByNid($nid);
    foreach ($userResources as $userResource) {
      $userResource->delete();
    }
  }

  /**
   * Permet la création d'une entité RessourceNotification .
   *
   * @param mixed $nid
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private static function deleteResourceNotifications($nid) {
    /** @var \Drupal\reservation\Service\ReservationNotificationServices $reservationNotification */
    $reservationNotification = \Drupal::service('reservation.notification');
    $notifications = $reservationNotification->getNotificationsByNid($nid);
    foreach ($notifications as $notification) {
      $notification->delete();
    }
  }

  /**
   * @param $nid
   */
  private static function deleteResourceDatesHoraires($nid) {
    /** @var \Drupal\reservation\Service\ReservationDateServices $reservationDate */
    $reservationDate = \Drupal::service('reservation.date');
    /** @var \Drupal\reservation\Service\ReservationHoraireServices $horaireServices */
    $horaireServices = \Drupal::service('reservation.horaire');

    $rdids = $reservationDate->getRdidByNid($nid);
    $dates = $reservationDate->getAll($rdids);
    foreach ($dates as $date) {
      $reservationHoraires = $horaireServices->getByDate($date->id());
      foreach ($reservationHoraires as $horaire) {
        $horaire->delete();
      }
      $date->delete();
    }
  }

  /**
   * Permet la création des entités RessourceNode et RessourceNotification,
   * lors de la création d'une entité ReservationRessource.
   *
   * @param \Drupal\reservation\Event\EntityEvent $event
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function onReservationRessourceSave(EntityEvent $event) {
    $entity = $event->getEntity();
    $type = $entity->getType();
    $statut = $entity->getStatut();

    if ($statut == 1) {
      $nodes = \Drupal::entityTypeManager()
        ->getStorage('node')
        ->loadByProperties(['type' => $type]);
      foreach ($nodes as $node) {
        $nid = $node->id();
        // self::createRessourceNode($nid, $type);
        // self::createRessourceNotification($nid);
      }
    }
  }

  /**
   * Suppression des éléments associés à une demande de réservation (webform
   * submission, mail logs...)
   *
   * @param \Drupal\reservation\Event\EntityEvent $event
   */
  public function onDemandeDelete(EntityEvent $event) {
    /** @var \Drupal\reservation\Entity\ReservationDemande $reservationDemande */
    $reservationDemande = $event->getEntity();
    /** @var \Drupal\webform\Entity\WebformSubmission $webformSubmission */
    $webformSubmission = $reservationDemande->getWebformSubmission();
    if (!empty($webformSubmission)) {
      if ($webformSubmission->isDraft()) {
        // On supprime la référence à la demande qui a été supprimée pour
        // permettre à l'utilisateur de réutiliser sa saisie en cours pour
        // effectuer une nouvelle réservation.
        $data = $webformSubmission->getData();
        if (isset($data['webform_reservation_demande_id'])) {
          // Webform handler V1.
          $data['webform_reservation_demande_id'] = '';
        }
        if (isset($data['webform_reservation_config'])) {
          // Webform handler NG V2.
          $webform_reservation_config = $data['webform_reservation_config'];
          $webform_reservation_config['webform_reservation_demande_id'] = '';
          $data['webform_reservation_config'] = $webform_reservation_config;
        }
        $webformSubmission->setData($data);
        // Retour à l'accueil du formulaire.
        $webformSubmission->setCurrentPage('');
        $webformSubmission->save();
      }
      else {
        // On supprime la soumission si elle n'est pas à l'état draft.
        // Les drafts sont supprimés par l'outil d'auto-purge fourni par le
        // module webform.
        $webformSubmission->delete();
      }
    }
    // On purge aussi les logs des mails envoyés au cours de la vie de la
    // demande.
    /** @var \Drupal\reservation\Service\ReservationMailServices $mailServices */
    $mailServices = \Drupal::service('reservation.mail');
    $mailServices->deleteDemandeMailHistory($reservationDemande->id());
  }

  /**
   * Suppression des éléments associés une soumission webform de réservation
   * (demande, etc...)
   *
   * @param \Drupal\reservation\Event\EntityEvent $event
   */
  public function onWebformSubmissionDelete(EntityEvent $event) {
    $webform_submission = $event->getEntity();
    if (!empty($webform_submission)) {
      // On vérifie que la soumission correspond à une réservation.
      $data = $webform_submission->getData();
      if (isset($data['webform_reservation_demande_id'])) {
        // Webform handler V1.
        $demande_id = $data['webform_reservation_demande_id'];
      }
      if (isset($data['webform_reservation_config'])) {
        // Webform handler NG V2.
        $webform_reservation_config = $data['webform_reservation_config'];
        $demande_id = $webform_reservation_config['webform_reservation_demande_id'] ?? NULL;
      }
      // Si la soumission correspond bien à une réservation,
      // on supprime la réservation associée.
      if (!empty($demande_id)) {
        /** @var \Drupal\reservation\Service\ReservationDemandeServices $demandeServices */
        $demandeServices = \Drupal::service('reservation.demande');
        $demandeServices->deleteDemande($demande_id);
      }
    }
  }

}
