<?php

namespace Drupal\reservation\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\reservation\Entity\ReservationDemande;

/**
 * Class StateForm.
 *
 * @ingroup bat
 */
class DemandeAnnulationForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'demande_cancel_form';
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param $token
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function buildForm(array $form, FormStateInterface $form_state, $token = NULL) {
    $form['#title'] = 'Annulation de réservation';

    if (!empty($token)) {

      /** @var \Drupal\webform\WebformSubmissionStorageInterface $webform_submission_storage */
      $webform_submission_storage = \Drupal::entityTypeManager()
        ->getStorage('webform_submission');

      if ($entities = $webform_submission_storage->loadByProperties(['token' => $token])) {

        $webform_submission = reset($entities);
        $data = $webform_submission->getData();

        // Webform handler V1.
        $rdmid = $data['webform_reservation_demande_id'] ?? NULL;
        if (empty($rdmid) && isset($data['webform_reservation_config'])) {
          // Webform handler NG V2.
          $rdmid = $data['webform_reservation_config']['webform_reservation_demande_id'] ?? NULL;
        }

        /** @var \Drupal\reservation\Service\ReservationDemandeServices $demandeServices */
        $demandeServices = \Drupal::service('reservation.demande');
        $demande = empty($rdmid) ? NULL : $demandeServices->load($rdmid);

        if (isset($demande)) {
          if ($demande->getStatut() == ReservationDemande::STATUT_ANNULE) {
            $form['message'] = [
              '#type' => 'item',
              '#markup' => 'La réservation est annulée.',
            ];
          }
          else {
            $form['message'] = [
              '#type' => 'item',
              '#markup' => 'Etes vous sûr de vouloir d\'annuler votre réservation ?',
            ];
            $form['token'] = [
              '#type' => 'hidden',
              '#name' => 'token',
              '#value' => $token,
            ];
            $form['rdmid'] = [
              '#type' => 'hidden',
              '#name' => 'rdmid',
              '#value' => $rdmid,
            ];
            $form['confirmation'] = [
              '#type' => 'submit',
              '#name' => 'confirm',
              '#value' => 'Confirmer l\'annulation',
            ];
          }
        }
        else {
          $form['message'] = [
            '#type' => 'item',
            '#markup' => 'Code de réservation invalide ou expiré.',
          ];
        }
      }
      else {
        $form['message'] = [
          '#type' => 'item',
          '#markup' => 'Code de réservation invalide ou expiré.',
        ];
      }
    }
    else {
      $form['message'] = [
        '#type' => 'item',
        '#markup' => 'Code de réservation requis.',
      ];
    }

    return $form;
  }

  /**
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();

    if ($trigger['#name'] == 'confirm') {

      $token = $form_state->getValue('token');
      $rdmid = $form_state->getValue('rdmid');

      /** @var \Drupal\reservation\Service\ReservationDemandeServices $demandeServices */
      $demandeServices = \Drupal::service('reservation.demande');
      $demande = $demandeServices->load($rdmid);

      if ($demande->getWebformToken() === $token) {

        $demandeServices->annuleDemande($demande);

        /** @var \Drupal\reservation\Service\ReservationMailServices $mailServices */
        $mailServices = \Drupal::service('reservation.mail');
        $mailServices->generateEmailById($rdmid, ReservationDemande::STATUT_ANNULE, TRUE);

        \Drupal::messenger()
          ->addStatus(t('Votre réservation a bien été annulée.'));

      }
      else {

        \Drupal::messenger()
          ->addStatus(t("Nous n'avons pas pu annuler votre réservation."));

      }
    }
  }

}
