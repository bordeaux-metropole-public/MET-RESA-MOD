<?php

namespace Drupal\reservation\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\reservation\Entity\ReservationDemande;

/**
 * Class DemandeCautionBancaireForm.
 *
 * @package Drupal\reservation\Form
 */
class DemandeCautionBancaireForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'demande_caution_bancaire_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state,
                       $token = NULL,
    ReservationDemande $demande = NULL) {
    $reservationSettings = \Drupal::config('reservation.settings');
    $caution_settings = $reservationSettings->get('caution');

    $route = 'reservation.demande.caution.traitement';
    $fields = [
      'ORDERID' => $demande->id(),
      'AMOUNT' => $demande->getDate()
        ->getReservationRessourceNode()
        ->getCautionMontant() * 100,
      'CN' => $demande->getDemandeur(),
      'EMAIL' => $demande->getEmail(),
      'ACCEPTURL' => $this->urlReturn($route, ReservationDemande::CAUTION_ACCEPT, $token, $caution_settings),
      'DECLINEURL' => $this->urlReturn($route, ReservationDemande::CAUTION_DECLINE, $token, $caution_settings),
      'EXCEPTIONURL' => $this->urlReturn($route, ReservationDemande::CAUTION_EXCEPT, $token, $caution_settings),
      'CANCELURL' => $this->urlReturn($route, ReservationDemande::CAUTION_CANCEL, $token, $caution_settings),
    ];
    $form = $this->parametreForm($form, $fields);

    $form = $this->parametreForm($form, $caution_settings['input']);

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => t('Carte Bancaire'),
    ];

    $form['#action'] = $caution_settings['url']['send'];

    return $form;
  }

  /**
   * @param $route
   * @param $etat
   * @param $token
   * @param $caution_settings
   *
   * @return string
   */
  public function urlReturn($route, $etat, $token, $caution_settings) {
    $url_scheme_host = $caution_settings['url']['return_host'];
    if (empty($url_scheme_host)) {
      $scheme = \Drupal::request()->getScheme();
      $host = \Drupal::request()->getHost();
      $url_scheme_host = $scheme . '://' . $host;
    }
    return $url_scheme_host . Url::fromRoute($route, [
      'etat' => $etat,
      'caution' => ReservationDemande::CAUTION_CARTE,
      'token' => $token,
    ])->toString();
  }

  /**
   * @param $form
   * @param $rows
   *
   * @return mixed
   */
  public function parametreForm($form, $rows) {
    foreach ($rows as $key => $row) {
      $form = $this->hiddenForm($form, $key, $row);
    }

    return $form;
  }

  /**
   * @param $form
   * @param $key
   * @param $row
   *
   * @return mixed
   */
  public function hiddenForm($form, $key, $row) {
    $form[$key] = [
      '#type' => 'hidden',
      '#value' => $row,
    ];

    return $form;
  }

  /**
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}
