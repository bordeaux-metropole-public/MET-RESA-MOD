<?php

namespace Drupal\reservation\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\reservation\Entity\ReservationDemande;

/**
 * Class StateForm.
 *
 * @ingroup bat
 */
class DemandeConfirmationActionForm extends FormBase {

  /**
   * @var null
   */
  public $rdmid = NULL;

  /**
   * @var null
   */
  public $type = NULL;

  /**
   * @var null
   */
  public $action = NULL;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'demande_confirmation_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $request = \Drupal::requestStack()->getCurrentRequest();
    $requestParams = $request->query->all();
    $this->rdmid = $requestParams['rdmid'];
    $this->type = $requestParams['type'];
    $this->action = $requestParams['action'];

    switch ($this->action) {
      case ReservationDemande::STATUT_CONFIRME:
        $libelle_action = "confirmer les ";
        break;

      case ReservationDemande::STATUT_ANNULE:
        $libelle_action = "annuler les ";
        break;

      case ReservationDemande::STATUT_ATTENTE:
        $libelle_action = "mettre en attente les ";
        break;

      case ReservationDemande::STATUT_CAUTION:
        $libelle_action = "mettre en attente de caution les ";
        break;

      case ReservationDemande::STATUT_REFUSE:
        $libelle_action = "refuser les ";
        break;

      case ReservationDemande::STATUT_ARCHIVE:
        $libelle_action = "archiver les ";
        break;

      case ReservationDemande::STATUT_NO_SHOW:
        $libelle_action = "appliquer le statut noshow pour les ";
        break;

      case ReservationDemande::STATUT_SHOW:
        $show_label = t('Resa Show');
        $libelle_action = "appliquer le statut " . $show_label . " pour les ";
        break;

      case ReservationDemande::STATUT_RAPPEL:
        $libelle_action = "envoyer un mail de rappel aux ";
        break;

      case 'delete':
        $libelle_action = "supprimer les ";
        break;
    }

    $form['validation'] = [
      '#type' => 'item',
      '#markup' => 'Etes vous sûr de vouloir ' . $libelle_action . count($this->rdmid) . ' demande(s) ?',
    ];

    $form['oui'] = [
      '#type' => 'submit',
      '#name' => 'oui',
      '#value' => 'Oui',
    ];

    $form['annuler'] = [
      '#type' => 'submit',
      '#name' => 'annuler',
      '#value' => 'Annuler',
    ];

    return $form;
  }

  /**
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $request = \Drupal::requestStack()->getCurrentRequest()->query->all();
    $trigger = $form_state->getTriggeringElement();

    unset($request['action']);
    unset($request['rdmid']);
    unset($request['type']);

    if ($trigger['#name'] == 'oui') {
      $this->submitActionForm($form_state);
      $form_state->setRedirectUrl(Url::fromRoute('reservation.demande.' . $this->type, [$request]));
    }
    else {
      $form_state->setRedirectUrl(Url::fromRoute('reservation.demande.' . $this->type, [$request]));
    }

  }

  /**
   *
   */
  protected function submitActionForm(FormStateInterface $form_state) {
    $ids = $_GET['rdmid'];
    $mailServices = \Drupal::service('reservation.mail');
    switch ($this->action) {
      case ReservationDemande::STATUT_CONFIRME:
      case ReservationDemande::STATUT_ANNULE:
      case ReservationDemande::STATUT_ATTENTE:
      case ReservationDemande::STATUT_CAUTION:
      case ReservationDemande::STATUT_REFUSE:
      case ReservationDemande::STATUT_ARCHIVE:
      case ReservationDemande::STATUT_NO_SHOW:
      case ReservationDemande::STATUT_SHOW:
        $this->setStatut($this->action);
        break;

      case ReservationDemande::STATUT_RAPPEL:
        $mailServices->generateMultipleEmailById($ids, $this->action, TRUE);
        break;

      case 'delete':
        $demandeServices = \Drupal::service('reservation.demande');
        $count = $demandeServices->deleteMultipleDemande($this->rdmid);
        $this->messenger()
          ->addMessage($count . ' demande(s) supprimée(s)', 'info');
        break;
    }
  }

  /**
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param mixed $field
   */
  public function setStatut($field) {
    foreach ($this->rdmid as $rdmid) {
      if ($rdmid) {
        $reservationDemande = ReservationDemande::load($rdmid);
        $reservationDemande->set('statut', $field);
        $reservationDemande->save();
      }
    }
  }

}
