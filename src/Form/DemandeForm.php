<?php

namespace Drupal\reservation\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\reservation\Entity\ReservationDemande;
use Drupal\reservation\ReservationUtils;

/**
 * Class StateForm.
 *
 * @ingroup bat
 */
class DemandeForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'demande_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $request = \Drupal::requestStack()->getCurrentRequest();

    $form['#attached']['library'][] = 'reservation/reservation.demande';

    /** @var \Drupal\reservation\Service\ReservationRessourceServices $reservationRessource */
    $reservationRessource = \Drupal::service('reservation.ressource');
    /** @var \Drupal\reservation\Service\ReservationDemandeServices $reservationDemande */
    $reservationDemande = \Drupal::service('reservation.demande');
    /** @var \Drupal\reservation\Service\ReservationDemandeFormServices $demandeFormServices */
    $demandeFormServices = \Drupal::service('reservation.demande.form');
    /** @var \Drupal\reservation\Service\ReservationDemandeFormServices $demandeActionServices */
    $demandeActionServices = \Drupal::service('reservation.demande.form');
    /** @var \Drupal\reservation\Service\ReservationDemandeTokenServices $tokenServices */
    $tokenServices = \Drupal::service('reservation.demande.token');
    $tokenServices->destroyTokenObsolete();
    $reservationSettings = \Drupal::config('reservation.settings');
    $demandeSettings = $reservationSettings->get('demande');

    $requestParams = $request->query->all();
    $route_action = 'reservation.demande.action';
    $route_retour = 'reservation.demande.simple';

    // $resourceIds = \Drupal::request()->query->get('ressource'); // Fails on D10
    $resourceIds = $requestParams['ressource'] ?? NULL;
    // $statut = \Drupal::request()->query->get('statut'); // Fails on D10
    $statut = $requestParams['statut'] ?? NULL;
    $choix_date_demande = \Drupal::request()->query->get('choix_date_demande');
    $date_demande_debut = \Drupal::request()->query->get('date_demande_debut');
    $date_demande_fin = \Drupal::request()->query->get('date_demande_fin');
    $choix_date_creneau = \Drupal::request()->query->get('choix_date_creneau');
    $date_creneau_debut = \Drupal::request()->query->get('date_creneau_debut');
    $date_creneau_fin = \Drupal::request()->query->get('date_creneau_fin');
    $caution = \Drupal::request()->query->get('caution');

    $userResourceIds = ReservationUtils::getUserRessourceIds();
    if ($resourceIds == NULL) {
      if (ReservationUtils::canViewOwnDemandes() ||
        ReservationUtils::canManageOwnResourcesReservations()) {
        // Set #default_value to user accessible resources.
        $resourceIds = $userResourceIds;
      }
    }
    else {
      if (ReservationUtils::canViewOnlyOwnDemandes() ||
        ReservationUtils::canManageOnlyOwnResourcesReservations()) {
        // Filter out non user accessible resources from #default_value.
        $resourceIds = array_intersect($resourceIds, $userResourceIds);
      }
    }

    if ($statut == NULL) {
      $statut = ['confirme', 'attente', 'caution'];
    }

    if ($choix_date_demande === NULL) {
      $choix_date_demande = TRUE;
    }

    if ($date_demande_debut == NULL) {
      $date_demande_debut_format = new \DateTime();
      $date_demande_debut_format->sub(new \DateInterval('P1M'));
      $date_demande_debut = $date_demande_debut_format->format('Y-m-d');
    }
    else {
      $date_demande_debut_format = new \DateTime($date_demande_debut);
    }

    if ($date_demande_fin == NULL) {
      $date_demande_fin_format = new \DateTime();
      $date_demande_fin_format->add(new \DateInterval('P1D'));
      $date_demande_fin = $date_demande_fin_format->format('Y-m-d');
    }
    else {
      $date_demande_fin_format = new \DateTime($date_demande_fin);
    }

    if ($choix_date_creneau === NULL) {
      $choix_date_creneau = FALSE;
    }

    if ($date_creneau_debut == NULL) {
      $date_creneau_debut_format = new \DateTime();
      $date_creneau_debut = $date_creneau_debut_format->format('Y-m-d');
    }
    else {
      $date_creneau_debut_format = new \DateTime($date_creneau_debut);
    }

    if ($date_creneau_fin == NULL) {
      $date_creneau_fin_format = new \DateTime();
      $date_creneau_fin_format->add(new \DateInterval('P1M'));
      $date_creneau_fin = $date_creneau_fin_format->format('Y-m-d');
    }
    else {
      $date_creneau_fin_format = new \DateTime($date_creneau_fin);
    }

    $availableRessources = $reservationRessource->getAllRessourcesDemandees();
    if (ReservationUtils::canManageAllRessourcesReservations()) {
      // No ressources filtering constraint.
    }
    else {
      if (ReservationUtils::canManageOwnResourcesReservations()) {
        $filteredRessources = [];
        foreach ($availableRessources as $nid => $resource) {
          if (in_array($nid, $userResourceIds)) {
            $filteredRessources[$nid] = $resource;
          }
        }
        $availableRessources = $filteredRessources;
      }
    }

    $form['table_filter'] = [
      '#type' => 'table',
      '#tableselect' => FALSE,
    ];

    $form['table_filter'][1]['ressource'] = [
      '#type' => 'select',
      '#multiple' => 'true',
      '#title' => 'Ressource : ',
      '#options' => $availableRessources,
      '#empty_value' => '',
      '#default_value' => $resourceIds,
    ];

    $form['table_filter'][1]['statut'] = [
      '#type' => 'checkboxes',
      '#title' => 'Statut',
      '#options' => $demandeFormServices->getStatutTab(
        $resourceIds, $choix_date_demande, $date_demande_debut, $date_demande_fin,
        $choix_date_creneau, $date_creneau_debut, $date_creneau_fin, $caution),
      '#default_value' => $statut,
    ];

    $form['table_filter'][1]['table_date_demande'] = [
      '#type' => 'table',
      '#tableselect' => FALSE,
    ];

    $form['table_filter'][1]['table_date_demande'][1]['choix_date_demande'] = [
      '#type' => 'checkbox',
      '#title' => 'Date Demande',
      '#default_value' => $choix_date_demande,
    ];

    $form['table_filter'][1]['table_date_demande'][2]['date_demande_debut'] = [
      '#type' => 'date',
      '#title' => 'Début',
      '#value' => $date_demande_debut_format->format("Y-m-d"),
    ];

    $form['table_filter'][1]['table_date_demande'][3]['date_demande_fin'] = [
      '#type' => 'date',
      '#title' => 'Fin',
      '#value' => $date_demande_fin_format->format("Y-m-d"),
    ];

    $form['table_filter'][1]['table_date_creneau'] = [
      '#type' => 'table',
      '#tableselect' => FALSE,
    ];

    $form['table_filter'][1]['table_date_creneau'][1]['choix_date_creneau'] = [
      '#type' => 'checkbox',
      '#title' => 'Date Créneau',
      '#default_value' => $choix_date_creneau,
    ];

    $form['table_filter'][1]['table_date_creneau'][2]['date_creneau_debut'] = [
      '#type' => 'date',
      '#title' => 'Début',
      '#value' => $date_creneau_debut_format->format("Y-m-d"),
    ];

    $form['table_filter'][1]['table_date_creneau'][3]['date_creneau_fin'] = [
      '#type' => 'date',
      '#title' => 'Fin',
      '#value' => $date_creneau_fin_format->format("Y-m-d"),
    ];

    if ($demandeSettings['caution'] == TRUE) {
      $selections = [
        ReservationDemande::CAUTION_NON_APPLICABLE => t('N/A'),
        ReservationDemande::CAUTION_NON_RENSEIGNEE => t('Non Renseigné'),
        ReservationDemande::CAUTION_CARTE => t('Carte Bancaire'),
        ReservationDemande::CAUTION_CHEQUE => t('Chèque'),
      ];
      $form['table_filter'][1]['caution'] = [
        '#type' => 'select',
        '#multiple' => 'true',
        '#title' => 'Ressource : ',
        '#options' => $selections,
        '#empty_value' => '',
        '#default_value' => $caution,
      ];
    }

    $form['table_filter'][1]['filtre'] = [
      '#type' => 'submit',
      '#name' => 'filtre',
      '#value' => 'Filtrer',
    ];

    $form['table_filter'][1]['reset'] = [
      '#type' => 'submit',
      '#name' => 'reset',
      '#value' => 'Reset',
    ];

    $form = $demandeActionServices->tableActionForm($form);

    // La méthode submitForm fait un redirect pour l'affichage des demandes
    // Donc aucun intérêt de faire un pré-rendu avant le redirect.
    // On affiche donc les demandes que pour les requêtes GET.
    if ($request->getMethod() !== 'POST') {

      $form = $demandeActionServices->tableActionForm($form);

      $form['table_action'][0]['print'] = [
        $this->createButtonAction('reservation.demande.export.simple', 'Export CSV des demandes issues du filtre', 'download', [$requestParams]),
      ];

      $output = [];
      $header = $demandeFormServices->getHeaderTableSimple();
      if (!empty($resourceIds)) {
        $demandes = $reservationDemande->getDemandesFiltered(
          $header, 15, $resourceIds, $statut, $choix_date_demande,
          $date_demande_debut, $date_demande_fin, $choix_date_creneau,
          $date_creneau_debut, $date_creneau_fin, $caution);
      }
      else {
        $demandes = [];
      }

      foreach ($demandes as $demande) {
        $webform_submission = $demande->getWebformSubmission();
        if ($webform_submission) {
          $route_show = 'entity.webform_submission.canonical';
          $route_edit = 'entity.webform_submission.edit_form';
          $button_parameter = array_merge([
            'webform' => 'reservation_le_hamac',
            'webform_submission' => $webform_submission->id(),
            'rdmid' => $demande->Id(),
            'url' => $route_retour,
          ], $requestParams);
        }
        else {
          $route_show = 'reservation.demande.edit';
          $route_edit = 'reservation.demande.edit';
          $button_parameter = array_merge([
            'rdmid' => $demande->Id(),
            'url' => $route_retour,
          ], $requestParams);
        }
        $button_show = $this->createButtonAction($route_show, 'Voir la demande', 'eye', $button_parameter);
        $button_edition = $this->createButtonAction($route_edit, 'Editer', 'pencil', $button_parameter);

        $output[$demande->Id()]['#attributes'] = ['class' => ['row-statut-' . $demande->getStatut()]];
        $output[$demande->Id()]['ressource'] = $demande->getTitleNode();
        $output[$demande->Id()]['datedemande'] = $demande->getCreatedFormat();
        $output[$demande->Id()]['demandeur'] = $demande->getDemandeur();
        $output[$demande->Id()]['email'] = $demande->getEmail();
        $output[$demande->Id()]['telephone'] = $demande->getTelephone();
        $output[$demande->Id()]['postcode'] = $demande->getPostcode();
        $output[$demande->Id()]['datecreneau'] = $demande->getDateCreneau();

        if ($demandeSettings['caution'] == TRUE) {
          if ($demande->getCaution() == ReservationDemande::CAUTION_NON_APPLICABLE) {
            $label = $demande->getCautionLabel();
          }
          elseif ($demande->getStatut() == 'caution') {
            $label = Markup::create('<em class="fa fa-exclamation-triangle fa-2x fa-fw"></em> ' . $demande->getCautionLabel() . ' ' . $demande->getCautionDelay());
          }
          else {
            $label = $demande->getCautionLabel();
          }
          $output[$demande->Id()]['caution'] = $label;
        }

        if ($demandeSettings['inscrit'] == TRUE) {
          $output[$demande->Id()]['jauge'] = $demande->getJauge();
        }
        $output[$demande->Id()]['statut'] = $demandeFormServices->getStatutFormat($demande->getStatut());
        if (!empty($demandeSettings['webform_key'])) {
          $webformFields = explode('|', $demandeSettings['webform_key']);
          $data = $demande->getWebformSubmissionData();
          foreach ($webformFields as $webformField) {
            $output[$demande->Id()][$webformField] = $data[$webformField] ?? '';
          }
        }

        $output[$demande->Id()]['actions'] = [
          'data' =>
            [
              $button_show,
              $button_edition,
              $this->createButtonAction($route_action, 'Confirmer', 'check-circle',
                array_merge([
                  'rdmid' => $demande->Id(),
                  'action' => 'confirme',
                  'route' => $route_retour,
                ], $requestParams)),
              $this->createButtonAction($route_action, 'Mettre en Attente', 'hourglass-half',
                array_merge([
                  'rdmid' => $demande->Id(),
                  'action' => 'attente',
                  'route' => $route_retour,
                ], $requestParams)),
              $this->createButtonAction($route_action, 'Caution', 'credit-card',
                array_merge([
                  'rdmid' => $demande->Id(),
                  'action' => 'caution',
                  'route' => $route_retour,
                ], $requestParams)),
              $this->createButtonAction($route_action, 'Refuser', 'times-circle',
                array_merge([
                  'rdmid' => $demande->Id(),
                  'action' => 'refuse',
                  'route' => $route_retour,
                ], $requestParams)),
              $this->createButtonAction($route_action, 'Rappel', 'envelope',
                array_merge([
                  'rdmid' => $demande->Id(),
                  'action' => 'rappel',
                  'route' => $route_retour,
                ], $requestParams)),
              $this->createButtonAction($route_action, 'Annuler', 'ban',
                array_merge([
                  'rdmid' => $demande->Id(),
                  'action' => 'annule',
                  'route' => $route_retour,
                ], $requestParams)),
              $this->createButtonAction($route_action, 'Archiver', 'save',
                array_merge([
                  'rdmid' => $demande->Id(),
                  'action' => 'archive',
                  'route' => $route_retour,
                ], $requestParams)),
              $this->createButtonAction($route_action, 'Mode NoShow', 'eye-slash',
                array_merge([
                  'rdmid' => $demande->Id(),
                  'action' => 'noshow',
                  'route' => $route_retour,
                ], $requestParams)),
              $this->createButtonAction($route_action, t('Mode Show'), 'thumbs-up',
                array_merge([
                  'rdmid' => $demande->Id(),
                  'action' => 'show',
                  'route' => $route_retour,
                ], $requestParams)),
              $this->createButtonAction('reservation.demande.delete', 'Supprimer la demande', 'trash-alt',
                array_merge([
                  'rdmid' => [$demande->Id()],
                  'type' => 'simple',
                  'url' => $route_retour,
                ], $requestParams)),
            ],
        ];
      }

      $form['table_demande'] = [
        '#type' => 'tableselect',
        '#header' => $header,
        '#options' => $output,
        '#empty' => 'Aucunes demandes liées à cette recherche',
      ];

      $form['pager'] = [
        '#type' => 'pager',
      ];
    }

    return $form;
  }

  /**
   *
   * @param mixed $url_action
   * @param mixed $icon
   * @param mixed $parameter
   *
   * @return mixed
   */
  public function createButtonAction($url_action, $action, $icon, $parameter) {
    return [
      '#type' => 'link',
      '#title' => Markup::create(' <div class="tooltip"><em class="fa fa-' . $icon . ' fa-2x fa-fw"></em><span class="tooltiptext">' . $action . '</span></div>'),
      '#url' => Url::fromRoute($url_action, $parameter),
    ];
  }

  /**
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Appel de la méthode commune (formulaire demande simple et multiple) pour
    // agir sur les demandes.
    /** @var \Drupal\reservation\Service\ReservationDemandeFormServices $demandeFormServices */
    $demandeFormServices = \Drupal::service('reservation.demande.form');
    $demandeFormServices->submitActionForm($form_state, 'simple');
  }

}
