<?php

namespace Drupal\reservation\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\reservation\Entity\ReservationRessourceNode;

/**
 * Class StateForm.
 *
 * @ingroup bat
 */
class DisponibiliteCautionForm extends FormBase {

  /**
   * @var mixed
   */
  protected $nid;

  /**
   * DisponibiliteCautionForm constructor.
   *
   * @param $nid
   */
  public function __construct($nid) {
    $this->nid = $nid;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'disponibilite_caution_form_' . $this->nid;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ReservationRessourceNode $ressourceNode = NULL) {

    $form['caution_statut'] = [
      '#type' => 'checkbox',
      '#title' => t('Caution'),
      '#id' => 'edit-caution-statut--' . $this->nid,
      '#default_value' => $ressourceNode ? $ressourceNode->getCautionStatut() : '',
      '#ajax' => [
        'callback' => '::setCaution',
        'wrapper' => 'mode',
        'event' => 'change',
      ],
    ];

    $form['caution_cheque_accepte'] = [
      '#type' => 'checkbox',
      '#title' => t('Chèque accepté'),
      '#id' => 'edit-caution-cheque-accepte--' . $this->nid,
      '#default_value' => $ressourceNode ? $ressourceNode->isChequeAccepte() : '',
      '#ajax' => [
        'callback' => '::setChequeAccepte',
        'wrapper' => 'mode',
        'event' => 'change',
      ],
    ];

    $form['caution_cheque_accepte']['#states'] = [
      'visible' => [
        ':input[id="edit-caution-statut--' . $this->nid . '"]' => [
          'checked' => TRUE,
        ],
      ],
    ];

    $form['caution_montant'] = [
      '#type' => 'number',
      '#id' => 'edit-caution-montant--' . $this->nid,
      '#default_value' => $ressourceNode ? $ressourceNode->getCautionMontant() : '',
      '#ajax' => [
        'callback' => '::setMontant',
        'wrapper' => 'mode',
        'event' => 'change',
      ],
    ];

    $form['caution_montant']['#states'] = [
      'visible' => [
        ':input[id="edit-caution-statut--' . $this->nid . '"]' => [
          'checked' => TRUE,
        ],
      ],
    ];

    return $form;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function setCaution(array $form, FormStateInterface $form_state) {
    $caution_statut = $form_state->getValue('caution_statut');

    $ressourcenode = ReservationRessourceNode::load($this->nid);
    $ressourcenode->setCautionStatut($caution_statut);
    $ressourcenode->save();

    $response = new AjaxResponse();
    $response->addCommand(new CloseModalDialogCommand());
    return $response;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function setChequeAccepte(array $form, FormStateInterface $form_state) {
    $cheque_accepte = $form_state->getValue('caution_cheque_accepte');

    $ressourcenode = ReservationRessourceNode::load($this->nid);
    $ressourcenode->setChequeAccepte($cheque_accepte);
    $ressourcenode->save();

    $response = new AjaxResponse();
    $response->addCommand(new CloseModalDialogCommand());
    return $response;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function setMontant(array $form, FormStateInterface $form_state) {
    $caution_montant = $form_state->getValue('caution_montant');

    $ressourcenode = ReservationRessourceNode::load($this->nid);
    $ressourcenode->setCautionMontant($caution_montant);
    $ressourcenode->save();

    $response = new AjaxResponse();
    $response->addCommand(new CloseModalDialogCommand());
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}
