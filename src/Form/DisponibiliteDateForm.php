<?php

namespace Drupal\reservation\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\reservation\Entity\ReservationDate;
use Drupal\reservation\ReservationUtils;

/**
 * Class StateForm.
 *
 * @ingroup bat
 */
class DisponibiliteDateForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'disponibilite_date_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /*
     *  Initialisation des variables et librairies
     *
     */

    /** @var \Drupal\reservation\Service\ReservationRessourceServices $reservationRessource */
    $reservationRessource = \Drupal::service('reservation.ressource');
    /** @var \Drupal\reservation\Service\ReservationDateServices $reservationDate */
    $reservationDate = \Drupal::service('reservation.date');

    $jour_fr = [
      1 => 'Lu',
      2 => 'Ma',
      3 => 'Me',
      4 => 'Je',
      5 => 'Ve',
      6 => 'Sa',
      7 => 'Di',
    ];

    $mois_fr = [
      0 => 'Janvier',
      1 => 'Février',
      2 => 'Mars',
      3 => 'Avril',
      4 => 'Mai',
      5 => 'Juin',
      6 => 'Juillet',
      7 => 'Août',
      8 => 'Septembre',
      9 => 'Octobre',
      10 => 'Novembre',
      11 => 'Décembre',
    ];

    $form['#attached']['library'][] = 'reservation/reservation_date';

    $availableResources = $reservationRessource->getAvailableRessources();

    $ressourceId = \Drupal::request()->query->get('nid');
    $year = \Drupal::request()->query->get('year');

    if (($ressourceId == NULL) || !ReservationUtils::isAccessibleResource($ressourceId)) {
      $ressourceId = current(array_keys($availableResources));
    }

    if ($year == NULL) {
      $date_now = new \DateTime();
      $year = $date_now->format('Y');
    }

    /*
     *  Génération des filtres Ressource et année
     */

    $form['table_filtre'] = [
      '#type' => 'table',
      '#tableselect' => FALSE,
    ];

    $form['table_filtre'][0]['select_ressource'] = [
      '#type' => 'select',
      '#title' => 'Ressource : ',
      '#options' => $availableResources,
      '#default_value' => $ressourceId,
      '#ajax' => [
        'callback' => '::redirectPage',
        'wrapper' => 'mode',
      ],
    ];

    $range_year = range($reservationDate->getYearMin(), $reservationDate->getYearMax() + 1);
    $form['table_filtre'][0]['select_year'] = [
      '#type' => 'select',
      '#title' => 'Année : ',
      '#options' => array_combine($range_year, $range_year),
      '#default_value' => $year,
      '#ajax' => [
        'callback' => '::redirectPage',
        'wrapper' => 'mode',
      ],
    ];

    /*
     *  Génération du tableau de dates
     */

    $form['table_legend'] = [
      '#type' => 'table',
      '#header' => ['Légende'],
      '#tableselect' => FALSE,
    ];

    $form['table_legend'][0]['past'] = [
      '#type' => 'item',
      '#markup' => 'Jour passé',
      '#wrapper_attributes' => ['class' => ['disponibilite-day-past']],
    ];

    $form['table_legend'][0]['attente'] = [
      '#type' => 'item',
      '#markup' => 'Jour non actif et non publié',
      '#wrapper_attributes' => ['class' => ['disponibilite-day-attente']],
    ];

    $form['table_legend'][0]['active'] = [
      '#type' => 'item',
      '#markup' => 'Jour actif et non publié',
      '#wrapper_attributes' => ['class' => ['disponibilite-day-active']],
    ];

    $form['table_legend'][0]['publie'] = [
      '#type' => 'item',
      '#markup' => 'Jour actif et publié',
      '#wrapper_attributes' => ['class' => ['disponibilite-day-publie']],
    ];

    $datesExists = $reservationDate->getFormDatesExist($ressourceId, $year);

    $start = new \DateTime(date("Y-01-01"));
    $date_now = new \DateTime();

    $interval_month = new \DateInterval("P1M");
    $period_month = new \DatePeriod($start, $interval_month, 11);

    $headers = ["Publier", "Dépublier", "mois", "Tout"];

    $form['table_date'] = [
      '#type' => 'table',
      '#header' => array_merge($headers, range(1, 31)),
      '#tableselect' => FALSE,
    ];

    foreach ($period_month as $month) {
      $month_word[] = $month->format("M");
      $month_num[] = $month->format("m");
    }

    foreach ($month_num as $key => $month) {

      $start = \DateTime::createFromFormat("Ymd", $year . $month . '01');
      $interval_day = new \DateInterval("P1D");

      $period_day = new \DatePeriod($start, $interval_day, $start->format("t") - 1);
      $disable_month = $start->format('Y-m') < $date_now->format('Y-m') ? TRUE : FALSE;

      $form['table_date'][$month]['publie'] = [
        '#type' => 'checkbox',
        '#default_value' => $reservationDate->getPublieMonth($year, $month, $ressourceId),
      ];

      $form['table_date'][$month]['publie'] = [
        '#type' => 'submit',
        '#value' => 'Publier',
        '#name' => 'submit-publie-' . $month,
        '#disabled' => $disable_month,
      ];

      $form['table_date'][$month]['depublie'] = [
        '#type' => 'submit',
        '#value' => 'Dépublier',
        '#name' => 'submit-depublie-' . $month,
        '#disabled' => $disable_month,
      ];

      $form['table_date'][$month]['MONTH'] = [
        '#type' => 'label',
        '#title' => $mois_fr[$key],
      ];

      $form['table_date'][$month]['ALL'] = [
        '#type' => 'checkbox',
        '#default_value' => FALSE,
        '#disabled' => $disable_month,
      ];

      foreach ($period_day as $day) {

        $dayInThePast = $day->format('Y-m-d') < $date_now->format('Y-m-d');

        $d = $day->format("d");

        $statutDefaultValue = FALSE;
        $isDatePubliee = FALSE;
        if (isset($datesExists[$month][$d])) {
          if (isset($datesExists[$month][$d]['statut'])) {
            $statutDefaultValue = $datesExists[$month][$d]['statut'] ? TRUE : FALSE;
          }
          if (isset($datesExists[$month][$d]['publie'])) {
            $isDatePubliee = $datesExists[$month][$d]['publie'] == 1 ? TRUE : FALSE;
          }
        }

        if ($dayInThePast) {
          $class = 'disponibilite-day-past';
        }
        else {
          if ($isDatePubliee) {
            $class = 'disponibilite-day-publie';
          }
          else {
            if ($statutDefaultValue) {
              $class = 'disponibilite-day-active';
            }
            else {
              $class = 'disponibilite-day-attente';
            }
          }
        }

        $statut = $dayInThePast ? 'no' : 'statut';
        $form['table_date'][$month][$d][$statut] = [
          '#type' => 'checkbox',
          '#default_value' => $statutDefaultValue,
          '#title' => $jour_fr[$day->format("N")] . ' ' . $day->format("d"),
          '#disabled' => $dayInThePast,
          '#wrapper_attributes' => ['class' => [$class]],
        ];
        if (isset($datesExists[$month][$d]['rdid']) && $datesExists[$month][$d]['rdid']) {
          $form['table_date'][$month][$d]['rdid'] = [
            '#type' => 'hidden',
            '#value' => $datesExists[$month][$d]['rdid'],
          ];
        }
      }

      $form['nid'] = [
        '#type' => 'hidden',
        '#value' => $ressourceId,
      ];

      $form['year'] = [
        '#type' => 'hidden',
        '#value' => $year,
      ];

      $form['submit'] = [
        '#type' => 'submit',
        '#value' => 'Sauvegarder',
        '#name' => 'submit-date',
      ];
    }

    return $form;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function redirectPage(array $form, FormStateInterface $form_state) {

    $nid = $form_state->getValue(['table_filtre', '0', 'select_ressource']);
    $year = $form_state->getValue(['table_filtre', '0', 'select_year']);

    $response = new AjaxResponse();
    $url = Url::fromRoute('reservation.disponibilite.date',
      ['nid' => $nid, 'year' => $year],
      ["absolute" => TRUE])->toString();

    $response->addCommand(new RedirectCommand($url));

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect(Url::fromRoute('reservation.disponibilite.index', ['choix' => 'user']));
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();

    $nid = $form_state->getValue('nid');
    $year = $form_state->getValue('year');

    preg_match('/submit-(.*)-(.*)/', $trigger['#name'], $choix);

    if (count($choix) >= 3) {
      if ($choix[1] === 'publie') {
        $this->publieForm($form, $form_state, $choix[2]);
      }
      elseif ($choix[1] === 'depublie') {
        $this->dePublieForm($form, $form_state, $choix[2]);
      }
      else {
        $this->monthForm($form, $form_state);
      }
    }
    else {
      $this->monthForm($form, $form_state);
    }

    $form_state->setRedirectUrl(Url::fromRoute('reservation.disponibilite.date', [
      'nid' => $nid,
      'year' => $year,
    ]));
  }

  /**
   * {@inheritdoc}
   */
  public function publieForm(array &$form, FormStateInterface $form_state, string $month) {
    $nid = $form_state->getValue('nid');
    $year = $form_state->getValue('year');

    $dateServices = \Drupal::service('reservation.date');
    $dates = $dateServices->getFormDates($nid, $year . '-' . $month, '1');
    foreach ($dates as $date) {
      $date->setPublie(TRUE);
      $date->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function dePublieForm(array &$form, FormStateInterface $form_state, string $month) {
    $nid = $form_state->getValue('nid');
    $year = $form_state->getValue('year');

    $dateServices = \Drupal::service('reservation.date');
    $dates = $dateServices->getFormDates($nid, $year . '-' . $month, NULL, TRUE);
    foreach ($dates as $date) {
      $date->setPublie(0);
      $date->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function monthForm(array &$form, FormStateInterface $form_state) {
    $nid = $form_state->getValue('nid');
    $year = $form_state->getValue('year');

    // Boucle mois.
    foreach ($form["table_date"]["#value"] as $month => $form_month) {
      // Boucle jour.
      foreach ($form_month as $day => $form_day) {
        if ($day !== "ALL") {
          // Cas d'une date déjà présente rdid non null.
          if (isset($form_day['rdid'])) {
            $entite = ReservationDate::load($form_day['rdid']);
            $entite->setStatut($form_day['statut'] ?? '0');
            $entite->save();
          }
          else {
            if ($form_day['statut']) {
              $date = \DateTime::createFromFormat("Y-m-d", $year . "-" . $month . "-" . $day);

              $entite = ReservationDate::create([
                'date' => $date->format('Y-m-d\Th:i:s'),
                'nid' => $nid,
                'statut' => $form_day['statut'] ?? '0',
              ]);
              $entite->save();
            }
          }
        }
      }
    }
  }

}
