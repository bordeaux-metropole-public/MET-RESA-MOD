<?php

namespace Drupal\reservation\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AppendCommand;
use Drupal\Core\Ajax\CssCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\reservation\Entity\ReservationDate;
use Drupal\reservation\Entity\ReservationHoraire;

/**
 * Class StateForm.
 *
 * @ingroup bat
 */
class DisponibiliteHoraireTableAddForm extends FormBase {

  /**
   * @var mixed
   */
  protected $rdid;

  /**
   * @var mixed
   */
  protected $ressourceId;

  /**
   * @var mixed
   */
  protected $month;

  /**
   * @var mixed
   */
  protected $year;

  /**
   * DisponibiliteHoraireTableAddForm constructor.
   *
   * @param $rdid
   * @param $nid
   * @param $month
   * @param $year
   */
  public function __construct($rdid, $nid, $month, $year) {
    $this->rdid = $rdid;
    $this->ressourceId = $nid;
    $this->month = $month;
    $this->year = $year;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'disponibilite_horaire_table_add_form_' . $this->rdid;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['table_horaire'] = [
      '#type' => 'reservation_div_table',
      '#header' => ['', 'Heure début', 'Heure fin', 'Jauge', '', 'Actions'],
      '#tableselect' => FALSE,
      '#table' => TRUE,
    ];

    $form['table_horaire'][$this->rdid]['creneau'] = [
      '#type' => 'item',
      '#markup' => '*',
    ];

    $form['table_horaire'][$this->rdid]['heure_debut'] = [
      '#type' => 'time',
      '#required' => TRUE,
    ];

    $form['table_horaire'][$this->rdid]['heure_fin'] = [
      '#type' => 'time',
      '#required' => TRUE,
    ];

    $form['table_horaire'][$this->rdid]['jauge_statut'] = [
      '#type' => 'checkbox',
      '#default_value' => TRUE,
    ];

    $form['table_horaire'][$this->rdid]['jauge_nombre'] = [
      '#type' => 'number',
      '#default_value' => 1,
      '#states' => [
        'enabled' => [
          ':input[name="table_horaire[' . $this->rdid . '][jauge_statut]"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    $form['table_horaire'][$this->rdid]['actions'] = [
      '#type' => 'button',
      '#value' => $this->t('Add'),
      '#ajax' => [
        'event' => 'click',
        'callback' => '::addLineAjax',
        'progress' => [
          'type' => 'throbber',
          'message' => NULL,
        ],
      ],
      '#suffix' => '<span class="valid-message_' . $this->rdid . '"></span>',
    ];

    return $form;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function addLineAjax(array &$form, FormStateInterface $form_state) {
    $validate = TRUE;

    $message = NULL;

    $css_valid = ['border' => '1px solid'];
    $css_error = ['border' => '2px solid red'];

    $heure_debut = $form_state->getValue([
      'table_horaire',
      $this->rdid,
      'heure_debut',
    ]);
    $heure_fin = $form_state->getValue([
      'table_horaire',
      $this->rdid,
      'heure_fin',
    ]);

    $response = new AjaxResponse();

    if ($heure_debut >= $heure_fin) {
      $message .= $this->t("<p>L'heure de fin doit être supérieure à l'heure de début.</p>");
      $response->addCommand(new CssCommand('#edit-table-horaire-' . $this->rdid . '-heure-debut', $css_error));
      $response->addCommand(new CssCommand('#edit-table-horaire-' . $this->rdid . '-heure-fin', $css_error));
      $validate = FALSE;
    }

    $jauge_statut = $form_state->getValue([
      'table_horaire',
      $this->rdid,
      'jauge_statut',
    ]);
    if ($jauge_statut == 1) {
      $jauge_nombre = $form_state->getValue([
        'table_horaire',
        $this->rdid,
        'jauge_nombre',
      ]);
      if ($jauge_nombre <= 0) {
        $message .= $this->t('<p>La jauge doit être supérieure à zéro.</p>');
        $response->addCommand(new CssCommand('#edit-table-horaire-' . $this->rdid . '-jauge-nombre', $css_error));
        $validate = FALSE;
      }
    }

    if ($validate) {

      $horaire = $this->createHoraire($form, $form_state);
      $formEditHoraire = new DisponibiliteHoraireTableEditForm(
        $this->rdid, $horaire->id(), $this->ressourceId, $this->month, $this->year);
      $formEditHoraireRender = \Drupal::formBuilder()
        ->getForm($formEditHoraire, $horaire, 1);

      $response->addCommand(new AppendCommand(
        '#disponibilite-horaire-table-' . $this->rdid . ' [data-resa-type="tbody"]', $formEditHoraireRender));

      $response->addCommand(new InvokeCommand(
          '#disponibilite-horaire-table-header-' . $this->rdid, 'removeClass', ['resa-hide'])
      );

      $response->addCommand(new CssCommand('#edit-table-horaire-' . $this->rdid . '-heure-debut', $css_valid));
      $response->addCommand(new CssCommand('#edit-table-horaire-' . $this->rdid . '-heure-fin', $css_valid));
      $response->addCommand(new CssCommand('#edit-table-horaire-' . $this->rdid . '-jauge-nombre', $css_valid));
    }

    $response->addCommand(new HtmlCommand('.valid-message_' . $this->rdid, $message));

    return $response;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function createHoraire(array $form, FormStateInterface $form_state) {
    $jauge_statut = $form_state->getValue([
      'table_horaire',
      $this->rdid,
      'jauge_statut',
    ]);
    $jauge_nombre = $form_state->getValue([
      'table_horaire',
      $this->rdid,
      'jauge_nombre',
    ]);
    $heure_debut = $form_state->getValue([
      'table_horaire',
      $this->rdid,
      'heure_debut',
    ]);
    $heure_fin = $form_state->getValue([
      'table_horaire',
      $this->rdid,
      'heure_fin',
    ]);

    $reservationDate = ReservationDate::load($this->rdid);
    $heureDebut = $this->getJourHeureDateTime($reservationDate, $heure_debut);
    $heureFin = $this->getJourHeureDateTime($reservationDate, $heure_fin);

    $reservationHoraire = ReservationHoraire::create([
      'rdid' => $this->rdid,
      'statut' => '1',
      'jauge_statut' => $jauge_statut,
      'jauge_nombre' => $jauge_nombre,
    ]
    );
    $reservationHoraire->setHeureDebut($heureDebut);
    $reservationHoraire->setHeureFin($heureFin);
    $reservationHoraire->save();

    return $reservationHoraire;
  }

  /**
   * @param $reservationDate
   * @param $heure
   *
   * @return \DateTime|false
   * @throws \Exception
   */
  private function getJourHeureDateTime($reservationDate, $heure) {
    $date = new \DateTime($reservationDate->get('date')->value);
    return \DateTime::createFromFormat("Y-m-d H:i", $date->format('Y-m-d ') . $heure);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $heure_debut = $form_state->getValue([
      'table_horaire',
      $this->rdid,
      'heure_debut',
    ]);
    $heure_fin = $form_state->getValue([
      'table_horaire',
      $this->rdid,
      'heure_fin',
    ]);
    if ($heure_debut > $heure_fin) {
      $form_state->setErrorByName('table_horaire][' . $this->rdid . '][heure_debut', 'L\'heure de fin doit être supérieure à l\'heure de début.');
      $form_state->setErrorByName('table_horaire][' . $this->rdid . '][heure_fin');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->createHoraire($form, $form_state);
  }

}
