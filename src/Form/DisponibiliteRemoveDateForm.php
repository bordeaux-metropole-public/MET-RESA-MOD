<?php

namespace Drupal\reservation\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Defines a confirmation form for deleting mymodule data.
 */
class DisponibiliteRemoveDateForm extends ConfirmFormBase {

  /**
   * The ID of the item to delete.
   *
   * @var string
   */
  protected $nid;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'disponibilite_remove_date_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    // The question to display to the user.
    return 'Voulez vous annuler les dispos de la ressource ' . $this->nid . ' ?';
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    // This needs to be a valid route otherwise the cancel link won't appear.
    return new Url('reservation.disponibilite.index', ['choix' => 'user']);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    // A brief desccription.
    return 'Êtes-vous sûr ?';
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return 'Suppression !';
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelText() {
    return 'Retour au menu';
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param $nid
   *
   * @return array
   */
  public function buildForm(array $form, FormStateInterface $form_state, $nid = NULL) {
    $this->nid = $nid;
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\reservation\Service\ReservationDateServices $reservationDate */
    $reservationDate = \Drupal::service('reservation.date');
    $reservationDate->setCancelDate($this->nid);

    $form_state->setRedirectUrl(Url::fromRoute('reservation.disponibilite.index', ['choix' => 'user']));
  }

}
