<?php

namespace Drupal\reservation\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\reservation\Entity\ReservationDemandeToken;
use Drupal\reservation\ReservationConstants;

/**
 * Class StateForm.
 *
 * @ingroup bat
 */
class ParametreCommunForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'parametre_commun';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // GET VALUES.
    $reservationSettings = \Drupal::config(ReservationConstants::MODULE_SETTINGS);

    $form['fieldset_token'] = [
      '#type' => 'fieldset',
      '#title' => 'Token',
    ];

    $tokenSettings = $reservationSettings->get('token');
    $form['fieldset_token'][ReservationDemandeToken::TOKEN_TIME_LIMIT] = [
      '#type' => 'number',
      '#title' => $this->t('Temps limite pour un formulaire en minutes'),
      '#default_value' => $tokenSettings[ReservationDemandeToken::TOKEN_TIME_LIMIT] ?? 20,
    ];

    $form['fieldset_token'][ReservationDemandeToken::TOKEN_TIME_CAUTION] = [
      '#type' => 'number',
      '#title' => $this->t('Temps limite pour une caution CB en minutes'),
      '#default_value' => $tokenSettings[ReservationDemandeToken::TOKEN_TIME_CAUTION] ?? 120,
    ];

    $form['fieldset_token'][ReservationDemandeToken::TOKEN_TIME_MANUELLE] = [
      '#type' => 'number',
      '#title' => $this->t('Temps limite pour une demande manuelle en minutes'),
      '#default_value' => $tokenSettings[ReservationDemandeToken::TOKEN_TIME_MANUELLE] ?? 720,
    ];

    $form['fieldset_validation'] = [
      '#type' => 'fieldset',
      '#title' => 'Messages de validation',
    ];

    $messageSettings = $reservationSettings->get('message_validation');
    foreach ($messageSettings as $key => $value) {
      $form['fieldset_validation'][$key] = [
        '#type' => 'textfield',
        '#title' => $key,
        '#default_value' => $value,
      ];
    }

    $form['fieldset_caution'] = [
      '#type' => 'fieldset',
      '#title' => 'Paramètrage Caution',
    ];

    $cautionSettings = $reservationSettings->get('caution');
    foreach ($cautionSettings as $key1 => $cautions) {
      // Input and url settings.
      if (is_array($cautions)) {
        $form['fieldset_caution'][$key1]['fieldset_sub_caution'] = [
          '#type' => 'fieldset',
          '#title' => $key1,
        ];
        foreach ($cautions as $key2 => $value) {
          $form['fieldset_caution'][$key1]['fieldset_sub_caution'][$key2] = [
            '#type' => 'textfield',
            '#title' => $key2,
            '#default_value' => $value,
          ];
        }
      }
    }
    $form['fieldset_caution']['iframe_theme'] = [
      '#type' => 'textfield',
      '#title' => t('Thème iframe'),
      '#default_value' => $cautionSettings['iframe_theme'] ?? '',
    ];

    $form['fieldset_webform'] = [
      '#type' => 'fieldset',
      '#title' => 'Paramètrage Webform',
    ];

    $webformSettings = $reservationSettings->get('webform');
    $form['fieldset_webform']['activation_ip'] = [
      '#type' => 'radios',
      '#title' => "Empêcher la sauvegarde de l'ip lors de la soumission d'un formulaire",
      '#options' => [
        0 => 'Non',
        1 => 'Oui',
      ],
      '#default_value' => $webformSettings['activation_ip'] ?? 0,
    ];

    $form['fieldset_json_calendar'] = [
      '#type' => 'fieldset',
      '#title' => t('JSON calendar'),
    ];

    $jsonCalendarSettings = $reservationSettings->get('json_calendar');
    $form['fieldset_json_calendar']['max-age'] = [
      '#type' => 'number',
      '#title' => t('JSON cache max-age in seconds'),
      '#required' => FALSE,
      '#default_value' => $jsonCalendarSettings['max-age'] ?? 10,
    ];
    $form['fieldset_json_calendar']['debug-enabled'] = [
      '#type' => 'checkbox',
      '#title' => t('Debug enabled'),
      '#required' => FALSE,
      '#default_value' => $jsonCalendarSettings['debug-enabled'] ?? FALSE,
    ];

    $form['fieldset_demande'] = [
      '#type' => 'fieldset',
      '#title' => 'Paramètrage Demandes',
    ];

    $demandeSettings = $reservationSettings->get('demande');
    $form['fieldset_demande']['caution'] = [
      '#type' => 'radios',
      '#title' => "Afficher la caution",
      '#options' => [
        0 => 'Non',
        1 => 'Oui',
      ],
      '#default_value' => $demandeSettings['caution'] ?? 0,
    ];
    $form['fieldset_demande']['inscrit'] = [
      '#type' => 'radios',
      '#title' => "Afficher le nombre d'inscrit",
      '#options' => [
        0 => 'Non',
        1 => 'Oui',
      ],
      '#default_value' => $demandeSettings['inscrit'] ?? 0,
    ];

    $form['fieldset_demande']['webform_key'] = [
      '#type' => 'textfield',
      '#title' => 'Clefs Champs webform personnalisé',
      '#default_value' => $demandeSettings['webform_key'] ?? '',
      '#maxlength' => 5000,
      '#size' => 150,
    ];

    $form['fieldset_demande']['webform_label'] = [
      '#type' => 'textfield',
      '#title' => 'Label Champs webform personnalisé',
      '#default_value' => $demandeSettings['webform_label'] ?? '',
      '#maxlength' => 5000,
      '#size' => 150,
    ];

    $form['fieldset_submission'] = [
      '#type' => 'fieldset',
      '#title' => 'Purge des soumissions webform obsolètes',
    ];

    $submissionSettings = $reservationSettings->get('submission');

    $form['fieldset_submission']['config_element_name'] = [
      '#type' => 'textfield',
      '#title' => 'Nom de l\'élément Webform Reservation Config',
      '#default_value' => $submissionSettings['config_element_name'] ?? 'webform_reservation_config',
      '#maxlength' => 5000,
      '#size' => 150,
    ];

    $form['fieldset_submission']['purge_strtotime'] = [
      '#type' => 'textfield',
      '#title' => 'Délai de purge des soumissions obsolètes (format strtotime)',
      '#default_value' => $submissionSettings['purge_strtotime'] ?? '-7 days',
      '#attributes' => [
        'placeholder' => 'ex: -1 day, -7 days, -1 minute...',
      ],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Save',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $time_limit = $form_state->getValue('time_limit');
    if ($time_limit < 1) {
      $form_state->setErrorByName('time_limit', 'Le temps limite doit être supérieur à 0.');
    }

    $max_age = $form_state->getValue('max-age');
    if ($max_age < 0) {
      $form_state->setErrorByName('max-age', 'La durée de mise en cache doit être supérieure ou égale à 0.');
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Mise en édition du settings.
    $config = \Drupal::service('config.factory')
      ->getEditable(ReservationConstants::MODULE_SETTINGS);

    // Sauvegarde Token.
    $config->set('token', [
      ReservationDemandeToken::TOKEN_TIME_LIMIT => $form_state->getValue(ReservationDemandeToken::TOKEN_TIME_LIMIT),
      ReservationDemandeToken::TOKEN_TIME_CAUTION => $form_state->getValue(ReservationDemandeToken::TOKEN_TIME_CAUTION),
      ReservationDemandeToken::TOKEN_TIME_MANUELLE => $form_state->getValue(ReservationDemandeToken::TOKEN_TIME_MANUELLE),
    ]);

    // Sauvegarde Message Validation.
    $config->set('message_validation', [
      'message_temps_depasse' => $form_state->getValue('message_temps_depasse'),
      'message_incident' => $form_state->getValue('message_incident'),
      'message_annulee' => $form_state->getValue('message_annulee'),
      'message_confirmation_pre_reservation' => $form_state->getValue('message_confirmation_pre_reservation'),
      'message_confirmation_acceptee' => $form_state->getValue('message_confirmation_acceptee'),
    ]);

    // Sauvegarde Caution.
    $config->set('caution',
      [
        'input' => [
          'PSPID' => $form_state->getValue('PSPID'),
          'CURRENCY' => $form_state->getValue('CURRENCY'),
          'LANGUAGE' => $form_state->getValue('LANGUAGE'),
          'OWNERZIP' => $form_state->getValue('OWNERZIP'),
          'OWNERADDRESS' => $form_state->getValue('OWNERADDRESS'),
          'OWNERCTY' => $form_state->getValue('OWNERCTY'),
          'OWNERTOWN' => $form_state->getValue('OWNERTOWN'),
          'OWNERTELNO' => $form_state->getValue('OWNERTELNO'),
        ],
        'url' => [
          'send' => $form_state->getValue('send'),
          'receive' => $form_state->getValue('receive'),
          'return_host' => $form_state->getValue('return_host'),
        ],
        'iframe_theme' => $form_state->getValue('iframe_theme'),
      ]
    );

    // Sauvegarde Activation IP.
    $config->set('webform', ['activation_ip' => $form_state->getValue('activation_ip')]);

    // Sauvegarde json-calendar.
    $max_age = $form_state->getValue('max-age');
    $debug_enabled = $form_state->getValue('debug-enabled');
    $config->set('json_calendar', [
      'max-age' => $max_age,
      'debug-enabled' => $debug_enabled,
    ]);

    // Sauvegarde Demandes.
    $config->set('demande', [
      'caution' => $form_state->getValue('caution'),
      'inscrit' => $form_state->getValue('inscrit'),
      'webform_key' => $form_state->getValue('webform_key'),
      'webform_label' => $form_state->getValue('webform_label'),
    ]);

    // Purge des soumissions obsolètes.
    $config->set('submission', [
      'config_element_name' => $form_state->getValue('config_element_name'),
      'purge_strtotime' => $form_state->getValue('purge_strtotime'),
    ]);

    // Sauvergarde config.
    $config->save();
  }

}
