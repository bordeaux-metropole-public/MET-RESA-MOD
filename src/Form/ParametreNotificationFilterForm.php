<?php

namespace Drupal\reservation\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\reservation\Entity\ReservationRessourceNode;

/**
 * Class StateForm.
 *
 * @ingroup bat
 */
class ParametreNotificationFilterForm extends FormBase {

  /**
   * @var \Drupal\reservation\Entity\ReservationRessourceNode|null
   */
  protected $resourceNode;

  /**
   * @var array
   */
  protected $statutstabs;

  /**
   * @var mixed
   */
  protected $typeEmail;

  /**
   * @var array
   */
  protected $ressources;

  /**
   * @var mixed
   */
  protected $nid;

  /**
   * ParametreNotificationFilterForm constructor.
   *
   * @param $ressources
   * @param $statutstabs
   * @param $type_email
   * @param $nid
   */
  public function __construct($ressources, $statutstabs, $type_email, $nid) {
    $this->resourceNode = ReservationRessourceNode::load($nid);
    $this->statutstabs = $statutstabs;
    $this->typeEmail = $type_email;
    $this->ressources = $ressources;
    $this->nid = $nid;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'reservation_notification_filter_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['select']['select_ressource'] = [
      '#type' => 'select',
      '#title' => 'Ressource : ',
      '#options' => $this->ressources,
      '#default_value' => $this->nid,
      '#ajax' => [
        'callback' => '::redirectPage',
        'wrapper' => 'mode',
      ],
    ];

    $form['email_to'] = [
      '#type' => 'textfield',
      '#title' => 'Email To :',
      '#default_value' => $this->resourceNode->getEmailTo(),
      '#ajax' => [
        'callback' => [$this, 'setEmailTo'],
        'event' => 'change',
      ],
    ];

    $form['statuts'] = [
      '#type' => 'table',
      '#title' => 'Sample Table',
    ];

    foreach ($this->statutstabs as $key => $row) {
      $form['statuts'][1][$key] = [
        '#type' => 'checkbox',
        '#title' => $row['title_statut'],
        '#default_value' => $row['statut'],
        '#ajax' => [
          'callback' => '::setStatut',
          'wrapper' => 'mode',
        ],
      ];
    }

    return $form;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function setEmailTo(array $form, FormStateInterface $form_state) {
    $emailTo = $form_state->getValue('email_to');
    $ressourceNode = $this->resourceNode;
    $ressourceNode->setEmailTo($emailTo);
    $ressourceNode->save();

    $response = new AjaxResponse();
    return $response;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function setStatut(array $form, FormStateInterface $form_state) {
    $notificationServices = \Drupal::service('reservation.notification');
    $statuts = $form_state->getValue('statuts');

    foreach ($statuts[1] as $key => $statut) {
      $node = $notificationServices->getNotificationByNidType($this->nid, $key);
      if (!empty($node)) {
        $node->setStatut($statut);
        $node->save();
      }
    }

    $response = new AjaxResponse();
    return $response;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function redirectPage(array $form, FormStateInterface $form_state) {
    $select_ressource = $form_state->getValue('select_ressource');

    $response = new AjaxResponse();
    $url = Url::fromRoute('reservation.parametre.notification',
      ['nid' => $select_ressource, 'type_email' => $this->typeEmail],
      ["absolute" => TRUE])->toString();

    $response->addCommand(new RedirectCommand($url));

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
