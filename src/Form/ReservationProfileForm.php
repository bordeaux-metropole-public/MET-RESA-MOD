<?php

namespace Drupal\reservation\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\reservation\Entity\ReservationRessourceUser;
use Drupal\user\ProfileForm;

/**
 * Class ReservationProfileForm.
 *
 * @package Drupal\reservation\Form
 */
class ReservationProfileForm extends ProfileForm {

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return mixed
   */
  public function form(array $form, FormStateInterface $form_state) {
    $uid = \Drupal::routeMatch()->getParameter('user')->id();
    /** @var \Drupal\reservation\Service\ReservationRessourceUserServices $ressourceUserServices */
    $ressourceUserServices = \Drupal::service('reservation.ressource.user');
    /** @var \Drupal\reservation\Service\ReservationRessourceServices $ressourceServices */
    $ressourceServices = \Drupal::service('reservation.ressource');

    $form['reservation'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#weight' => 50,
      '#title' => 'Réservation',
    ];

    $form['reservation']['ressource'] = [
      '#type' => 'checkboxes',
      '#title' => 'Ressources actives',
      '#options' => $ressourceServices->getAvailableRessources(),
      '#default_value' => $ressourceUserServices->getUserNodeIds($uid),
    ];

    return parent::form($form, $form_state);
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function save(array $form, FormStateInterface $form_state) {
    parent::save($form, $form_state);

    $uid = \Drupal::routeMatch()->getParameter('user')->id();
    $ressourceUserServices = \Drupal::service('reservation.ressource.user');
    foreach ($form_state->getValue('ressource') as $key => $ressource) {
      $ressourceUser = $ressourceUserServices->getUser($uid, $key);
      if ($ressource && !$ressourceUser) {
        $ressourceUser = ReservationRessourceUser::create([
          'user_id' => $uid,
          'nid' => $key,
        ]);
        $ressourceUser->save();
      }

      if (!$ressource && $ressourceUser) {
        $ressourceUser->delete();
      }
    }
  }

}
