<?php

namespace Drupal\reservation\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\reservation\ReservationConstants;

/**
 * Task permettant la suppression automatique des demandes dépassant un seuil
 * définie dans le panneau /admin/reservation/parametre/ressource
 *
 * @QueueWorker(
 *   id = "reservation_demande_remove",
 *   title = @Translation("Suppression des demandes dont la date est supérieure
 *   au paramètre en base"), cron = {"time" = 1}
 * )
 */
class TaskWorkerReservationDemandeRemove extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {

    $reservationRessourceNode = \Drupal::service('reservation.ressource.node');
    $reservationRessource = \Drupal::service('reservation.ressource');
    $reservationDemande = \Drupal::service('reservation.demande');
    $reservationDate = \Drupal::service('reservation.date');
    $count = 0;

    $ressources = $reservationRessource->getRessources();
    foreach ($ressources as $ressource) {
      $rdid = NULL;
      $demandes = NULL;

      $nodes = $reservationRessourceNode->queryNodeByRrid($ressource->id());

      if ($nodes) {
        $rdid = $reservationDate->getRdidByNid($nodes);
      }
      if ($rdid) {
        $demandes = $reservationDemande->getDemandeByYear($rdid, $ressource->getRgpd());
      }
      if ($demandes) {
        $count += $this->removeDemandeWebform($demandes);
      }
    }

    \Drupal::logger(ReservationConstants::LOG_CHANNEL)
      ->info('Nombre de demandes supprimées : %count',
        ['%count' => $count]);
  }

  /**
   *
   */
  private function removeDemandeWebform($demandes) {
    /** @var \Drupal\reservation\Service\ReservationDemandeServices $demandeServices */
    $demandeServices = \Drupal::service('reservation.demande');
    $count = 0;
    foreach ($demandes as $demande) {
      $demandeServices->deleteDemande($demande->id());
      $count++;
    }
    return $count;
  }

}
