<?php

namespace Drupal\reservation\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'webform_reservation_accompagnants' element.
 *
 * @WebformElement(
 *   id = "webform_reservation_accompagnants",
 *   default_key = "webform_reservation_accompagnants",
 *   label = @Translation("Réservation Accompagnants (v2+ only)"),
 *   description = @Translation("Provides a webform reservation accompagnants
 *   (handler v2+ only)."), category = @Translation("Reservation"), multiline =
 *   TRUE, composite = TRUE, states_wrapper = TRUE,
 * )
 */
class WebformReservationAccompagnantsPlugin extends WebformReservationPersonnePlugin {

  /**
   * {@inheritdoc}
   */
  public function initialize(array &$element) {
    // Surcharge le titre de l'élément dans l'onglet "Construire"
    // $element['#admin_title'] = "Réservation Accompagnants Admin";.
    parent::initialize($element);
  }

  /**
   * {@inheritdoc}
   */
  public function defineDefaultProperties() {
    return [
      'title' => 'Réservation Accompagnants',
    ] + parent::defineDefaultProperties();
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    return $form;
  }

}
