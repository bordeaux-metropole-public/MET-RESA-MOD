<?php

namespace Drupal\reservation\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElementBase;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Provides a 'webform_calendar_element' element.
 *
 * @WebformElement(
 *   id = "webform_reservation_calendar",
 *   label = @Translation("Réservation Calendar"),
 *   description = @Translation("Element permettant la sélection des dates
 *   dispos."), category = @Translation("Reservation"),
 * )
 *
 * @see \Drupal\webform_datepicker_element\Element\WebformExampleElement
 * @see \Drupal\webform\Plugin\WebformElementBase
 * @see \Drupal\webform\Plugin\WebformElementInterface
 * @see \Drupal\webform\Annotation\WebformElement
 */
class WebformReservationCalendar extends WebformElementBase {

  /**
   * {@inheritdoc}
   */
  public function initialize(array &$element) {
    $element['#admin_title'] = "Date Réservation";
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultProperties() {
    $properties = parent::getDefaultProperties();

    $properties['showElements'] = '';
    $properties['showplacedate'] = '';
    $properties['showplacehoraire'] = '';
    $properties['autorefresh'] = '';
    $properties['title'] = 'Webform Réservation Calendar';

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function prepare(array &$element, WebformSubmissionInterface $webform_submission = NULL) {
    parent::prepare($element, $webform_submission);

    // Here you can customize the webform element's properties.
    // You can also customize the form/render element's properties via the
    // FormElement.
    //
    // @see \Drupal\webform_datepicker_element\Element\WebformExampleElement::processWebformElementExample
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {

    $form['element'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Element settings'),
      '#access' => TRUE,
      '#weight' => -50,
    ];

    $form['element']['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#maxlength' => NULL,
      '#description' => $this->t('This is used as a descriptive label when displaying this webform element.'),
      '#required' => TRUE,
      '#disabled' => TRUE,
      '#attributes' => ['autofocus' => 'autofocus'],
    ];

    $form['element']['value'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Value'),
      '#description' => $this->t('The value of the webform element.'),
    ];

    $form['element']['showElements'] = [
      '#title' => 'Afficher les autres élements du formulaire en permanence (si non coché, les élements seront affichés seulement si une date du calendrier est choisie)',
      '#type' => 'checkbox',
    ];

    $form['element']['showplacedate'] = [
      '#title' => 'Afficher les places disponibles pour les dates.',
      '#type' => 'checkbox',
    ];

    $form['element']['showplacehoraire'] = [
      '#title' => 'Afficher les places disponibles pour les horaires.',
      '#type' => 'checkbox',
    ];

    $form['element']['autorefresh'] = [
      '#title' => 'Rafraîchir automatiquement les disponibilités du calendrier.',
      '#type' => 'checkbox',
    ];

    $form['options'] = [];

    $form['options_other'] = [];

    $form['validation'] = [
      '#type' => 'details',
      '#title' => $this->t('Form validation'),
    ];

    $form['validation']['required_container'] = [
      '#type' => 'container',
      '#access' => TRUE,
    ];

    $form['validation']['required_container']['required'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Required'),
      '#description' => $this->t('Check this option if the user must enter a value.'),
      '#return_value' => TRUE,
    ];

    $form['validation']['required_container']['required_error'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Required message'),
      '#description' => $this->t('If set, this message will be used when a required webform element is empty, instead of the default "Field x is required." message.'),
      '#states' => [
        'visible' => [
          ':input[name="properties[required]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return $form;
  }

  /**
   *
   */
  public function getValue(array $element, WebformSubmissionInterface $webform_submission, array $options = []) {
    if (!isset($element['#webform_key']) && isset($element['#value'])) {
      return $element['#value'];
    }

    $webform_key = (isset($options['webform_key'])) ? $options['webform_key'] : $element['#webform_key'];
    $value = $webform_submission->getElementData($webform_key);
    // Is value is NULL and there is a #default_value, then use it.
    if ($value === NULL && isset($element['#default_value'])) {
      $value = $element['#default_value'];
    }

    // Return multiple (delta) value or composite (composite_key) value.
    if (is_array($value)) {
      // Return $options['delta'] which is used by tokens.
      // @see _webform_token_get_submission_value()
      if (isset($options['delta'])) {
        $value = (isset($value[$options['delta']])) ? $value[$options['delta']] : NULL;
      }

      // Return $options['composite_key'] which is used by composite elements.
      // @see \Drupal\webform\Plugin\WebformElement\WebformCompositeBase::formatTableColumn
      if ($value && isset($options['composite_key'])) {
        $value = (isset($value[$options['composite_key']])) ? $value[$options['composite_key']] : NULL;
      }
    }

    $reservationDateServices = \Drupal::service('reservation.date');
    $ressourceDate = $reservationDateServices->load($value);

    if ($ressourceDate) {
      return $ressourceDate->getDateFormat('d/m/Y');
    }
    else {
      return "";
    }
  }

}
