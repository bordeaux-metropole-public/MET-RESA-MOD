<?php

namespace Drupal\reservation\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElement\WebformCompositeBase;
use Drupal\webform\WebformSubmissionInterface;

/**
 *
 * @WebformElement(
 *   id = "webform_reservation_horaire",
 *   label = @Translation("Réservation Horaire"),
 *   description = @Translation("Element permettant la sélection des horaires
 *   dispos pour une date."), category = @Translation("Reservation"), composite
 *   = TRUE, states_wrapper = FALSE,
 * )
 */
class WebformReservationHoraire extends WebformCompositeBase {

  /**
   * {@inheritdoc}
   */
  public function initialize(array &$element) {

    parent::initialize($element);

    $element['#admin_title'] = "Horaire Réservation";
    $this->initializeCompositeElements($element);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultProperties() {
    $properties = parent::getDefaultProperties();

    $properties['title'] = 'Webform Réservation Horaire';
    $properties['admin_title'] = 'Réservation Horaire';

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function getTableColumn(array $element) {

    $columns = parent::getTableColumn($element);
    unset($columns['element__webform_reservation_horaire__reservation-horaire-select']);
    $columns['element__webform_reservation_horaire']['title'] = "Horaire";

    return $columns;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['element']['title']['#disabled'] = TRUE;

    $form['composite']['element'] = [];
    $form['composite']['flexbox'] = [];
    $form['access'] = [];
    $form['element_attributes'] = [];

    unset($form['element_description']);
    unset($form['element']['multiple']);
    unset($form['element']['multiple_error']);
    unset($form['element']['multiple__header_container']);
    unset($form['conditional_logic']);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function formatHtmlItemValue(array $element, WebformSubmissionInterface $webform_submission, array $options = []) {
    return $this->formatTextItemValue($element, $webform_submission, $options);
  }

  /**
   * {@inheritdoc}
   */
  protected function formatTextItemValue(array $element, WebformSubmissionInterface $webform_submission, array $options = []) {
    $value = $this->getValue($element, $webform_submission, $options);
    $horaireId = $value['reservation-horaire-select'] ?? NULL;
    // Fix for Museum display bug.
    $horaireId = empty($horaireId) && isset($value['']) ? $value[''] : $horaireId;

    $lines = [];

    $reservationHoraireServices = \Drupal::service('reservation.horaire');
    $ressourceHoraire = $reservationHoraireServices->load($horaireId);

    if ($ressourceHoraire) {
      $lines[] = $ressourceHoraire->getHeureDebutFormat('H:i') . $ressourceHoraire->getHeureFinFormat(' - H:i');
    }
    else {
      $lines[] = '';
    }

    return $lines;
  }

}
