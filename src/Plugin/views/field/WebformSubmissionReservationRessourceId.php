<?php

namespace Drupal\reservation\Plugin\views\field;

/**
 * Field handler to display the reservation demande id for a webform submission.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("webform_submission_reservation_ressource_id")
 */
class WebformSubmissionReservationRessourceId extends WebformSubmissionReservationConfigBase {

  /**
   * {@inheritdoc}
   */
  protected function getPropertyName(): string {
    return 'webform_reservation_ressource_id';
  }

}
