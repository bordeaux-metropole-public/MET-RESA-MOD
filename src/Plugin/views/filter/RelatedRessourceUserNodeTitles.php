<?php

namespace Drupal\reservation\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\reservation\ReservationUtils;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\filter\ManyToOne;
use Drupal\views\ViewExecutable;

/**
 * Filters by given list of related ressource user node title options.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("reservation_related_ressource_user_node_titles")
 */
class RelatedRessourceUserNodeTitles extends ManyToOne {

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);

    $this->valueTitle = t('Allowed related ressource user node titles');
    $this->definition['options callback'] = [$this, 'generateOptions'];

    $this->definition['allow empty'] = FALSE;
    $this->always_required = TRUE;

  }

  /**
   * Validate the exposed handler form.
   */
  public function validateExposed(&$form, FormStateInterface $form_state) {
    $identifier = $this->options['expose']['identifier'];
    $request = \Drupal::requestStack()->getCurrentRequest();
    if (count($request->query->all()) > 0) {
      $value = $request->get($identifier);
      if (empty($value) || !is_array($value)) {
        $form_state->setErrorByName($identifier, 'Vous devez sélectionner une ressource.');
      }
      else {
        if (!ReservationUtils::isAdmin() && !ReservationUtils::canManageAllRessourcesReservations()) {
          $resourceIds = ReservationUtils::getUserRessourceIds();
          $validIds = array_intersect($value, $resourceIds);
          if (!($value == $validIds)) {
            $form_state->setErrorByName($identifier, 'Ressources sélectionnées invalides.');
          }
        }
      }
    }
  }

  /**
   * Override the query so that no filtering takes place if the user doesn't
   * select any options.
   */
  public function query() {
    if (!empty($this->value)) {
      parent::query();
    }
  }

  /**
   * Skip validation if no options have been chosen so we can use it as a
   * non-filter.
   */
  public function validate() {
    if (!empty($this->value)) {
      parent::validate();
    }
  }

  /**
   * Helper function that generates the options.
   *
   * @return array
   */
  public function generateOptions() {
    /** @var \Drupal\reservation\Service\ReservationRessourceServices $ressourceServices */
    $ressourceServices = \Drupal::service('reservation.ressource');
    return $ressourceServices->getAvailableRessources();
  }

  /**
   * @return array
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['value']['default'] = ReservationUtils::getUserRessourceIds();
    $options['expose']['required'] = TRUE;

    return $options;
  }

}
