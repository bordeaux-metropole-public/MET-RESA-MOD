<?php

namespace Drupal\reservation\Plugin\views\relationship;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\relationship\RelationshipPluginBase;
use Drupal\views\Views;

/**
 * Relationship handler to return the taxonomy terms of nodes.
 *
 * @ingroup views_relationship_handlers
 *
 * @ViewsRelationship("webform_submission_reservation_relationship")
 */
class WebformSubmissionReservationConfigRelationship extends RelationshipPluginBase {

  const DEFAULT_RESA_CONF_ELT_NAME = 'webform_reservation_config';

  const RESA_CONFIG_ELEMENT_NAME = 'reservation_config_element_name';

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->ensureMyTable();

    $resa_config_element_name =
      $this->options[self::RESA_CONFIG_ELEMENT_NAME] ?: self::DEFAULT_RESA_CONF_ELT_NAME;

    $first = [
      'table' => 'webform_submission_data',
      'field' => 'sid',
      'left_table' => 'webform_submission',
      'left_field' => 'sid',
      'extra' => [
        ['field' => 'name', 'value' => $resa_config_element_name],
        ['field' => 'property', 'value' => $this->definition['property name']],
        ['field' => 'delta', 'value' => 0],
      ],
    ];

    if (!empty($this->options['required'])) {
      $first['type'] = 'INNER';
    }

    /** @var \Drupal\views\Plugin\views\join\JoinPluginBase $first_join */
    $first_join = Views::pluginManager('join')->createInstance('standard', $first);

    $first_alias = $this->query->addTable('webform_submission_data', $this->relationship, $first_join);

    $second = [
      'left_table' => $first_alias,
      'left_field' => 'value',
      'table' => $this->definition['base'],
      'field' => $this->definition['base field'],
      'adjusted' => TRUE,
    ];

    if (!empty($this->options['required'])) {
      $second['type'] = 'INNER';
    }

    /** @var \Drupal\views\Plugin\views\join\JoinPluginBase $second_join */
    $second_join = Views::pluginManager('join')->createInstance('standard', $second);

    $alias = $this->definition['base'] . '__' . $this->definition['base field'];

    $this->alias = $this->query->addRelationship($alias, $second_join, $this->definition['base'], $this->relationship);
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options[self::RESA_CONFIG_ELEMENT_NAME] = ['default' => self::DEFAULT_RESA_CONF_ELT_NAME];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form[self::RESA_CONFIG_ELEMENT_NAME] = [
      '#title' => $this->t('Reservation config element name'),
      '#type' => 'textfield',
      '#default_value' => $this->options[self::RESA_CONFIG_ELEMENT_NAME] ?: self::DEFAULT_RESA_CONF_ELT_NAME,
    ];
  }

}
