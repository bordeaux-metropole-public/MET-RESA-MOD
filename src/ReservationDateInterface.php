<?php

namespace Drupal\reservation;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a ReservationDate entity.
 *
 * We have this interface so we can join the other interfaces it extends.
 *
 * @ingroup reservation
 */
interface ReservationDateInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Returns the associated ReservationResource node object.
   *
   * @return \Drupal\reservation\ReservationRessourceNodeInterface
   *   The corresponding ReservationRessourceNode entity.
   */
  public function getReservationRessourceNode();

}
