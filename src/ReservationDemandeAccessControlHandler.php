<?php

namespace Drupal\reservation;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityHandlerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\reservation\Entity\ReservationDemande;
use Drupal\reservation\Service\ReservationRessourceUserServices;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Access controller for the ReservationDemande entity.
 *
 * @see \Drupal\reservation\Entity\ReservationDemande.
 */
class ReservationDemandeAccessControlHandler extends EntityAccessControlHandler implements EntityHandlerInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\reservation\Service\ReservationRessourceUserServices
   */
  protected $ressourceUserServices;

  /**
   * Constructs an access control handler instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\reservation\Service\ReservationRessourceUserServices $ressourceUserServices
   *   The ResourceUser manager.
   */
  public function __construct(EntityTypeInterface $entity_type, ReservationRessourceUserServices $ressourceUserServices) {
    parent::__construct($entity_type);
    $this->ressourceUserServices = $ressourceUserServices;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('reservation.ressource.user'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\reservation\Entity\ReservationDemande $entity */
    switch ($operation) {
      case 'view':
        $access_result = $this->checkAccessByOperation($account, $entity, 'view');
        break;

      case 'update':
        $access_result = $this->checkAccessByOperation($account, $entity, 'edit');
        break;

      case 'delete':
        $access_result = $this->checkAccessByOperation($account, $entity, 'delete');
        break;

      default:
        $access_result = AccessResult::neutral();
        break;
    }

    return $access_result->addCacheableDependency($account);
  }

  /**
   * @param \Drupal\Core\Session\AccountInterface $account
   * @param \Drupal\reservation\Entity\ReservationDemande $entity
   * @param string $operation
   *
   * @return \Drupal\Core\Access\AccessResult|\Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultNeutral
   */
  protected function checkAccessByOperation(
    AccountInterface $account,
    ReservationDemande $entity,
    string $operation) {
    if ($account->hasPermission($operation . ' ReservationDemande entities')) {
      $access_result = AccessResult::allowed();
    }
    else {
      if ($account->hasPermission($operation . ' my ReservationDemande entities')) {
        $is_resource_accessible = $this->isResourceAccessible($account, $entity);
        $access_result = AccessResult::allowedIf($is_resource_accessible)
          ->addCacheableDependency($entity);
      }
      else {
        $access_result = AccessResult::neutral();
      }
    }
    return $access_result;
  }

  /**
   * @param \Drupal\Core\Session\AccountInterface $account
   * @param \Drupal\reservation\Entity\ReservationDemande $entity
   *
   * @return bool
   */
  protected function isResourceAccessible(AccountInterface $account, ReservationDemande $entity) {
    $nids = $this->getUserResources($account->id());
    $date = $entity->getDate();
    if ($date) {
      $resource = $date->getReservationRessourceNode();
      return $nids && $resource && in_array($resource->id(), $nids);
    }
    else {
      return FALSE;
    }
  }

  /**
   * @param int $user_id
   *   User id.
   *
   * @return array Resources accessible to the user
   */
  protected function getUserResources(int $user_id) {
    return ReservationUtils::getFastCachedUserRessourceIdsByUid($user_id);
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add ReservationDemande entities');
  }

}
