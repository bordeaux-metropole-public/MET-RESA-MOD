<?php

namespace Drupal\reservation;

use Drupal\Core\Entity\EntityInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\webform\WebformInterface;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Class ReservationUtils.
 *
 * @package Drupal\reservation
 */
class ReservationUtils {

  /**
   * @param $timestamp
   *
   * @return \DateTime
   */
  public static function getDateTimeFromTimestamp($timestamp) {
    $date = new \DateTime();
    $date->setTimestamp($timestamp);
    return $date;
  }

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param $fieldName
   * @param $dateStr
   *
   * @return \DateTime|false
   */
  public static function getDateEntityFieldValue(EntityInterface $entity, $fieldName) {
    $dateStr = $entity->get($fieldName)->value;
    if ($dateStr) {
      $dateTime = \DateTime::createFromFormat(
        DateTimeItemInterface::DATETIME_STORAGE_FORMAT, $dateStr,
        new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE));
      $dateTime = $dateTime->setTimezone(new \DateTimeZone(date_default_timezone_get()));
    }
    else {
      $dateTime = NULL;
    }
    return $dateTime;
  }

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param \DateTime $dateTime
   *
   * @return \Drupal\Core\Entity\EntityInterface
   */
  public static function setDateEntityFieldValue($entity, $fieldName, \DateTime $dateTime = NULL) {
    $dateTimeStringValue = self::getDateEntityFieldStringValue($dateTime);
    $entity->set($fieldName, $dateTimeStringValue);
    return $entity;
  }

  /**
   * @param \DateTime $dateTime
   *
   * @return string
   */
  public static function getDateEntityFieldStringValue(\DateTime $dateTime = NULL) {
    if (empty($dateTime)) {
      $dateStringValue = NULL;
    }
    else {
      $dateTime = $dateTime->setTimezone(new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE));
      $dateStringValue = $dateTime->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
    }
    return $dateStringValue;
  }

  /**
   * @return array|mixed|null
   */
  public static function getFastCachedUserRessourceIdsByUid(int $user_id) {
    static $drupal_static_fast;
    if (!isset($drupal_static_fast)) {
      $drupal_static_fast['user_ressource_ids'] = &drupal_static(__FUNCTION__);
    }
    $user_ressource_ids = &$drupal_static_fast['user_ressource_ids'];
    if (!isset($user_ressource_ids)) {
      /** @var \Drupal\reservation\Service\ReservationRessourceUserServices $ressourceUserServices */
      $ressourceUserServices = \Drupal::service('reservation.ressource.user');
      $user_ressource_ids = $ressourceUserServices->getRessourceUserNodeIdsByUid($user_id);
    }
    return $user_ressource_ids;
  }

  /**
   * @return bool
   */
  public static function isAdmin() {
    return \Drupal::currentUser()
      ->hasPermission(ReservationConstants::ADMIN_DEMANDE_PERMISSION);
  }

  /**
   * @return bool
   */
  public static function isGestionnaireManuel() {
    return \Drupal::currentUser()
      ->hasPermission(ReservationConstants::MANUELLE_DEMANDE_PERMISSION);
  }

  /**
   * @return bool
   */
  public static function canViewOnlyOwnDemandes() {
    return self::canViewOwnDemandes() && !self::canManageAllRessourcesReservations();
  }

  /**
   * @return bool
   */
  public static function canViewOwnDemandes() {
    return \Drupal::currentUser()->hasPermission(
      ReservationConstants::VIEW_OWN_DEMANDE_PERMISSION);
  }

  /**
   * @return bool
   */
  public static function canManageAllRessourcesReservations() {
    return \Drupal::currentUser()->hasPermission(
      ReservationConstants::MANAGE_ALL_DEMANDES_PERMISSION);
  }

  /**
   * @return bool
   */
  public static function canManageOnlyOwnResourcesReservations() {
    return self::canManageOwnResourcesReservations() && !self::canManageAllRessourcesReservations();
  }

  /**
   * @return bool
   */
  public static function canManageOwnResourcesReservations() {
    return \Drupal::currentUser()->hasPermission(
      ReservationConstants::MANAGE_OWN_RESERVATIONS_PERMISSION);
  }

  /**
   * @param $nid
   *
   * @return bool
   */
  public static function isAccessibleResource($nid) {
    if (self::canManageAllRessourcesAvailabilities()) {
      // @todo check if resource exists?
      return TRUE;
    }
    else {
      if (self::canManageOwnResourcesAvailabilities()) {
        return in_array($nid, self::getUserRessourceIds());
      }
      else {
        return FALSE;
      }
    }
  }

  /**
   * @return bool
   */
  public static function canManageAllRessourcesAvailabilities() {
    return \Drupal::currentUser()->hasPermission(
      ReservationConstants::MANAGE_ALL_DISPOS_PERMISSION);
  }

  /**
   * @return bool
   */
  public static function canManageOwnResourcesAvailabilities() {
    return \Drupal::currentUser()->hasPermission(
      ReservationConstants::MANAGE_OWN_AVAILABILITIES_PERMISSION);
  }

  /**
   * @return array
   */
  public static function getUserRessourceIds() {
    /** @var \Drupal\reservation\Service\ReservationRessourceUserServices $ressourceUserServices */
    $ressourceUserServices = \Drupal::service('reservation.ressource.user');
    return $ressourceUserServices->getRessourceUserNodeIdsByUid(\Drupal::currentUser()
      ->id());
  }

  /**
   * @param \DateTime $expirationTime
   * @param $expirationDelay
   *   in minutes
   *
   * @throws \Exception
   */
  public static function addExpirationDelayToDateTime(\DateTime $expirationTime, $expirationDelay) {
    return $expirationTime->add(new \DateInterval('PT' . $expirationDelay . 'M'));
  }

  /**
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *
   * @return mixed
   */
  public static function getWebformSubmissionDemandeId(WebformSubmissionInterface $webform_submission) {
    $submissionData = $webform_submission->getData();
    $webform_reservation_config = $submissionData['webform_reservation_config'];
    return $webform_reservation_config['webform_reservation_demande_id'] ?? NULL;
  }

  /**
   * @param \Drupal\webform\WebformInterface $webform
   *   The webform to search the caution element in.
   *
   * @return string
   *   The caution webform element name.
   */
  public static function getCautionElementName(WebformInterface $webform): string {
    $elements = $webform->getElementsDecodedAndFlattened();
    foreach ($elements as $name => $element) {
      if ($element['#type'] === 'webform_reservation_caution') {
        return $name;
      }
    }
    return 'webform_reservation_caution';

  }

}
