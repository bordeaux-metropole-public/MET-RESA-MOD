<?php

namespace Drupal\reservation\Service;

use Drupal\reservation\Entity\ReservationDate;
use Drupal\reservation\Entity\ReservationDemande;
use Drupal\reservation\Entity\ReservationHoraire;
use Drupal\reservation\Entity\ReservationRessourceNode;

/**
 * Class ReservationCalendarServices.
 *
 * @package Drupal\reservation\Service
 */
class ReservationCalendarServices {

  /**
   *
   */
  const STATUT_RESERVE = 'reserveDates';

  /**
   *
   */
  const STATUT_WAIT = 'waitDates';

  /**
   *
   */
  const STATUT_OPEN = 'openDates';

  /**
   *
   */
  const STATUT_UNAVAILABLE = 'unavailableDates';

  /**
   *
   */
  const ID_RESERVATION_DEMANDE = 'reservation_demande';

  /**
   *
   * @var demandeServices
   */
  protected $demandeServices;

  /**
   *
   * @var horaireServices
   */
  protected $horaireServices;

  /**
   *
   * @param \Drupal\reservation\Service\ReservationDemandeServices $demandeServices
   */
  public function __construct(ReservationDemandeServices $demandeServices, ReservationHoraireServices $horaireServices) {
    $this->demandeServices = $demandeServices;
    $this->horaireServices = $horaireServices;
  }

  /**
   *
   * @param mixed $rdid
   * @param mixed $rhid
   * @param mixed $destroy
   *
   * @return mixed
   */
  public function verificationDisponibilite($rdid = NULL, $rhid = NULL) {
    $statut = FALSE;
    /** @var \Drupal\reservation\Entity\ReservationDate $reservationDate */
    $reservationDate = ReservationDate::load($rdid);
    /** @var \Drupal\reservation\Entity\ReservationRessourceNode $ressourceNode */
    $ressourceNode = $reservationDate->getReservationRessourceNode();
    if ($reservationDate->getHoraire()) {
      if ($rhid == NULL) {
        throw new \InvalidArgumentException(
          "Cette ressource à reserver requiert un identifiant d'horaire non nul.");
      }
      else {
        $reservationHoraire = ReservationHoraire::load($rhid);
        $horaireRdid = $reservationHoraire->getDate()->id();
        // Vérification importante pour préserver l'intégrité des données !!
        if ($horaireRdid == $rdid) {
          if ($ressourceNode->isOffLimit($reservationHoraire->getHeureDebut())) {
            $statut = FALSE;
          }
          else {
            if ($reservationHoraire->getJaugeStatut()) {
              $count = $this->getDemandeHoraireDetail($reservationHoraire, $ressourceNode);
              if ($count['horaire']['place'] > 0) {
                $statut = TRUE;
              }
            }
            // Pas de jauge, c'est open bar.
            else {
              $statut = TRUE;
            }
          }
        }
        else {
          $statut = FALSE;
        }
      }
    }
    else {
      if ($ressourceNode->isOffLimit($reservationDate->getDateTime())) {
        $statut = FALSE;
      }
      else {
        $dates = $this->getDemandeDate($reservationDate);
        $statut = ($dates['statut'] === self::STATUT_OPEN) ? TRUE : FALSE;
      }
    }

    return $statut;
  }

  /**
   * @param \Drupal\reservation\Entity\ReservationHoraire $reservationHoraire
   * @param \Drupal\reservation\Entity\ReservationRessourceNode $ressourceNode
   *
   * @return array
   */
  public function getDemandeHoraireDetail(
    ReservationHoraire $reservationHoraire,
    ReservationRessourceNode $ressourceNode = NULL) {
    $reservationDemandes = $this->demandeServices->getDemandeByRhid($reservationHoraire->Id());
    $horaire = $this->getDemandeCountStatut($reservationDemandes);
    $horaire['jauge'] = $reservationHoraire->getJaugeNombre();

    if (!isset($ressourceNode) || !$ressourceNode->isOffLimit($reservationHoraire->getHeureDebut())) {
      $horaire['horaire'] = [
        'rhid' => $reservationHoraire->Id(),
        'heure' => $reservationHoraire->getHeureDebutFormat('H:i - ') . $reservationHoraire->getHeureFinFormat('H:i'),
        'place' => $horaire['jauge'] - $horaire['reserve'] - $horaire['wait'],
        'place-nowait' => $horaire['jauge'] - $horaire['reserve'],
        'jauge' => $reservationHoraire->getJaugeStatut(),
      ];
    }

    return $horaire;
  }

  /**
   * @param $reservationDemandes
   *
   * @return array
   */
  public function getDemandeCountStatut($reservationDemandes) {
    $statut = NULL;
    $count_wait = 0;
    $count_reserve = 0;

    foreach ($reservationDemandes as $reservationDemande) {
      $statut = $reservationDemande->getStatut();
      if ($statut == ReservationDemande::STATUT_CAUTION || $statut == ReservationDemande::STATUT_FORMULAIRE || $statut == ReservationDemande::STATUT_ATTENTE) {
        $count_wait += $reservationDemande->getJauge();
      }
      elseif ($statut == ReservationDemande::STATUT_CONFIRME || $statut == ReservationDemande::STATUT_ARCHIVE
        || $statut == ReservationDemande::STATUT_NO_SHOW || $statut == ReservationDemande::STATUT_SHOW) {
        $count_reserve += $reservationDemande->getJauge();
      }
    }

    return ['wait' => $count_wait, 'reserve' => $count_reserve];
  }

  /**
   * @param $reservationDate
   *
   * @return array
   */
  public function getDemandeDate($reservationDate) {
    $reservationDemandes = $this->demandeServices->getDemandeByRdid($reservationDate->Id());
    $count = $this->getDemandeCountStatut($reservationDemandes);
    $statut = $this->getDemandeLabelStatut(
      $reservationDate->getJaugeStatut(), $reservationDate->getJaugeNombre(), $count['reserve'], $count['wait']);
    $place = $reservationDate->getJaugeNombre() - $count['reserve'] - $count['wait'];
    $placeNoWait = $reservationDate->getJaugeNombre() - $count['reserve'];
    return $this->getFormatDate($reservationDate, $statut, FALSE, $place, $placeNoWait);
  }

  /**
   *
   * @param mixed $jaugeStatut
   * @param mixed $jaugeNombre
   * @param mixed $count_reserve
   * @param mixed $count_wait
   *
   * @return mixed
   */
  public function getDemandeLabelStatut($jaugeStatut = 1, $jaugeNombre = 0, $count_reserve = 0, $count_wait = 0) {
    $statut = NULL;

    if ($jaugeStatut && ($jaugeNombre <= $count_reserve)) {
      $statut = self::STATUT_RESERVE;
    }
    elseif ($jaugeStatut && ($jaugeNombre <= $count_reserve + $count_wait)) {
      $statut = self::STATUT_WAIT;
    }
    else {
      $statut = self::STATUT_OPEN;
    }

    return $statut;
  }

  /**
   * @param $reservationDate
   * @param $statut
   * @param bool $horaire
   * @param int $place
   *
   * @return array
   */
  public function getFormatDate($reservationDate, $statut, $horaire = FALSE, $place = 0, $placeConf = 0) {
    return [
      'rdid' => $reservationDate->Id(),
      'date' => $reservationDate->getDateFormat('m/d/Y'),
      'statut' => $statut,
      'place' => $place,
      'place-nowait' => $placeConf,
      'jauge' => $reservationDate->getJaugeStatut(),
      'horaire' => $horaire,
    ];
  }

  /**
   * @param $reservationDate
   *
   * @return array
   */
  public function getOffLimitDate($reservationDate) {
    $reservationDemandes = $this->demandeServices->getDemandeByRdid($reservationDate->Id());
    $count = $this->getDemandeCountStatut($reservationDemandes);
    $statut = self::STATUT_UNAVAILABLE;
    $place = $reservationDate->getJaugeNombre() - $count['reserve'] - $count['wait'];
    $placeNoWait = $reservationDate->getJaugeNombre() - $count['reserve'];
    return $this->getFormatDate($reservationDate, $statut, FALSE, $place, $placeNoWait);
  }

  /**
   * @param \Drupal\reservation\Entity\ReservationDate $reservationDate
   * @param \Drupal\reservation\Entity\ReservationRessourceNode $ressourceNode
   *
   * @return array
   */
  public function getDemandeHoraire(
    ReservationDate $reservationDate,
    ReservationRessourceNode $ressourceNode) {
    $count_reserve = 0;
    $count_wait = 0;
    $count_jauge = 0;
    $jauge_statut = 1;
    $horaires = [];

    $horaireDisponibles = $this->horaireServices->getByRdid($reservationDate->Id());
    foreach ($horaireDisponibles as $horaireDisponible) {
      $horaireDetail = $this->getDemandeHoraireDetail($horaireDisponible, $ressourceNode);
      if (isset($horaireDetail['horaire'])) {
        $jauge_statut *= $horaireDetail['horaire']['jauge'];
        if ($horaireDetail['horaire']['jauge'] == 0 || $horaireDetail['horaire']['place-nowait'] > 0) {
          $count_wait += $horaireDetail['wait'];
          $count_reserve += $horaireDetail['reserve'];
          $count_jauge += $horaireDetail['jauge'];
          $horaires[] = $horaireDetail['horaire'];
        }
      }
    }

    $statut = $this->getDemandeLabelStatut($jauge_statut, $count_jauge, $count_reserve, $count_wait);

    return $this->getFormatDate($reservationDate, $statut, $horaires);
  }

}
