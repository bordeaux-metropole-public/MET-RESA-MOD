<?php

namespace Drupal\reservation\Service;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\reservation\Entity\ReservationDemande;

/**
 * Class ReservationDemandeFormServices.
 *
 * @package Drupal\reservation\Service
 */
class ReservationDemandeFormServices {

  /**
   *
   * @var \Drupal\reservation\Service\ReservationDemandeServices
   */
  protected $demandeServices;

  /**
   *
   * @var \Drupal\reservation\Service\ReservationMailServices
   */
  protected $mailServices;

  /**
   * @var string[]
   */
  protected $labelStatuts;

  /**
   *
   * @param \Drupal\reservation\Service\ReservationDemandeServices $demandeServices
   * @param \Drupal\reservation\Service\ReservationMailServices $mailServices
   */
  public function __construct(ReservationDemandeServices $demandeServices, ReservationMailServices $mailServices) {
    $this->demandeServices = $demandeServices;
    $this->mailServices = $mailServices;
    $this->labelStatuts = $labelStatuts = [
      ReservationDemande::STATUT_CONFIRME => 'Confirmée',
      ReservationDemande::STATUT_ANNULE => 'Annulée',
      ReservationDemande::STATUT_ATTENTE => 'En attente',
      ReservationDemande::STATUT_CAUTION => 'En attente Caution',
      ReservationDemande::STATUT_ARCHIVE => 'Archivée',
      ReservationDemande::STATUT_REFUSE => 'Refusée',
      ReservationDemande::STATUT_SHOW => 'Show',
      ReservationDemande::STATUT_NO_SHOW => 'NoShow',
      ReservationDemande::STATUT_FORMULAIRE => 'Formulaire en traitement',
    ];
  }

  /**
   *
   * @param mixed $form
   *
   * @return string
   */
  public static function tableDemandeForm($form, $webform_submission) {
    $reservationDemande = \Drupal::service('reservation.demande');
    $demande = $reservationDemande->loadByWebformSubmission($webform_submission->id());

    $markup = $demande
      ? 'Date / Horaire du créneau : <b>' . $demande->getDateCreneau() . '</b>'
      : t('Demande introuvable pour soumission webform @id', ['@id' => $webform_submission->id()]);

    $form['table_date'] = [
      '#type' => 'item',
      '#markup' => $markup,
      '#weight' => -1,
    ];

    return $form;
  }

  /**
   *
   * @param mixed $form
   *
   * @return string
   */
  public static function tableActionForm($form) {
    $form['table_action'] = [
      '#type' => 'table',
      '#tableselect' => FALSE,
      '#weight' => 0,
    ];

    $form['table_action'][0]['confirme'] = [
      '#type' => 'submit',
      '#name' => 'confirme',
      '#value' => 'Confirmer',
    ];

    $form['table_action'][0]['annule'] = [
      '#type' => 'submit',
      '#name' => 'annule',
      '#value' => 'Annuler',
    ];

    $form['table_action'][0]['attente'] = [
      '#type' => 'submit',
      '#name' => 'attente',
      '#value' => 'En attente',
    ];

    $form['table_action'][0]['caution'] = [
      '#type' => 'submit',
      '#name' => 'caution',
      '#value' => 'En attente Caution',
    ];

    $form['table_action'][0]['refuse'] = [
      '#type' => 'submit',
      '#name' => 'refuse',
      '#value' => 'Refuser',
    ];

    $form['table_action'][0]['rappel'] = [
      '#type' => 'submit',
      '#name' => 'rappel',
      '#value' => 'Mail Rappel',
    ];

    $form['table_action'][0]['archive'] = [
      '#type' => 'submit',
      '#name' => 'archive',
      '#value' => 'Archive',
    ];

    $form['table_action'][0]['show'] = [
      '#type' => 'submit',
      '#name' => 'show',
      '#value' => t('Resa Show'),
    ];

    $form['table_action'][0]['noshow'] = [
      '#type' => 'submit',
      '#name' => 'noshow',
      '#value' => 'NoShow',
    ];

    $form['table_action'][0]['delete'] = [
      '#type' => 'submit',
      '#name' => 'delete',
      '#value' => 'Supprimer',
    ];

    return $form;
  }

  /**
   *
   * @param mixed $form
   *
   * @return string
   */
  public static function tableCautionForm($form) {
    $form['table_caution'] = [
      '#type' => 'table',
      '#tableselect' => FALSE,
      '#weight' => 0,
    ];

    $form['table_caution'][0]['caution_na'] = [
      '#type' => 'submit',
      '#name' => 'caution_na',
      '#value' => t('N/A'),
    ];

    $form['table_caution'][0]['caution_nr'] = [
      '#type' => 'submit',
      '#name' => 'caution_nr',
      '#value' => t('Non Renseigné'),
    ];

    $form['table_caution'][0]['caution_carte'] = [
      '#type' => 'submit',
      '#name' => 'caution_carte',
      '#value' => t('Carte Bancaire'),
    ];

    $form['table_caution'][0]['caution_cheque'] = [
      '#type' => 'submit',
      '#name' => 'caution_cheque',
      '#value' => t('Chèque'),
    ];

    return $form;
  }

  /**
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param mixed $field
   */
  public function setStatut(FormStateInterface $form_state, $field) {
    foreach ($form_state->getValue('table_demande') as $rdmid) {
      if ($rdmid) {
        $reservationDemande = ReservationDemande::load($rdmid);
        $reservationDemande->set('statut', $field);
        $reservationDemande->save();
      }
    }
  }

  /**
   *
   * @param mixed $form
   *
   * @return string
   */
  public function submitActionForm(FormStateInterface $form_state, $type_demande) {
    $request = \Drupal::requestStack()->getCurrentRequest();
    $requestParams = $request->request->all();
    $trigger = $form_state->getTriggeringElement();
    unset($requestParams['rdmid']);
    switch ($trigger['#name']) {
      case ReservationDemande::STATUT_CONFIRME:
      case ReservationDemande::STATUT_ANNULE:
      case ReservationDemande::STATUT_ATTENTE:
      case ReservationDemande::STATUT_CAUTION:
      case ReservationDemande::STATUT_REFUSE:
      case ReservationDemande::STATUT_ARCHIVE:
      case ReservationDemande::STATUT_SHOW:
      case ReservationDemande::STATUT_NO_SHOW:
      case ReservationDemande::STATUT_RAPPEL:
      case 'delete':
        if (array_key_exists('table_demande', $requestParams)) {
          $table_demande = $requestParams['table_demande'];
          if (is_array($table_demande)) {
            $values = $this->selectedId($table_demande);
            $form_state->setRedirectUrl(Url::fromRoute('reservation.demande.confirmation',
              array_merge([
                'rdmid' => $values,
                'action' => $trigger['#name'],
                'type' => $type_demande,
              ], $requestParams)));
          }
        }
        break;

      case 'reset':
        $form_state->setRedirectUrl(Url::fromRoute('reservation.demande.' . $type_demande));
        break;

      case 'filtre':
        $this->filtreDemande($form_state, $type_demande);
        break;
    }
  }

  /**
   *
   * @param array $valeurs
   *
   * @return array
   */
  public function selectedId(array $valeurs) {
    $ids = [];
    foreach ($valeurs as $valeur) {
      if ($valeur) {
        $ids[] = $valeur;
      }
    }
    return $ids;
  }

  /**
   *
   * @param mixed $form_state
   */
  public function filtreDemande($form_state, $type_demande) {
    if ($type_demande == 'simple') {
      $this->filtreDemandeSimple($form_state);
    }
    else {
      $this->filtreDemandeMultiple($form_state);
    }
  }

  /**
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function filtreDemandeSimple(FormStateInterface $form_state) {
    $ressource = $form_state->getValue(['table_filter', '1', 'ressource']);
    $statuts = [];
    foreach ($form_state->getValue([
      'table_filter',
      '1',
      'statut',
    ]) as $statut) {
      if ($statut) {
        $statuts[] = $statut;
      }
    }

    $choix_date_demande = $form_state->getValue([
      'table_filter',
      '1',
      'table_date_demande',
      '1',
      'choix_date_demande',
    ]);
    $date_demande_debut = $form_state->getValue([
      'table_filter',
      '1',
      'table_date_demande',
      '2',
      'date_demande_debut',
    ]);
    $date_demande_fin = $form_state->getValue([
      'table_filter',
      '1',
      'table_date_demande',
      '3',
      'date_demande_fin',
    ]);

    $choix_date_creneau = $form_state->getValue([
      'table_filter',
      '1',
      'table_date_creneau',
      '1',
      'choix_date_creneau',
    ]);
    $date_creneau_debut = $form_state->getValue([
      'table_filter',
      '1',
      'table_date_creneau',
      '2',
      'date_creneau_debut',
    ]);
    $date_creneau_fin = $form_state->getValue([
      'table_filter',
      '1',
      'table_date_creneau',
      '3',
      'date_creneau_fin',
    ]);

    $caution = $form_state->getValue(['table_filter', '1', 'caution']);

    $form_state->setRedirectUrl(Url::fromRoute('reservation.demande.simple',
      [
        'ressource' => $ressource,
        'statut' => $statuts,
        'choix_date_demande' => $choix_date_demande,
        'date_demande_debut' => $date_demande_debut,
        'date_demande_fin' => $date_demande_fin,
        'choix_date_creneau' => $choix_date_creneau,
        'date_creneau_debut' => $date_creneau_debut,
        'date_creneau_fin' => $date_creneau_fin,
        'caution' => $caution,
      ]));
  }

  /**
   *
   * @param mixed $form_state
   */
  public function filtreDemandeMultiple($form_state) {
    $year = $form_state->getValue(['table_filter', '1', 'year']);
    $month = $form_state->getValue(['table_filter', '1', 'month']);

    $caution = $form_state->getValue(['table_filter', '1', 'caution']);

    $form_state->setRedirectUrl(Url::fromRoute('reservation.demande.multiple',
      [
        'year' => $year,
        'month' => $month,
        'caution' => $caution,
      ]));
  }

  /**
   * @return array
   */
  public function getHeaderTableSimple() {

    $reservationSettings = \Drupal::config('reservation.settings');
    $demandeSettings = $reservationSettings->get('demande');

    $header = [];

    $header['ressource'] = [
      'data' => 'Ressource',
      'field' => 'rdid.entity.nid.entity.nid.entity.title',
      'specifier' => 'rdid.entity.nid.entity.nid.entity.title',
    ];

    $header['datedemande'] = [
      'data' => 'Date demande',
      'field' => 'created',
      'specifier' => 'created',
      'sort' => 'desc',
    ];

    $header['demandeur'] = [
      'data' => 'Demandeur',
      'field' => 'demandeur',
      'specifier' => 'demandeur',
    ];

    $header['email'] = [
      'data' => 'Email',
      'field' => 'email',
      'specifier' => 'email',
    ];

    $header['telephone'] = [
      'data' => 'Téléphone',
      'field' => 'telephone',
      'specifier' => 'telephone',
    ];

    $header['postcode'] = [
      'data' => 'Code postal',
      'field' => 'postcode',
      'specifier' => 'postcode',
    ];

    $header['datecreneau'] = [
      'data' => 'Date/Créneau',
      'field' => 'rdid.entity.date',
      'specifier' => 'rdid.entity.date',
    ];

    if ($demandeSettings['caution'] == TRUE) {
      $header['caution'] = [
        'data' => 'Caution',
        'field' => 'caution',
        'specifier' => 'caution',
      ];
    }

    if ($demandeSettings['inscrit'] == TRUE) {
      $header['jauge'] = [
        'data' => 'NB Inscrits',
      ];
    }

    $header['statut'] = [
      'data' => 'Statut',
      'field' => 'statut',
      'specifier' => 'statut',
    ];

    if (!empty($demandeSettings['webform_key']) && !empty($demandeSettings['webform_label'])) {
      $webformFieldKeys = explode('|', $demandeSettings['webform_key']);
      $webformFieldLabels = explode('|', $demandeSettings['webform_label']);
      foreach ($webformFieldKeys as $key => $webformFieldKey) {
        $header[$webformFieldKey] = [
          'data' => !empty($webformFieldLabels[$key]) ? $webformFieldLabels[$key] : $key,
        ];
      }
    }

    $header['actions'] = [
      'data' => 'Actions',
    ];

    return $header;
  }

  /**
   * @return array
   */
  public function getHeaderTableMultiple() {
    $reservationSettings = \Drupal::config('reservation.settings');
    $demandeSettings = $reservationSettings->get('demande');

    $header = [];

    $header['email'] = [
      'data' => 'Email',
      'field' => 'email',
      'specifier' => 'email',
    ];

    $header['telephone'] = [
      'data' => 'Téléphone',
      'field' => 'telephone',
      'specifier' => 'telephone',
    ];

    $header['postcode'] = [
      'data' => 'Code postal',
      'field' => 'postcode',
      'specifier' => 'postcode',
    ];

    $header['ressource'] = [
      'data' => 'Ressource',
      'field' => 'rdid.entity.nid.entity.nid.entity.title',
      'specifier' => 'rdid.entity.nid.entity.nid.entity.title',
    ];

    $header['datedemande'] = [
      'data' => 'Date demande',
      'field' => 'created',
      'specifier' => 'created',
      'sort' => 'desc',
    ];

    $header['demandeur'] = [
      'data' => 'Demandeur',
      'field' => 'demandeur',
      'specifier' => 'demandeur',
    ];

    $header['datecreneau'] = [
      'data' => 'Date/Créneau',
      'field' => 'rdid.entity.date',
      'specifier' => 'rdid.entity.date',
    ];

    if ($demandeSettings['caution'] == TRUE) {
      $header['caution'] = [
        'data' => 'Caution',
        'field' => 'caution',
        'specifier' => 'caution',
      ];
    }

    if ($demandeSettings['inscrit'] == TRUE) {
      $header['jauge'] = [
        'data' => 'NB Inscrits',
      ];
    }

    $header['statut'] = [
      'data' => 'Statut',
      'field' => 'statut',
      'specifier' => 'statut',
    ];

    if (!empty($demandeSettings['webform_key']) && !empty($demandeSettings['webform_label'])) {
      $webformFieldKeys = explode('|', $demandeSettings['webform_key']);
      $webformFieldLabels = explode('|', $demandeSettings['webform_label']);
      foreach ($webformFieldKeys as $key => $webformFieldKey) {
        $header[$webformFieldKey] = [
          'data' => !empty($webformFieldLabels[$key]) ? $webformFieldLabels[$key] : $key,
        ];
      }
    }

    $header['actions'] = [
      'data' => 'Actions',
    ];

    return $header;

  }

  /**
   * @return array
   */
  public function getStatutTab(
    $ressource = NULL,
    $choix_date_demande = TRUE,
    $date_demande_debut = NULL,
    $date_demande_fin = NULL,
    $choix_date_creneau = TRUE,
    $date_creneau_debut = NULL,
    $date_creneau_fin = NULL,
    $caution = NULL) {

    $counts = $this->demandeServices->countDemandeByFilter(
      $ressource, $choix_date_demande, $date_demande_debut, $date_demande_fin,
      $choix_date_creneau, $date_creneau_debut, $date_creneau_fin, $caution);

    foreach ($counts as $count) {
      $countStatut[$count["statut"]] = $count["statut_count"];
    }

    $statuts = [];

    $labels = $this->labelStatuts;

    foreach ($labels as $key => $label) {
      if (isset($labels[$key])) {
        if (isset($countStatut[$key])) {
          $statuts[$key] = $labels[$key] . " (" . $countStatut[$key] . ")";
        }
        else {
          $statuts[$key] = $labels[$key] . " (0)";
        }
      }
    }

    return $statuts;
  }

  /**
   * @param $statut
   *
   * @return mixed
   */
  public function getStatutFormat($statut) {
    $tab = $this->labelStatuts;

    return $tab[$statut];
  }

}
