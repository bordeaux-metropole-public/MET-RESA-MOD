<?php

namespace Drupal\reservation\Service;

use Drupal\reservation\Entity\ReservationDemande;
use Drupal\reservation\ReservationConstants;
use Drupal\reservation\ReservationUtils;

/**
 * Class ReservationDemandeServices.
 *
 * @package Drupal\reservation\Service
 */
class ReservationDemandeServices {

  /**
   *
   */
  const STATUT_RESERVE = 'reserveDates';

  /**
   *
   */
  const STATUT_WAIT = 'waitDates';

  /**
   *
   */
  const STATUT_OPEN = 'openDates';

  /**
   *
   */
  const RESERVATION_DEMANDE_ENTITY_NAME = 'reservation_demande';

  /**
   * ReservationDemandeServices constructor.
   */
  public function __construct() {

  }

  /**
   *
   * @param string $sid
   *
   * @return mixed
   */
  public function loadByWebformSubmission(string $sid = NULL) {
    $id = $this->getByWebform($sid);
    return $id ? $this->load($id) : NULL;
  }

  /**
   *
   * @param string $sid
   *
   * @return mixed
   */
  public function getByWebform(string $sid = NULL) {
    $query = \Drupal::entityQuery('reservation_demande');
    $query->condition('sid', $sid, '=');
    $query->accessCheck(TRUE);
    return current($query->execute());
  }

  /**
   *
   * @param mixed $id
   *
   * @return \Drupal\reservation\Entity\ReservationDemande
   */
  public function load($id) {
    return \Drupal::entityTypeManager()
      ->getStorage(self::RESERVATION_DEMANDE_ENTITY_NAME)
      ->load($id);
  }

  /**
   * {@inheritdoc}
   */
  public function getYearMax() {
    $date = new \DateTime();
    $date->setTimestamp($this->getDateMax());
    return $date->format('Y');
  }

  /**
   *
   * @return mixed
   */
  public function getDateMax() {
    $query = \Drupal::entityQuery(self::RESERVATION_DEMANDE_ENTITY_NAME);
    $query->range(0, 1);
    $query->sort('created', 'DESC');
    $query->accessCheck(TRUE);
    $retour = $query->execute();
    $reservationdemande = ReservationDemande::load(current($retour));
    if ($reservationdemande) {
      return $reservationdemande->getCreatedTime();
    }
    else {
      return new \DateTime('last day of this month');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getYearMin() {
    $date = new \DateTime();
    $date->setTimestamp($this->getDateMin());
    return $date->format('Y');
  }

  /**
   * {@inheritdoc}
   */
  public function getDateMin() {
    $query = \Drupal::entityQuery(self::RESERVATION_DEMANDE_ENTITY_NAME);
    $query->range(0, 1);
    $query->sort('created', 'ASC');
    $query->accessCheck(TRUE);
    $retour = $query->execute();
    $reservationdemande = ReservationDemande::load(current($retour));
    if ($reservationdemande) {
      return $reservationdemande->getCreatedTime();
    }
    else {
      return new \DateTime('first day of this month');
    }
  }

  /**
   * @param $rdid
   * @param int $years
   *
   * @return mixed
   * @throws \Exception
   */
  public function getDemandeByYear($rdid, $years = 1) {

    $date_old = new \DateTime();
    $date_old->sub(new \DateInterval('P' . $years . 'Y'));
    $query = \Drupal::entityQuery(self::RESERVATION_DEMANDE_ENTITY_NAME);
    $query->condition('rdid', $rdid, 'IN');
    $query->condition('created', $date_old->getTimestamp(), '<');
    $query->accessCheck(TRUE);
    return $this->getAll($query->execute());
  }

  /**
   *
   * @param mixed $ids
   *
   * @return mixed
   */
  public function getAll($ids = NULL) {
    return \Drupal::entityTypeManager()
      ->getStorage(self::RESERVATION_DEMANDE_ENTITY_NAME)
      ->loadMultiple($ids);
  }

  /**
   * @param $rdid
   *
   * @return mixed
   */
  public function getDemandeByRdid($rdid) {

    $query = \Drupal::entityQuery(self::RESERVATION_DEMANDE_ENTITY_NAME);
    $query->condition('rdid', $rdid);
    $condition_or = $query->orConditionGroup();
    $condition_or->condition('rhid', '0');
    $condition_or->notExists('rhid');
    $query->condition($condition_or);
    $query->accessCheck(FALSE);

    return $this->getAll($query->execute());
  }

  /**
   * @param $rhid
   *
   * @return mixed
   */
  public function getDemandeByRhid($rhid) {

    $query = \Drupal::entityQuery(self::RESERVATION_DEMANDE_ENTITY_NAME);
    $query->condition('rhid', $rhid);
    $query->accessCheck(FALSE);

    return $this->getAll($query->execute());
  }

  /**
   * @param string|null $statut
   *
   * @return mixed
   */
  public function getDdemandeEmail(string $statut = NULL) {
    $query = \Drupal::entityQuery(self::RESERVATION_DEMANDE_ENTITY_NAME);

    if ($statut) {
      $query->condition('statut', $statut, '=');
    }
    $query->accessCheck(TRUE);
    return $this->getAll($query->execute());
  }

  /**
   * @param $header
   * @param int $pager
   * @param $nid
   * @param $statut
   * @param bool $choix_date_demande
   * @param $date_demande_debut
   * @param $date_demande_fin
   * @param bool $choix_date_creneau
   * @param $date_creneau_debut
   * @param $date_creneau_fin
   *
   * @return mixed
   * @throws \Exception
   */
  public function getDemandesFiltered(
    $header = NULL,
    int $pager = 20,
    $nid = NULL,
    $statut = NULL,
    $choix_date_demande = TRUE,
    $date_demande_debut = NULL,
    $date_demande_fin = NULL,
    $choix_date_creneau = TRUE,
    $date_creneau_debut = NULL,
    $date_creneau_fin = NULL,
    $caution = NULL) {
    $query = \Drupal::entityQuery(self::RESERVATION_DEMANDE_ENTITY_NAME);

    if ($nid) {
      $query->condition('rdid.entity.nid', $nid, 'IN');
    }

    if ($statut) {
      $query->condition('statut', $statut, 'IN');
    }

    if ($choix_date_demande && $date_demande_debut) {
      $date = new \DateTime($date_demande_debut);
      $query->condition('created', $date->getTimestamp(), '>');
    }

    if ($choix_date_demande && $date_demande_fin) {
      $date = new \DateTime($date_demande_fin);
      $query->condition('created', $date->getTimestamp(), '<');
    }

    if ($choix_date_creneau && $date_creneau_debut) {
      $date = new \DateTime($date_creneau_debut);
      $query->condition('rdid.entity.date', $date->format('Y-m-d\T00:00:00'), '>');
    }

    if ($choix_date_creneau && $date_creneau_fin) {
      $date = new \DateTime($date_creneau_fin);
      $query->condition('rdid.entity.date', $date->format('Y-m-d\T00:00:00'), '<');
    }

    if ($caution) {
      $query->condition('caution', $caution, 'IN');
    }

    if ($header) {
      $query->tableSort($header);
    }

    $query->pager($pager);
    $query->accessCheck(TRUE);

    return $this->getAll($query->execute());
  }

  /**
   * @param $nid
   * @param bool $choix_date_demande
   * @param $date_demande_debut
   * @param $date_demande_fin
   * @param bool $choix_date_creneau
   * @param $date_creneau_debut
   * @param $date_creneau_fin
   * @param $caution
   *
   * @return array
   * @throws \Exception
   */
  public function countDemandeByFilter(
    $nid = NULL,
    $choix_date_demande = TRUE,
    $date_demande_debut = NULL,
    $date_demande_fin = NULL,
    $choix_date_creneau = TRUE,
    $date_creneau_debut = NULL,
    $date_creneau_fin = NULL,
    $caution = NULL) {
    $query = \Drupal::entityQueryAggregate(self::RESERVATION_DEMANDE_ENTITY_NAME);

    if ($nid) {
      $query->condition('rdid.entity.nid', $nid, 'IN');
    }

    if ($choix_date_demande && $date_demande_debut) {
      $date = new \DateTime($date_demande_debut);
      $query->condition('created', $date->getTimestamp(), '>');
    }

    if ($choix_date_demande && $date_demande_fin) {
      $date = new \DateTime($date_demande_fin);
      $query->condition('created', $date->getTimestamp(), '<');
    }

    if ($choix_date_creneau && $date_creneau_debut) {
      $date = new \DateTime($date_creneau_debut);
      $query->condition('rdid.entity.date', $date->format('Y-m-d\T00:00:00'), '>');
    }

    if ($choix_date_creneau && $date_creneau_fin) {
      $date = new \DateTime($date_creneau_fin);
      $query->condition('rdid.entity.date', $date->format('Y-m-d\T00:00:00'), '<');
    }

    if ($caution) {
      $query->condition('caution', $caution, 'IN');
    }

    $query
      ->groupBy('statut')
      ->aggregate('statut', 'COUNT');

    $query->accessCheck(TRUE);

    return $query->execute();
  }

  /**
   * @param $header
   * @param int $pager
   * @param $year
   * @param $month
   * @param $caution
   * @param $resourceIds
   *
   * @return mixed
   * @throws \Exception
   */
  public function getDemandeMultipleByFilter(
    $header = NULL,
    $pager = 15,
    $year = NULL,
    $month = NULL,
    $caution = NULL,
    $resourceIds = NULL) {
    $emails = [];
    $date_debut = new \DateTime($year . '-' . $month);
    $date_fin = new \DateTime($year . '-' . $month);
    $date_fin->add(new \DateInterval('P1M'));

    $queryEmail = \Drupal::entityQueryAggregate(self::RESERVATION_DEMANDE_ENTITY_NAME);

    $queryEmail->condition('created', $date_debut->getTimestamp(), '>');
    $queryEmail->condition('created', $date_fin->getTimestamp(), '<');

    $queryEmail->conditionAggregate('rdid', 'COUNT', 1, '>');
    $queryEmail->groupby('email');

    $queryEmail->accessCheck(TRUE);

    $rows = $queryEmail->execute();
    foreach ($rows as $row) {
      $emails[] = $row['email'];
    }

    $query = \Drupal::entityQuery(self::RESERVATION_DEMANDE_ENTITY_NAME);

    if ($resourceIds) {
      $query->condition('rdid.entity.nid', $resourceIds, 'IN');
    }

    $query->condition('created', $date_debut->getTimestamp(), '>');
    $query->condition('created', $date_fin->getTimestamp(), '<');

    $query->condition('statut', ReservationDemande::STATUT_FORMULAIRE, '<>');

    if ($emails) {
      $query->condition('email', $emails, 'IN');
    }
    else {
      $query->condition('email', ' ', '=');
    }

    if ($caution) {
      $query->condition('caution', $caution, 'IN');
    }

    $query->tableSort($header);

    $query->pager($pager);
    $query->accessCheck(TRUE);

    return $this->getAll($query->execute());
  }

  /**
   * @param $nid
   * @param string $type_email
   *
   * @return mixed
   */
  public function getDemandeByNidType($nid, $type_email = 'demande') {
    $nodes = \Drupal::entityTypeManager()
      ->getStorage(self::RESERVATION_DEMANDE_ENTITY_NAME)
      ->loadByProperties(['nid' => $nid, 'type_email' => $type_email]);

    return array_shift($nodes);
  }

  /**
   * @param $rdid
   * @param string $rhid
   * @param $sid
   * @param $statut
   * @param $demandeur
   * @param $jauge
   * @param $email
   * @param $telephone
   *
   * @return mixed
   */
  public function createDemande(
    $rdid = NULL,
    $rhid = '0',
    $sid = NULL,
    $statut = NULL,
    $demandeur = NULL,
    $jauge = NULL,
    $email = NULL,
    $telephone = NULL) {
    $demande = ReservationDemande::create([
      'rdid' => $rdid,
      'rhid' => $rhid,
      'sid' => $sid,
      'statut' => $statut,
      'demandeur' => $demandeur,
      'jauge' => $jauge,
      'email' => $email,
      'telephone' => $telephone,
    ]);
    $demande->save();

    return $demande;
  }

  /**
   * @param $rdid
   * @param string $rhid
   * @param $statut
   * @param $jauge
   * @param string $wfToken
   * @param \DateTime $wfTokenExpiration
   *
   * @return mixed
   */
  public function createDemandeFromWebform(
    $rdid = NULL,
    $rhid = '0',
    $statut = NULL,
    $jauge = NULL,
    string $wfToken = NULL,
    \DateTime $wfTokenExpiration = NULL) {
    $wfTokenExpirationValue = ReservationUtils::getDateEntityFieldStringValue($wfTokenExpiration);
    $demande = ReservationDemande::create([
      'rdid' => $rdid,
      'rhid' => $rhid,
      'statut' => $statut,
      'jauge' => $jauge,
      'webform_token' => $wfToken,
      'webform_token_expiration' => $wfTokenExpirationValue,
    ]);
    $demande->save();
    return $demande;
  }

  /**
   * @param $rdmid
   * @param $rdid
   * @param $rhid
   * @param $sid
   * @param $statut
   * @param $demandeur
   * @param $jauge
   * @param $email
   * @param $telephone
   *
   * @return mixed
   */
  public function editDemande(
    $rdmid = NULL,
    $rdid = NULL,
    $rhid = NULL,
    $sid = NULL,
    $statut = NULL,
    $demandeur = NULL,
    $jauge = NULL,
    $email = NULL,
    $telephone = NULL) {
    $demande = $this->load($rdmid);
    if ($rdid) {
      $demande->setRdid($rdid);
    }
    if ($rhid) {
      $demande->setRhid($rhid);
    }
    if ($statut) {
      $demande->setStatut($statut);
    }
    if ($demandeur) {
      $demande->setDemandeur($demandeur);
    }
    if ($jauge) {
      $demande->setJauge($jauge);
    }
    if ($email) {
      $demande->setEmail($email);
    }
    if ($telephone) {
      $demande->setTelephone($telephone);
    }
    $demande->save();

    return $demande;
  }

  /**
   *
   * @param string $rdmid
   * @param string $action
   *
   * @return mixed
   */
  public function setStatut(string $rdmid, string $action) {
    $reservationDemande = $this->load($rdmid);
    if ($reservationDemande) {
      $reservationDemande->setStatut($action);
      return $reservationDemande->save();
    }
    else {
      return FALSE;
    }
  }

  /**
   *
   * @param string $rdmid
   * @param string $caution
   *
   * @return mixed
   */
  public function setCaution(string $rdmid, string $caution) {
    $reservationDemande = $this->load($rdmid);
    $reservationDemande->setCaution($caution);
    return $reservationDemande->save();
  }

  /**
   * @param \Drupal\reservation\Entity\ReservationDemande $reservationDemande
   *
   * @return int
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function annuleDemande(ReservationDemande $reservationDemande) {
    $reservationDemande->setStatut(ReservationDemande::STATUT_ANNULE);
    return $reservationDemande->save();
  }

  /**
   *
   * @param array $rdmids
   *
   * @return int
   */
  public function deleteMultipleDemande(array $rdmids = []) {
    $count = 0;
    foreach ($rdmids as $rdmid) {
      $this->deleteDemande($rdmid);
      $count++;
    }
    return $count;
  }

  /**
   *
   * @param string $rdmid
   *
   * @return void
   */
  public function deleteDemande(string $rdmid) {
    $reservationDemande = $this->load($rdmid);
    if ($reservationDemande) {
      $this->deleteDemandeEntity($reservationDemande);
    }
    else {
      \Drupal::logger(ReservationConstants::LOG_CHANNEL)->warning(
        t('Delete canceled, ReservationDemande %rdmid entity not found.', ['%rdmid' => $rdmid]));
    }
  }

  /**
   * Suppression d'une entité ReservationDemande.
   *
   * @param \Drupal\reservation\Entity\ReservationDemande $reservationDemande
   *   Entité ReservationDemande à supprimer.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function deleteDemandeEntity(ReservationDemande $reservationDemande) {
    if ($reservationDemande) {
      $reservationDemande->delete();
    }
    else {
      \Drupal::logger(ReservationConstants::LOG_CHANNEL)->warning(
        t("Can't delete null ReservationDemande entity."));
    }
  }

  /**
   * Extraction de toutes les demandes éligible à la notification de rappel.
   */
  public function getDemandeRappel() {
    $datas = [];

    $demandes = $this->getDemandeByType("rappel", "confirme");
    foreach ($demandes as $demande) {
      if ($demande->getDate()->availableRappel()) {
        $datas[] = $demande;
      }
    }

    return $datas;
  }

  /**
   * @param $type
   * @param string $statut
   *
   * @return mixed
   */
  public function getDemandeByType($type = NULL, string $statut = 'confirme') {
    $query = \Drupal::entityQuery(self::RESERVATION_DEMANDE_ENTITY_NAME);

    $query->condition('statut', $statut, '=');
    $query->condition('rdid.entity.statut', 1, '=');
    $query->condition('rdid.entity.nid.entity.rrid.entity.statut', 1, '=');

    if ($type == 'enquete') {
      $query->condition('rdid.entity.enquete', 1, '=');
      $date = new \DateTime();
      $date->add(new \DateInterval('P1D'));
      $query->condition('rdid.entity.date', $date->format('Y-m-d\T00:00:00'), '<');
    }

    if ($type == 'rappel') {
      $query->condition('rdid.entity.rappel', 1, '=');
      $date = new \DateTime();
      $query->condition('rdid.entity.date', $date->format('Y-m-d\T00:00:00'), '>');
    }

    $query->accessCheck(TRUE);

    return $this->getAll($query->execute());
  }

  /**
   * Extraction de toutes les demandes éligible à la notification d'une enquête.
   */
  public function getDemandeEnquete() {
    $datas = [];

    $demandes = $this->getDemandeByType("enquete", "confirme");
    foreach ($demandes as $demande) {
      if ($demande->getDate()->availableEnquete()) {
        $datas[] = $demande;
      }
    }

    return $datas;
  }

  /**
   *
   */
  public function destroyExpiredWebformDemandes() {
    $demandes = $this->getExpiredWebformDemandes();
    foreach ($demandes as $demande) {
      $this->deleteDemandeEntity($demande);
    }
  }

  /**
   *
   * @return \Drupal\Core\Entity\Entity[]|\Drupal\Core\Entity\EntityInterface[]
   */
  public function getExpiredWebformDemandes() {
    $nowFormatted = ReservationUtils::getDateEntityFieldStringValue(new \DateTime());

    $query = \Drupal::entityQuery('reservation_demande');

    $statut_or_condition = $query->orConditionGroup();
    $statut_or_condition->condition(
      'statut', ReservationDemande::STATUT_FORMULAIRE, '=');
    $statut_caution_cb = $query->andConditionGroup();
    $statut_caution_cb->condition(
      'statut', ReservationDemande::STATUT_CAUTION, '=');
    $statut_caution_cb->condition(
      'caution', ReservationDemande::CAUTION_CARTE, '=');
    $statut_or_condition->condition($statut_caution_cb);

    $query->condition($statut_or_condition);
    $query->condition('webform_token_expiration', $nowFormatted, '<');
    $query->accessCheck(FALSE);

    return ReservationDemande::loadMultiple($query->execute());
  }

}
