<?php

namespace Drupal\reservation\Service;

/**
 * Class ReservationRessourceUserServices.
 *
 * @package Drupal\reservation\Service
 */
class ReservationRessourceUserServices {

  /**
   *
   */
  const RESERVATION_RESSOURCE_USER_ENTITY_NAME = 'reservation_ressource_user';

  /**
   *
   * @var demandeServices
   */
  protected $ressourceServices;

  /**
   *
   * @param \Drupal\reservation\Service\ReservationRessourceServices $ressourceServices
   */
  public function __construct(ReservationRessourceServices $ressourceServices) {
    $this->ressourceServices = $ressourceServices;
  }

  /**
   * @param $id
   *
   * @return mixed
   */
  public function load($id = NULL) {
    return \Drupal::entityTypeManager()
      ->getStorage(self::RESERVATION_RESSOURCE_USER_ENTITY_NAME)
      ->load($id);
  }

  /**
   *
   * @param mixed $uid
   *
   * @return array
   */
  public function getRessourceUserNodeIdsByUid($uid = NULL) {
    $ids = [];
    foreach ($this->getRessourceUserByUid($uid) as $ressource_user) {
      if ($ressource_user->getNode()) {
        $ids[] = $ressource_user->getNode()->id();
      }
    }
    return $ids;
  }

  /**
   *
   * @param mixed $uid
   *
   * @return mixed
   */
  public function getRessourceUserByUid($uid = NULL) {
    return $this->getAll($this->queryRessourceUserByUid($uid));
  }

  /**
   *
   * @param mixed $ids
   *
   * @return mixed
   */
  public function getAll($ids = NULL) {
    return \Drupal::entityTypeManager()
      ->getStorage(self::RESERVATION_RESSOURCE_USER_ENTITY_NAME)
      ->loadMultiple($ids);
  }

  /**
   *
   * @param mixed $uid
   *
   * @return mixed
   */
  public function queryRessourceUserByUid($uid = NULL, $nid = NULL) {
    $query = \Drupal::entityQuery(self::RESERVATION_RESSOURCE_USER_ENTITY_NAME);
    if ($uid) {
      $query->condition('user_id', $uid);
    }
    if ($nid) {
      $query->condition('nid', $nid);
    }
    $query->accessCheck(TRUE);
    return $query->execute();
  }

  /**
   *
   * @param mixed $uid
   *
   * @return mixed
   */
  public function getRessourceUserByNid($nid = NULL) {
    return $this->getAll($this->queryRessourceUserByUid(NULL, $nid));
  }

  /**
   *
   * @param mixed $uid
   *
   * @return array
   */
  public function getUserNodeIds($uid = NULL) {
    $nodeIds = [];
    foreach ($this->getRessourceUserByUid($uid) as $user) {
      if ($user->getNode()) {
        $nodeIds[] = $user->getNode()->id();
      }
    }
    return $nodeIds;
  }

  /**
   *
   * @param mixed $uid
   *
   * @return mixed
   */
  public function getUser($uid = NULL, $nid = NULL) {
    return current($this->getRessourceUserByUidRessource($uid, $nid));
  }

  /**
   *
   * @param mixed $uid
   *
   * @return mixed
   */
  public function getRessourceUserByUidRessource($uid = NULL, $nid = NULL) {
    return $this->getAll($this->queryRessourceUserByUid($uid, $nid));
  }

  /**
   * @param $role
   * @param $nid
   *
   * @return array
   */
  public function getUsers($role = NULL, $nid = NULL) {
    $users = \Drupal::entityTypeManager()->getStorage('user')->loadMultiple();

    $array = [];
    foreach ($users as $user) {
      $query = \Drupal::entityQuery(self::RESERVATION_RESSOURCE_USER_ENTITY_NAME);
      $query->condition('user_id', $user->id());
      $query->condition('nid', $nid);
      $query->accessCheck(TRUE);
      $exist_user = $query->count()->execute();

      if ($user->id() && array_search($role, $user->getRoles()) && !$exist_user) {
        $array[$user->id()] = $user->get('name')->value;
      }
    }

    return $array;
  }

  /**
   * @return array
   */
  public function getRoles() {
    $roles = \Drupal::entityTypeManager()
      ->getStorage('user_role')
      ->loadMultiple();

    $array = [];
    foreach ($roles as $role) {
      $array[$role->id()] = $role->label();
    }

    return $array;
  }

  /**
   * {@inheritdoc}
   */
  public function getPublieMonth($year, $month, $nid, $publie = TRUE) {
    $query = $this->queryDate($nid, $year . '-' . $month, TRUE, $publie);
    $query->range(0, 1);
    return $query->count()->execute() ? TRUE : FALSE;
  }

}
