<?php

namespace Drupal\reservation\Theme;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;
use Drupal\reservation\ReservationConstants;
use Symfony\Component\Routing\Route;

/**
 * Defines a theme negotiator for the Reservation iframe routes.
 */
class ReservationThemeNegotiator implements ThemeNegotiatorInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a DbUpdateNegotiator.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    $route = $route_match->getRouteObject();
    if (!$route instanceof Route) {
      return FALSE;
    }
    $option = $route->getOption('_iframe');
    if (!$option) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function determineActiveTheme(RouteMatchInterface $route_match) {
    $reservationSettings = \Drupal::config(ReservationConstants::MODULE_SETTINGS);
    $cautionSettings = $reservationSettings->get('caution');
    $iframe_theme = $cautionSettings['iframe_theme'];
    if (!$iframe_theme) {
      $config = $this->configFactory->get('system.theme');
      $iframe_theme = $config->get('default');
    }
    return $iframe_theme;
  }

}
